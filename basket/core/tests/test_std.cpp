#include <catch2/catch_test_macros.hpp>

#include <core/vector.hpp>
#include <core/tuple.hpp>
#include <core/dequeue.hpp>
#include <core/list.hpp>
#include <core/functional.hpp>
#include <core/thread.hpp>
#include <core/mutex.hpp>
#include <core/memory.hpp>
#include <core/string.hpp>
#include <core/string_view.hpp>
#include <core/map.hpp>

using namespace basket;

class foo
{
    public:
        int dummy(int value)
        {
            BK_LOG(core, info, "dummy call");
            return value + 10;
        }
};

int f()
{
    return 10;
}

void f2(int& x)
{
    x += 100;
}

void reset(float& value)
{
    value = 0.0f;
}

int function1()
{
    return 10;
}

int function2()
{
    BK_LOG(core, info, "function2 call");
    return 10;
}

int function_arg(int value)
{
    BK_LOG(core, info, "function_arg call");
    return value * 2;
}

int _thread_value = 0;
int function_arg_thread(int value)
{
    BK_LOG(core, info, "function_arg call from THREAD");
    _thread_value = value;
    return value * 2;
}

mutex _test_mutex;

void function_mutex(int from, int to)
{
    _test_mutex.lock();
    for (int index = from; index < to; ++index)
    {
        BK_LOG(core, info, "value {0}", index);
    }

    _test_mutex.unlock();
}


struct functor1
{
    int operator()(int a)
    {
        return a;
    }
};


TEST_CASE("Testing std")
{
    SECTION("vector")
    {
        {
            vector<int> primitives;
            REQUIRE(primitives.size() == 0);
            REQUIRE(primitives.capacity() == 0);
            REQUIRE(primitives.data() == nullptr);

            primitives.push_back(1);
            primitives.push_back(2);
            primitives.push_back(3);
            primitives.push_back(4);

            REQUIRE(primitives.size() == 4);
            REQUIRE(primitives.capacity() == 4);
            REQUIRE(primitives[2] == 3);
            REQUIRE(primitives.data() != nullptr);

            {
                int sum = 0;
                for(int value : primitives)
                {
                    sum += value;
                }

                REQUIRE(sum == 10);
            }
        }

        {
            int values[5] {0, 1, 2, 3, 4};

            vector<int> v(values, values + 5);

            REQUIRE(v.size() == 5);
            REQUIRE(v.capacity() == 5);
            REQUIRE(v[0] == 0);
            REQUIRE(v[1] == 1);
            REQUIRE(v[2] == 2);
            REQUIRE(v[3] == 3);
            REQUIRE(v[4] == 4);
        }
        
        {
            vector<int> one;
            vector<int> two;

            REQUIRE(one == two);

            one.emplace_back(10);

            REQUIRE(one.size() == 1);
            REQUIRE(one.capacity() == 1);

            REQUIRE(one != two);

            two.reserve(1);

            REQUIRE(one.capacity() == two.capacity());

            two = one;

            REQUIRE(one == two);

        }

        {
            struct foo
            {
                int var;
                foo() = default;

                foo(const foo&) = default;

                foo(foo&&) = default;
            };

            // BK_LOG(cat, info, ("-------------------------");
            vector<foo> foos;

            foos.resize(2);

            foos[0].var = 10;
            foos[1].var = 20;
            // BK_LOG(cat, info, ("-------------------------");
            vector<foo> cfoos(foos);
            REQUIRE(cfoos.size() == foos.size());
            REQUIRE(cfoos.capacity() == foos.capacity());
            REQUIRE(cfoos[0].var == foos[0].var);
            // BK_LOG(cat, info, ("-------------------------");
            vector<foo> mfoos(move(cfoos));
            REQUIRE(cfoos.size() == 0);
            REQUIRE(mfoos.size() == foos.size());
            REQUIRE(mfoos[0].var == 10);

        }

        {
            vector<int> values{1, 2, 3, 4, 5};
            REQUIRE(values.size() == 5);
        }
    }

    SECTION("Tuple")
    {
        using tp1 = tuple<int, float, char>;

        using tct = tuple_element<2, tp1>::type;
        using tct2 = tuple_element<0, tp1>::type;

        static_assert(is_same_v<tct, char>, "Type doesn't match");
        static_assert(is_same_v<tct2, int>, "Type doesn't match");
        tuple<int, int, float, decay<int()>::type> t(1, 0, 0.5f, &function1);

        static_assert(is_same_v<decltype(get<0>(t)), int&>, "Type doesn't match");
        static_assert(is_same_v<decltype(get<2>(t)), float&>, "Type doesn't match");

        REQUIRE(get<0>(t) == 1);
        REQUIRE(get<2>(t) == 0.5f);
        REQUIRE(get<3>(t)() == 10);

        REQUIRE(tuple_size<tp1>::value == 3);

        tuple t2(t);

        REQUIRE(tuple_size<decltype(t)>::value == tuple_size<decltype(t2)>::value);

        // tuple<int, int, float, decay<int()>::type> t2(t);

        // REQUIRE(get<0>(t2) == 1);
        // REQUIRE(get<2>(t2) == 0.5f);
        // REQUIRE(get<3>(t2)() == 13);

        // tuple<> te;

        // get<0>(te);
    }

    SECTION("map")
    {
        {
            map<int, string> m;
            REQUIRE(m.size() == 0);
            m.insert({0, "Hello"});
            REQUIRE(m.size() == 1);
            m.insert({1, "s1"});
            m.insert({-1, "s-1"});
            REQUIRE(m.size() == 3);
            m.erase(0);
            REQUIRE(m.size() == 2);
        }

        {
            map<int, char> m;
            REQUIRE(m.size() == 0);
            m.insert({0, 'a'});
            REQUIRE(m.size() == 1);
            m.insert({1, 'b'});
            m.insert({-1, 'c'});
            REQUIRE(m.size() == 3);
            m.insert({-4, 'd'});
            m.insert({-2, 'e'});
            m.insert({-5, 'f'});
            m.insert({4, 'g'});
            m.insert({2, 'h'});
            m.insert({5, 'i'});

            REQUIRE(m.size() == 9);
            m.erase(-1);
            REQUIRE(m.size() == 8);

            REQUIRE(m[4] == 'g');
            
        }
    }

    SECTION("Dequeue")
    {
        Dequeue<int> d;
        d.push_front(10);
        REQUIRE(d.front() == 10);
    }

    SECTION("List")
    {
        struct s1
        {
            int value;

            s1(): value(0)
            {}

            s1(const s1& p) = default;
            s1(s1&& p) = default;

            s1(int p_value): value(p_value)
            {

            }

            bool operator==(const s1& o)
            {
                return value == o.value;
            }
        };

        

        List<s1> l;

        REQUIRE(l.size() == 0);
        REQUIRE(l.empty());

        s1 s;
        s.value = 33;
        l.push_back(s);
        REQUIRE(l.size() == 1);
        REQUIRE(!l.empty());

        REQUIRE(l.front() == l.back());

        s1& val = l.front();
        val.value = 40;
        REQUIRE(l.back() == 40);
        l.clear();

        REQUIRE(l.size() == 0);
        REQUIRE(l.empty());

        
    }

    SECTION("unique_ptr")
    {
        {
            unique_ptr<int> value(new int(10));
            REQUIRE(*value == 10);
            *value = 99;
            REQUIRE(*value == 99);
        }

        {
            
            struct s1
            {
                int value;
            };

            unique_ptr<s1> po(new s1);
            po->value = 20;

            REQUIRE((*po).value == 20);
            REQUIRE(po->value == 20);
        }

        {
            struct s2
            {
                float value;
                s2(float p_value): value(p_value) {}
            };

            unique_ptr<s2> p(make_unique<s2>(30));

            REQUIRE(p->value == 30.0f);
            unique_ptr<s2> p2 = p.release();
            REQUIRE(p.get() == nullptr);
            REQUIRE(p2->value == 30.0f);

            unique_ptr<s2> p3(new s2(40));
            p2.swap(move(p3));
            REQUIRE(p2->value == 40.0f);
            
        }
    }


    SECTION("shared_ptr")
    {
        {
            shared_ptr<int> s(nullptr);
            {
                shared_ptr<int> s1(new int);
                REQUIRE(s1);

                s = s1;

                *s1 = 10;

                shared_ptr<int> s2(s1);
                REQUIRE(s1 == s2);
            }

            REQUIRE(*s == 10);
        }
    }

    SECTION("string")
    {
        {
            string s;
            s.resize(10);
            REQUIRE(s.size() == 10);
        }

        {
            string s = "dummy";

            BK_LOG(core, info, s);

            REQUIRE(s.size() == 5);

            string s2("YMMUD");
            string r(s + s2);
            BK_LOG(core, info, r);
            REQUIRE(r == "dummyYMMUD");
            string sub = r.substr(0, 5);
            REQUIRE(sub == "dummy");

            REQUIRE(r.find('M', 0) == 6);
            REQUIRE(r.find('r', 0) == string::npos);
            REQUIRE(r.rfind('M') == 7);
            REQUIRE(r.rfind('r') == string::npos);
        }

        {
            string s1 = "/modules/render/tests/triangle.vert";
            string s2 = "G:\\basket\\build\\Debug\\bin";

            string r = s2 + s1;
            REQUIRE(r == "G:\\basket\\build\\Debug\\bin/modules/render/tests/triangle.vert");
        }

        {
            string s1 = "/modules/render/tests/triangle.vert";
            string s2 = "G:\\basket\\build\\Debug\\bin\\editor.exe";
            // string s2 = "G:/basket/build/Debug/bin/editor.exe";
            auto pos = s2.rfind('\\');
            s2 = s2.substr(0, pos);

            string r = s2 + s1;
            REQUIRE(r == "G:\\basket\\build\\Debug\\bin/modules/render/tests/triangle.vert");
        }
    }


    SECTION("functional")
    {
        
        

        SECTION("Traits")
        {
            using sequence = index_sequence<1, 2, 3>;
            REQUIRE(sequence().size() == 3);
        }

        SECTION("function")
        {
            

            function<int(int)> f(nullptr);

            REQUIRE(f(10) == 0);

            f = &function_arg;

            REQUIRE(f(10) == 20);

            function<int(int)> f2;

            f2 = f;
            REQUIRE((f(10) == 20 && f2(1) == 2));

            function<int(int)> f3;

            f3 = move(f);

            REQUIRE((f3(50) == 100));
            #ifndef BASKET_STD
                REQUIRE((f(999) == 0)); //this test is noly
            #endif


            auto bo = bind(function_arg, placeholders::_1);

            REQUIRE(bo(2) == 4);

            // function<int(int)> bf = bo;

        }
    }



    

    SECTION("string_view")
    {

        string s = "Hi! how are you?";

        {
            string_view v;
            REQUIRE(v.size() == 0);
            REQUIRE(v.data() == nullptr);
        }

        {
            string_view v(s.data(), s.size());

            REQUIRE(v.size() == s.size());
            REQUIRE(v.data() == s.data());
        }


    }

    // SECTION("threads")
    // {
    //     REQUIRE(_thread_value == 0);
    //     thread t(function_arg_thread, 10);

    //     t.join();

    //     REQUIRE(_thread_value == 10);
    // }

    // SECTION("mutex")
    // {
    //     thread t(function_mutex, 0, 100);
    //     thread t2(function_mutex, 101, 200);

    //     t.join();
    //     t2.join();
    // }

        // SECTION("Set")
        // {
        //     int value;

        //     set<int> s;
        // }

    // SECTION("Set")
    // {
    //     Set<int> s;

    // }
}