#include <catch2/catch_test_macros.hpp>

#include <core/reflection/meta_variable.hpp>
#include <core/reflection/meta_component.hpp>

using namespace basket;

struct bar
{
    int val1;
    int val2;
};

TEST_CASE("IO", "[io]")
{

    SECTION("Variables")
    {
        bar sample;

        MetaVariable meta1("val1", offsetof(bar, val1));
        meta1.set_instance(&sample);
        meta1.assign(10);
        REQUIRE(meta1.get<int>() == 10);

        MetaVariable meta2("val2", offsetof(bar, val2));
        meta2.set_instance(&sample);
        meta2.assign(9);
        REQUIRE(meta2.get<int>() == 9);
        REQUIRE(meta2.get<int>() + meta1.get<int>() == 19);
    }

    SECTION("Component")
    {
        bar sample;

        MetaComponent mc = BK_REGISTER_COMPONENT_IMPLEMENTATION(bar, pair<string, size_t>("val1", offsetof(bar, val1)), pair<string, size_t>("val2", offsetof(bar, val2)));

        mc.set("val1", 69);

        REQUIRE(mc.get<int>("val1") == 69);
    }

}