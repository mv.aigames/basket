// #define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>
#include <core/math/constants.hpp>
#include <core/math/vector2.hpp>
#include <core/math/vector3.hpp>
#include <core/math/vector4.hpp>
#include <core/math/matrix22.hpp>
#include <core/math/matrix33.hpp>
#include <core/math/matrix44.hpp>
// #include <core/math/inaccurate.hpp>

using namespace basket;

//TODO: Implement proper tests for floating points.

TEST_CASE("test_math", "[Math]") {
    

    SECTION("Vector2")
    {
        SECTION("Operator overload Vector2")
        {
            REQUIRE(Vector2(1, 0) == Vector2(1, 0));
            REQUIRE(Vector2(1, 0) != Vector2(4, 3));
            REQUIRE(Vector2(1, 2) + Vector2(4, 5) == Vector2(5, 7));
            REQUIRE(Vector2(10, 9) - Vector2(1, 1) == Vector2(9, 8));
            REQUIRE(Vector2(2.5f, 4) * 2.0f == Vector2(5, 8));
            REQUIRE(2.0f * Vector2(2.5f, 4) == Vector2(5, 8));
            REQUIRE(Vector2(6, 3) / 2.0f == Vector2(3, 1.5f));
            REQUIRE(-Vector2(5, -7) == Vector2(-5, 7));
            REQUIRE(+Vector2(5, -7) == Vector2(5, -7));

            Vector2 v0(4, 4);
            v0 -= Vector2(1, 1);
            REQUIRE(v0 == Vector2(3, 3));

            Vector2 v1(1, 2);
            v1 += Vector2(-2, 1);
            REQUIRE(v1 == Vector2(-1, 3));

            Vector2 v2(-3, -1);
            v2 *= 3.0f;
            REQUIRE(v2 == Vector2(-9, -3));

            Vector2 v3(-9, -6);
            v3 /= 3.0f;
            REQUIRE(v3 == Vector2(-3, -2));
        }

        SECTION("Constants")
        {
            REQUIRE(Vector2::zero       == Vector2(0, 0));
            REQUIRE(Vector2::one        == Vector2(1, 1));
            REQUIRE(Vector2::left       == Vector2(-1, 0));
            REQUIRE(Vector2::right      == Vector2(1, 0));
            REQUIRE(Vector2::up         == Vector2(0, 1));
            REQUIRE(Vector2::down       == Vector2(0, -1));
        }

        SECTION("Operations")
        {
            REQUIRE(bmath::magnitude(Vector2(0, -3)) == 3);
            REQUIRE(bmath::dot(Vector2(1, 0), Vector2(1, 0)) == 1.0f);
            REQUIRE(bmath::distance(Vector2(0, 1), Vector2(0, -1)) == 2.0f);
            REQUIRE(bmath::distance_square(Vector2(0, 1), Vector2(0, -1)) == 4.0f);
            REQUIRE(bmath::is_normalized(Vector2(0, 1)) == true);
            REQUIRE(bmath::normalized(Vector2(0, 100)) == Vector2::up);
            Vector2 n = Vector2(0, -100);
            bmath::normalize(n);

            REQUIRE(n == Vector2::down);
            REQUIRE(bmath::is_normalized(n));
        }
    }

    SECTION("Vector3")
    {
        SECTION("Operator overload")
        {
            REQUIRE(Vector3(1, 0, 1) == Vector3(1, 0, 1));
            REQUIRE(Vector3(1, 0, 3) != Vector3(4, 3, 6));
            REQUIRE(Vector3(1, 2, 3) + Vector3(4, 5, 6) == Vector3(5, 7, 9));
            REQUIRE(Vector3(10, 9, 4) - Vector3(1, 1, 1) == Vector3(9, 8, 3));
            REQUIRE(Vector3(2.5f, 4, 2) * 2.0f == Vector3(5, 8, 4));
            REQUIRE(Vector3(6, 3, 8) / 2.0f == Vector3(3, 1.5f, 4));
            REQUIRE(-Vector3(5, -7, 2) == Vector3(-5, 7, -2));
            REQUIRE(+Vector3(5, -7, 2) == Vector3(5, -7, 2));

            Vector3 v0(4, 4, 4);
            v0 -= Vector3(1, 1, 1);
            REQUIRE(v0 == Vector3(3, 3, 3));

            Vector3 v1(1, 2, 3);
            v1 += Vector3(-2, 1, 0);
            REQUIRE(v1 == Vector3(-1, 3, 3));

            Vector3 v2(-3, -1, 5);
            v2 *= 3.0f;
            REQUIRE(v2 == Vector3(-9, -3, 15));

            Vector3 v3(-9, -6, 0);
            v3 /= 3.0f;
            REQUIRE(v3 == Vector3(-3, -2, 0));
        }

        SECTION("Constants")
        {
            REQUIRE(Vector3::zero       == Vector3(0, 0, 0));
            REQUIRE(Vector3::one        == Vector3(1, 1, 1));
            REQUIRE(Vector3::left       == Vector3(-1, 0, 0));
            REQUIRE(Vector3::right      == Vector3(1, 0, 0));
            REQUIRE(Vector3::forward    == Vector3(0, 0, -1));
            REQUIRE(Vector3::backward   == Vector3(0, 0, 1));
            REQUIRE(Vector3::up         == Vector3(0, 1, 0));
            REQUIRE(Vector3::down       == Vector3(0, -1, 0));
        }

        SECTION("Operations")
        {
            REQUIRE(bmath::magnitude(Vector3(0, -3, 0)) == 3);
            REQUIRE(bmath::dot(Vector3(1, 0, 0), Vector3(1, 0, 0)) == 1.0f);
            REQUIRE(bmath::distance(Vector3(0, 1, 0), Vector3(0, -1, 0)) == 2.0f);
            REQUIRE(bmath::distance_square(Vector3(0, 1, 0), Vector3(0, -1, 0)) == 4.0f);
            REQUIRE(bmath::is_normalized(Vector3(0, 1, 0)) == true);
            REQUIRE(bmath::normalized(Vector3(0, 100, 0)) == Vector3::up);
            REQUIRE(bmath::cross(Vector2::right, Vector2::up) == Vector3::backward);
            REQUIRE(bmath::cross(Vector2::up, Vector2::right) == Vector3::forward);

            REQUIRE(bmath::cross(Vector3::right, Vector3::up) == Vector3::backward);
            REQUIRE(bmath::cross(Vector3::up, Vector3::left) == Vector3::backward);
            REQUIRE(bmath::cross(Vector3::left, Vector3::up) == Vector3::forward);
            REQUIRE(bmath::cross(Vector3::forward, Vector3::right) == Vector3::down);
            REQUIRE(bmath::cross(Vector3::right, Vector3::forward) == Vector3::up);
            REQUIRE(bmath::cross(Vector3::up, Vector3::forward) == Vector3::left);
            REQUIRE(bmath::cross(Vector3::backward, Vector3::down) == Vector3::right);
            REQUIRE(bmath::cross(Vector3(1, 3, 4), Vector3(2, -5, 8)) == Vector3(44, 0, -11));
            REQUIRE(bmath::dot(bmath::cross(Vector3::right, Vector3::up), Vector3::up) == 0.0f);
            // REQUIRE(bmath::rotated2(Vector3::right, bmath::pi, Vector3::backward) == Vector3::left); // it works
            // REQUIRE(bmath::rotated2(Vector3::right * 2, bmath::pi / 2, Vector3::backward) == Vector3::up * 2);
            // REQUIRE(bmath::rotated(Vector3::right, Vector3::backward, bmath::pi) == Vector3::left);
            Vector3 n = Vector3(0, -100, 0);
            bmath::normalize(n);

            REQUIRE(n == Vector3::down);
            REQUIRE(bmath::is_normalized(n));
        }
    }

    SECTION("Vector4")
    {
        SECTION("Comparison")
        {
            REQUIRE(Vector4(1, 1, 1, 1) != Vector4(0, 0, 0, 0));
            REQUIRE(Vector4(1, 1, 1, 1) == Vector4(1, 1, 1, 1));
            REQUIRE(Vector4(1, 0, 1, 5) != Vector4(1, 1, 1, 1));
            REQUIRE(Vector4(1, 0, 1, 5) != Vector4(3, 3, 3, 3));
            REQUIRE(Vector4(1, 0, 1, 5) == Vector4(1, 0, 1, 5));
        }

        SECTION("Constants")
        {
            REQUIRE(Vector4(1, 1, 1, 1) == Vector4::one);
            REQUIRE(Vector4(0, 0, 0, 0) == Vector4::zero);
        }

        SECTION("Operators")
        {
            REQUIRE(Vector4(1, 1, 1, 1) + Vector4(1, 1, 1, 1) == Vector4(2, 2, 2, 2));
            REQUIRE(Vector4(1, 1, 1, 1) - Vector4(1, 1, 1, 1) == Vector4::zero);

            Vector4 v0(3, 7, 5, 6);
            v0 += Vector4(10, 10, 10, 10);
            REQUIRE(v0 == Vector4(13, 17, 15, 16));
            v0 -= Vector4(5, 5, 5, 5);
            REQUIRE(v0 == Vector4(8, 12, 10, 11));
            v0 *= 2.0f;
            REQUIRE(v0 == Vector4(8, 12, 10, 11) * 2);
            v0 /= 3;
            REQUIRE(v0 == (Vector4(8, 12, 10, 11) * 2) / 3);

            REQUIRE(Vector4(1, 1, 1, 1) * 4 == Vector4(4, 4, 4, 4));
        }

        SECTION("Funcionts")
        {
            REQUIRE(bmath::dot(Vector4(1, 1, 1, 1), Vector4(1, 1, 1, 1)) == 4);
        }
    }

    SECTION("Matrix22")
    {
        Matrix22 m(1);

        REQUIRE(m != Matrix22::identity);
        REQUIRE(m == Matrix22(1, 1, 1, 1));
        Matrix22 m1(4, -10, 3, 0);

        REQUIRE(m1 + m == Matrix22(5, -9, 4, 1));
        REQUIRE(m1 - m == Matrix22(3, -11, 2, -1));
        REQUIRE(bmath::transposed(bmath::transposed(m1)) == m1);
        REQUIRE(bmath::transposed(m1) == Matrix22(4, 3, -10, 0));
        bmath::transpose(m1);
        REQUIRE(m1 == Matrix22(4, 3, -10, 0));
        REQUIRE(m1 * Matrix22::identity == m1);
        REQUIRE(Matrix22::identity * m1 == m1);
        REQUIRE(m1 * 0.0f == Matrix22::zero);
        REQUIRE(0 * m1 == Matrix22::zero);
        REQUIRE(1.0f * m1 == m1);
        REQUIRE(10.0f * m1 == Matrix22(40, 30, -100, 0));
        // Matrix22 m4 = bmath::mat(0.0f);
        // REQUIRE(Matrix22::identity == bmath::matrix22(0.0f));
        // REQUIRE(bmath::matrix22(bmath::pi) == Matrix22(cos(bmath::pi), sin(bmath::pi), -sin(bmath::pi), cos(bmath::pi)));
        // bmath::mat(0.0f);
    }

    SECTION("Matrix33")
    {
        SECTION("operators")
        {

            Matrix33 m({1, 1, 1},{1, 1, 1}, {1, 1, 1});

            REQUIRE(m == Matrix33::one);
            REQUIRE(m != Matrix33::identity);

            Matrix33 m0(1, -5, 3, 0, -2, 6, 7, 2, -4);
            Matrix33 m1(-8, 6, 1, 7, 0, -3, 2, 4, 5);

            Matrix33 mr(-37, 18, 31, -2, 24, 36, -50, 26, -19);

            REQUIRE(m0 * m1 == mr);

            REQUIRE(Matrix33(5) + Matrix33(5) == Matrix33(10));
            REQUIRE(Matrix33(5) - Matrix33(1) == Matrix33(4));
            REQUIRE(Matrix33(5) * 10 == Matrix33(50));
            REQUIRE(Matrix33(5.0f) / 2.0f == Matrix33(2.5f));

            Matrix33 m2(15);


            REQUIRE((m0 *= m1, m0) == mr);
            REQUIRE((m2 *= 10, m2) == Matrix33(150));
            REQUIRE((m2 /= 10, m2) == Matrix33(15));
            REQUIRE((m2 += Matrix33(10), m2) == Matrix33(25));
            REQUIRE((m2 -= Matrix33(10), m2) == Matrix33(15));
        }

        SECTION("functions")
        {

            Matrix33 m1 = Matrix33(-4, -3, 3, 0, 2, -2, 1, 4, -1);
            Matrix33 m2 = Matrix33(-4, -3, 3, 3, 2, -2, 1, 4, -1);

            REQUIRE(bmath::determinant(m1) == -24);
            REQUIRE(bmath::adjacent(m1) == Matrix33(6, 9, 0, -2, 1, -8, -2, 13, -8));
            REQUIRE(bmath::inversed(Matrix33::identity) == Matrix33::identity);
            // REQUIRE(m1 * bmath::inversed(m1) == Matrix33::identity);
            REQUIRE(bmath::inversed(bmath::inversed(m1)) == m1);
            REQUIRE(bmath::inversed(bmath::transposed(m1)) == bmath::transposed(bmath::inversed(m1)));
            // REQUIRE(bmath::inversed(m1 * m2) == bmath::inversed(m2) * bmath::inversed(m1)) // It works!!!
            REQUIRE(bmath::determinant(bmath::inversed(m1)) == static_cast<Matrix33::value_type>(1) / bmath::determinant(m1));
            

        }
    }

    SECTION("Matrix44")
    {
        Matrix44 m({1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1});

        REQUIRE(m == Matrix44::one);
        REQUIRE(m != Matrix44::identity);

        Matrix44 m0(4, -1, 0, 10, 8, -2, -7, -1, -3, 9, 1, 1, 0, 2, 3, 5);
        Matrix44 m1(12, -10, 0, 1, 9, 7, -8, 3, -1, -1, 9, 4, 10, 13, 11, 0);

        Matrix44 mr(139, 83, 118, 1, 75, -100, -58, -26, 54, 105, -52, 28, 65, 76, 66, 18);

        REQUIRE(m0 * m1 == mr);

        SECTION("Functions")
        {
            Matrix44 m1(1, 4, 2, 3, 0, 1, 4, 4, -1, 0, 1, 0, 2, 0, 4, 1);
            Matrix44 m2(4, 3, 2, 2, 0, 1, -3, 3, 0, -1, 3, 3, 0, 3, 1, 1);
            REQUIRE(bmath::determinant(m1) == 65);
            REQUIRE(bmath::determinant(m2) == -240);
            REQUIRE(bmath::determinant(m1 * m2) == bmath::determinant(m1) * bmath::determinant(m2));
            REQUIRE(bmath::determinant(Matrix44({1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1})) == 1);

            REQUIRE(bmath::transposed(bmath::transposed(m1)) == m1);
            REQUIRE(bmath::transposed(m1) != m1);
            // REQUIRE(bmath::determinant(m1) == bmath::determinant(bmath::transposed(m1))); // For some reasno this test fails.
            REQUIRE(bmath::determinant(bmath::transposed(m2)) == bmath::determinant(m2));
            
        }
    }

    SECTION("Inaccurate")
    {
        // REQUIRE(bmath::unprecise_sqrt2(50, 10) == sqrt(50));
    }


    
}
