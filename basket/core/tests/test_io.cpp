#include <catch2/catch_test_macros.hpp>

#include <core/io/file.hpp>

using namespace basket;

TEST_CASE("IO", "[io]") {

    SECTION("[File]")
    {
        BK_LOG(core, info, "Current path: {0}", std::filesystem::current_path().string());

        File file;
        REQUIRE(file.open("core/tests/hello.txt") == Result::success);

        string content = file.get_content();

        REQUIRE(content == "hello");
        
    }

}