#include <catch2/catch_test_macros.hpp>

#include <core/function.hpp>
#include <core/logging.hpp>

#include <functional>
#include <utility>

BK_DECLARE_LOG(core);

using namespace basket;



template<typename F, size_t... I, typename R, typename G>
constexpr size_t foo2(F, index_sequence<I...> p_s, R, G)
{
    return p_s.size();
}

template<size_t... I>
size_t bar(index_sequence<I...> p_s)
{
    return p_s.size();
}

TEST_CASE("Testing containers")
{
    
    
}