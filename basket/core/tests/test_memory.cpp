#include <catch2/catch_test_macros.hpp>

#include <core/memory/arena.hpp>

using namespace basket;

TEST_CASE("Testing memory")
{
    SECTION("Arena")
    {
        Arena<int> aint(10);

        REQUIRE(!aint.empty());
        REQUIRE(aint.size() == 10);

        for (size_t index = 0; index < aint.size(); ++index)
        {
            *aint.get(index) = index;
        }

        for (size_t index = 0; index < aint.size(); ++index)
        {
            REQUIRE(*aint.get(index) == index);
        }
    }
}