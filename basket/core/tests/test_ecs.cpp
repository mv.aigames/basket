#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <basket.hpp>

using namespace basket;

TEST_CASE("ECS", "Entity")
{
    SECTION("Entity")
    {
        ecs::Entity* e1 = World::get().get_notebook().create_entity();
        ecs::Entity* e2 = World::get().get_notebook().create_entity();

        REQUIRE(1 == 1);
        REQUIRE(*e1 != *e2);
        REQUIRE(!(*e1 == *e2));
        World::get().get_notebook().destroy_entity(*e2);
    }

    SECTION("Components")
    {
        ecs::Notebook& notebook = World::get().get_notebook();
        ecs::Entity* e1 = notebook.create_entity();
        _2d::TransformComponent* tc = notebook.add_component<_2d::TransformComponent>(*e1);
        tc->position = Vector2(0, 1);
        {
            _2d::TransformComponent* ptc = notebook.get_component<_2d::TransformComponent>(*e1);
            REQUIRE(tc == ptc);
            tc->position = Vector2(4);
            REQUIRE(ptc->position == Vector2(4));
        }

        // tc.position.x = -1;
        REQUIRE(tc->position.x == 4);

        REQUIRE(notebook.has_component<_2d::TransformComponent>(*e1));
        // Just to be sure.
        REQUIRE(!notebook.remove_component<_2d::SquareColliderComponent>(*e1));
        REQUIRE(notebook.remove_component<_2d::TransformComponent>(*e1));

        REQUIRE(!notebook.has_component<_2d::TransformComponent>(*e1));
    }
}