#ifndef BASKET_STREAMBUF_HPP
#define BASKET_STREAMBUF_HPP

#ifdef BASKET_STD
    #include <streambuf>
    namespace basket = std;
#else
    #include "_std/_streambuf.hpp"
#endif

#endif