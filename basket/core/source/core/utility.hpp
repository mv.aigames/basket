#ifndef BASKET_UTILITY_HPP
#define BASKET_UTILITY_HPP

#ifdef BASKET_STD
    #include <utility>
    namespace basket = std;
#else
    #include "_std/_utility.hpp"
#endif

#endif