#ifndef BASKET_FUNCTIONAL_HPP
#define BASKET_FUNCTIONAL_HPP

#ifdef BASKET_STD
    #include <functional>
    namespace basket = std;
#else
    #include "_std/_functional.hpp"
#endif

#endif