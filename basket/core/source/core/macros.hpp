#ifndef BASKET_MACROS_HPP
#define BASKET_MACROS_HPP

#include "pch.hpp"

#define BK_SAFE_RETURN(cond) \
    if (cond)\
        return;\
    else\
        (void)0;
    

#define BK_VARIABLE_WITH_DEFAULT(TYPE, NAME, DEFAULT_VALUE) TYPE NAME = DEFAULT_VALUE
    
#define BK_ASSERT(cond, message) assert((cond) && message)

#ifdef BASKET_WINDOWS
    #define BK_NOVTABLE __declspec(novtable)
    #undef far
    #undef near
    #ifdef BASKET_SHARED
        // #ifdef BASKET_EXPORT
            #define BASKET_API __declspec(dllexport)
        // #else
        //     #define BASKET_API __declspec(dllimport)
        // #endif
    #else
        #define BASKET_API
    #endif
    #define BK_FORCEINLINE __forceinline
#else
    #define BK_NOVTABLE
    #define BK_FORCEINLINE __attribute__((always_inline))
    #define BASKET_API
    #error "Only windows is supported"
#endif

#endif