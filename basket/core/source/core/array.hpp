#ifndef BASKET_ARRAY_HPP
#define BASKET_ARRAY_HPP

#ifdef BASKET_STD
    #include <array>
    namespace basket = std;
#else
    #include "_std/_array.hpp"
#endif

#endif