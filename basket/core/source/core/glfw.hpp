#ifndef BASKET_GLFW_HPP
#define BASKET_GLFW_HPP

// VK_USE_PLATFORM_WIN32_KHR GLFW_EXPOSE_NATIVE_WIN32
#ifdef BASKET_WINDOWS
    #define GLFW_EXPOSE_NATIVE_WIN32
    #define GLFW_INCLUDE_NONE
#else
    #error "Only windows is supported"
#endif

    #include <GLFW/glfw3.h>
    #include <GLFW/glfw3native.h>

#endif