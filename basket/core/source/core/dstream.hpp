/**
 * @file dstream.hpp
 * @author jordan motta (you@domain.com)
 * @brief dstream is a more OS way to handle files. Windows and unix-like systems
 * use a numeric value to identify a file. dstream is almost identical to fstream, but
 * the key difference relays on the streambuf. It doesn't use basic_streambuf base-class,
 * but a new streambuf due t o the incopatibility with pointers.
 * 
 * @version 0.1
 * @date 2023-08-27
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef BASKET_DSTREAM_HPP
#define BASKET_DSTREAM_HPP

#include "_std/_dstream.hpp"

#endif