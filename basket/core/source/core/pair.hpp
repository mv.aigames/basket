#ifndef BASKET_PAIR_HPP
#define BASKET_PAIR_HPP

#ifdef BASKET_STD
    #include <pair>
    namespace basket = std;
#else
    #include "_std/_pair.hpp"
#endif

#endif