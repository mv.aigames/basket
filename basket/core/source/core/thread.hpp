#ifndef BASKET_THREAD_HPP
#define BASKET_THREAD_HPP

#ifdef BASKET_STD
    #include <thread>
    namespace basket = std;
#else
    #include "_std/_thread.hpp"
#endif

#endif