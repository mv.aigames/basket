#ifndef BASKET_UUID_HPP
#define BASKET_UUID_HPP

#include "pch.hpp"

namespace basket
{
    class UUID
    {
        public:
            using integer_type = uint64_t;
        public:
            UUID();
            UUID(const UUID& p_other);
            UUID& operator= (const UUID& p_other);
            UUID(UUID&& p_other);
            UUID& operator=(UUID&& p_other);
            integer_type get() const;
        private:
            integer_type id;
            static constexpr integer_type invalid_id = 0;
            friend bool operator== (const UUID& lhs, const UUID& rhs);
            friend bool operator< (const UUID& lhs, const UUID& rhs);
    };

    // constexpr UUID::integer_type invalid_id = 0;
}

#endif