#ifndef BASKET_FILESYSTEM_HPP
#define BASKET_FILESYSTEM_HPP

#ifdef BASKET_STD
    #include <filesystem>
    namespace basket = std;
#else
    #include "_std/_filesystem.hpp"
#endif

#endif