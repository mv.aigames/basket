#ifndef BASKET_STACK_HPP
#define BASKET_STACK_HPP

#ifdef BASKET_STD
    #include <stack>
    namespace basket = std;
#else
    #include "_std/_stack.hpp"
#endif

#endif