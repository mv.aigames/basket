#ifndef BASKET_ITERATOR_HPP
#define BASKET_ITERATOR_HPP

#ifdef BASKET_STD
    #include <iterator>
    namespace basket = std;
#else
    #include "_std/_iterator.hpp"
#endif

#endif