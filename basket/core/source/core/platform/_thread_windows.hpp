#ifndef BASKET__THREAD_WINDOWS_HPP
#define BASKET__THREAD_WINDOWS_HPP

#include "../_std/_utility.hpp"
#include "../_std/_tuple.hpp"
#include "../_std/_memory/unique_ptr.hpp"

#include <Windows.h>

namespace basket
{

    class thread;

    



    class _thread_exec
    {
        public:
            thread* _thread;
            _thread_exec(thread* p_value)
            {
                _thread = p_value;
            }

            _thread_exec() = default;
            _thread_exec(const _thread_exec&) = default;

            
    };

    

    

    class thread
    {

        friend class _thread_exec;
        private:
            bool _executing = true;
        public:
            class id
            {
                public:
                    DWORD _id;

                   

                    constexpr bool operator==(const id& p_other)
                    {
                        return _id == p_other._id;
                    }
            };

            void _finish()
            {
                _executing = false;
            }

            // _thread_exec _exec;

            id _id;
            HANDLE _handle;
        
        private:
            void _wait()
            {
                WaitForSingleObject(_handle, INFINITE);
            }

            void _clear()
            {
                if (_pointer)
                {
                    delete _pointer;
                }
            }

            void* _pointer = nullptr;
        public:
            
            void join()
            {
                _wait();
            }

        public:
            template<typename F, typename... Args>
            thread(F&& p_callabe, Args&&... p_args)
            {

                tuple<thread*, decay_t<F>, Args...>* t = new tuple<thread*, decay_t<F>, Args...>(this, &p_callabe, move(p_args)...);
                _pointer = t;
                _executing = true;
                _handle = CreateThread(NULL, 0, __wrapper_call<F, Args...>, t, 0, &_id._id);
                if (_handle == NULL)
                {
                    BK_LOG(core, error, "Cannot create thread");
                    _clear();
                }
            }

            ~thread()
            {
                if (_handle != NULL)
                {
                    _wait();
                    _clear();
                    CloseHandle(_handle);
                }
            }
    };


    template<typename ThreadType, typename _CallerType, typename... Args, size_t... I>
    void _thread_caller(tuple<ThreadType, _CallerType, Args...>& p_tuple, index_sequence<I...>)
    {
        using tt = tuple<Args...>;
        _CallerType& _call = get<1>(p_tuple);
        _call(forward<tuple_element_t<I , tt>>(get<I + 2>(p_tuple))...);
    }

    template<typename F, typename... Args>
    DWORD __stdcall __wrapper_call(void* p_user_args)
    {
        using tt = tuple<thread*, F, Args...>;
        tt* v = reinterpret_cast<tt*>(p_user_args);
        using sequence = make_index_sequence<tuple_size_v<tt> - 2>;
        _thread_caller(*v, sequence());
        get<0>(*v)->_finish();
        return 0;
    }
    
}


#endif
