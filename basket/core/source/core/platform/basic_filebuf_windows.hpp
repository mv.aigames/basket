#ifndef BASKET_BASIC_FILEBUF_WINDOWS_HPP
#define BASKET_BASIC_FILEBUF_WINDOWS_HPP

#include "../_std/_io/_ios_base.hpp"
#include "../_std/_string/_char_traits.hpp"
#include <windows.h>

namespace basket
{

    inline DWORD _openmode_to_generic_open(ios_base::openmode p_modes)
    {
        

        return 0;
    }

    static constexpr char _new_line = '\n';

    template<typename CharType, typename Traits = char_traits<CharType>>
    class basic_filebuf: public basic_streambuf<CharType, Traits>
    {

        private:
            HANDLE _handle;
            OVERLAPPED _overlapped;

        private:
            using _bf = basic_filebuf<CharType, Traits>;
            using _base = basic_streambuf<CharType, Traits>;


        public:
            using traits_type   = Traits;
            using char_type     = typename _base::char_type;
            using int_type      = typename _base::int_type;
            using off_type      = typename _base::off_type;
            using pos_type      = typename _base::pos_type;

        // Positioning
        protected:

            virtual _base* setbuf(char_type* p_s, streamsize p_n)
            {
                return this;
            }

            virtual pos_type seekoff(
                                off_type p_off,
                                ios_base::seekdir p_dir,
                                ios_base::openmode p_which = ios_base::in | ios_base::out)
            {
                return {};
            }

            virtual pos_type seekpos(
                                off_type p_off,
                                ios_base::openmode p_which = ios_base::in | ios_base::out)
            {
                return {};
            }

            virtual streamsize xsgetn(char_type* p_s, streamsize p_count) override
            {
                return 0;
            }
            
        private:
            _bf* _open(const char* p_filename, ios_base::openmode p_mode)
            {
                if (is_open())
                {
                    return nullptr;
                }


                DWORD flags = _openmode_to_generic_open(p_mode);
                _handle = CreateFile(p_filename,
                                    flags,
                                    FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
                                    NULL,
                                    OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL,
                                    NULL);
                
                if (_handle == INVALID_HANDLE_VALUE)
                {
                    BK_LOG(core, info, "Failed to open file: {0},  Error code: {1}", p_filename , GetLastError());
                    return nullptr;
                }

                return this;
            }

            void _close()
            {
                if (is_open())
                {
                    CloseHandle(_handle);
                }
            }

        public:
            bool is_open() const
            {
                return _handle != INVALID_HANDLE_VALUE;
            }

            _bf* open(const char* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {
                return _open(p_filename, p_mode);
                
            }

            _bf* open(const string& p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {
                return _open(p_filename.c_str(), p_mode);
            }

        public:
            basic_filebuf()
            {
                _handle = INVALID_HANDLE_VALUE;
            }

            basic_filebuf(_bf&&)
            {

            }

            basic_filebuf(const _bf&) = delete;
    };
}

#endif