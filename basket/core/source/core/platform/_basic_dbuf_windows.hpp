#ifndef BASKET_BASIC_DBUF_WINDOWS_HPP
#define BASKET_BASIC_DBUF_WINDOWS_HPP

#include "../_std/_io/_ios_base.hpp"
#include "../_std/_string/_char_traits.hpp"
#include "../_std/_stream/_basic_streambuf.hpp"
#include <windows.h>

namespace basket
{

    inline DWORD _openmode_to_generic_open(ios_base::openmode p_modes)
    {
        DWORD generics = 0;

        if ((p_modes & ios_base::in) != 0)
        {
            generics = GENERIC_READ;
        }

        if ((p_modes & ios_base::out) != 0 || (p_modes & ios_base::trunc) != 0)
        {
            generics |= GENERIC_WRITE;
        }

        return generics;
    }

    inline DWORD _get_openmode(ios_base::openmode p_mode)
    {
        DWORD mode = OPEN_EXISTING;

        if ((p_mode & ios_base::out) != 0 && (p_mode & ios_base::trunc) == 0)
        {
            mode = OPEN_ALWAYS;
        }

        if ((p_mode & ios_base::out) != 0 && (p_mode & ios_base::trunc) != 0)
        {
            mode = TRUNCATE_EXISTING;
        }

        return mode;
    }

    static constexpr char _new_line = '\n';

    template<typename CharType, typename Traits = char_traits<CharType>>
    class basic_dbuf: public basic_streambuf<CharType, Traits>
    {
        public:
            using traits_type = Traits;
            using char_type = traits_type::char_type;
            using int_type = traits_type::int_type;
            using off_type = traits_type::off_type;
            using pos_type = traits_type::pos_type;

        private:
            HANDLE _handle;
            OVERLAPPED _overlapped;
            // OVERLAPPED _out_overlapped;

        private:
            using _bf = basic_dbuf<CharType, Traits>;
            using _base = basic_dbuf<CharType, Traits>;

        private:
            union __move_pos
            {
                struct
                {
                    LONG high;
                    LONG low;
                };

                LONG64 value;
            };

            LONG64 __move_file_pos(off_type p_off, ios_base::seekdir p_dir, DWORD p_pos)
            {
                __move_pos _mpos;
                _mpos.low = SetFilePointer(_handle, p_off, &_mpos.high, p_pos);
                return _mpos.value;
            }


        

        // Positioning
        protected:

            virtual _base* setbuf(char_type* p_s, streamsize p_n)
            {
                return this;
            }

            virtual pos_type seekoff(
                                off_type p_off,
                                ios_base::seekdir p_dir,
                                ios_base::openmode p_which = ios_base::in | ios_base::out)
            {
                if (p_dir == ios_base::beg)
                {
                    __move_file_pos(p_off, p_dir, FILE_BEGIN);
                }

                if (p_dir == ios_base::cur)
                {
                    __move_file_pos(p_off, p_dir, FILE_CURRENT);
                    // __move_pos _mpos;
                    // _mpos.low = SetFilePointer(_handle, p_off, &_mpos.high, FILE_CURRENT);
                    // return _mpos.value;
                }

                if (p_dir == ios_base::end)
                {
                    __move_file_pos(p_off, p_dir, FILE_END);
                    // __move_pos _mpos;
                    // _mpos.low = SetFilePointer(_handle, p_off, &_mpos.high, FILE_CURRENT);
                    // return _mpos.value;
                }

                return {};
            }

            virtual pos_type seekpos(
                                off_type p_off,
                                ios_base::openmode p_which = ios_base::in | ios_base::out)
            {
                return {};
            }

            virtual streamsize xsgetn(char_type* p_s, streamsize p_count)
            {
                DWORD _read;
                BOOL s = ReadFile(_handle, p_s, p_count, &_read, NULL);
                if (!s)
                {
                    BK_LOG(core, error, "Error reading file. Error code {0}", GetLastError());
                }

                return static_cast<streamsize>(_read);
            }

            virtual streamsize xsputn(const char_type* p_s, streamsize p_count)
            {
                DWORD written;
                BOOL s = WriteFile(_handle, p_s, p_count, &written, NULL);
                if (s == FALSE)
                {
                    BK_LOG(core, error, "Failed to write file. Error code: {0}", GetLastError());
                }
                return static_cast<streamsize>(written);
            }
            
        private:
            _bf* _open(const char* p_filename, ios_base::openmode p_mode)
            {
                if (is_open())
                {
                    return nullptr;
                }


                DWORD generics = _openmode_to_generic_open(p_mode);
                DWORD openmode = _get_openmode(p_mode);
                _handle = CreateFile(p_filename,
                                    generics,
                                    FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
                                    NULL,
                                    openmode,
                                    FILE_ATTRIBUTE_NORMAL,
                                    NULL);
                
                if (_handle == INVALID_HANDLE_VALUE)
                {
                    BK_LOG(core, info, "Failed to open file: {0},  Error code: {1}", p_filename , GetLastError());
                    return nullptr;
                }

                return this;
            }

            void _close()
            {
                if (is_open())
                {
                    CloseHandle(_handle);
                }
            }

        public:
            bool is_open() const
            {
                return _handle != INVALID_HANDLE_VALUE;
            }

            _bf* open(const char* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {
                return _open(p_filename, p_mode);
                
            }

            _bf* open(const string& p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {
                return _open(p_filename.c_str(), p_mode);
            }

            void close()
            {
                _close();
            }

        public:
            basic_dbuf()
            {
                _handle = INVALID_HANDLE_VALUE;
            }

            basic_dbuf(_bf&&)
            {

            }

            basic_dbuf(const _bf&) = delete;
    };
}

#endif