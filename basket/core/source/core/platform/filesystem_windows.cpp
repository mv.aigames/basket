#include <core/filesystem.hpp>

#include <windows.h>

BK_DECLARE_LOG(core);

namespace basket::filesystem
{
    

    static string _replace(const string& p_s, char p_old, char p_new)
    {
        string s(p_s);
        string::size_type pos = s.find(p_old, 0);
        while (pos != string::npos)
        {
            s[pos] = p_new;
            pos = s.find(p_old, pos);
        }

        return s;
    }

    // It is the native path to exe.
    static string _execution_path;

    
    // This should be in module::pre_init
    string get_process_path()
    {
        return _execution_path;
    }

    Result create_file(const path& p_path)
    {
        if (!p_path.is_valid())
        {
            return Result::filepath_failed;
        }


        const string& s = p_path.get_path_native();
        const CHAR* p = s.c_str();
        HANDLE hdl = CreateFile(p,
            GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
            NULL,
            CREATE_ALWAYS,
            FILE_ATTRIBUTE_NORMAL,
            NULL);
        
        CloseHandle(hdl);
        return Result::success;
    }

    // _FileHandle open_file(const path& p_path, OpenMode p_flags)
    // File::handle_type open_file(const path& p_path)
    // {
    //     if (!p_path.is_valid())
    //     {
    //         return {};
    //     }

    //     // decltype(GENERIC_WRITE) flags = _OpenMode_to_generic_open(p_flags & OpenMode::Read) | _OpenMode_to_generic_open(p_flags & OpenMode::Write);
    //     decltype(GENERIC_WRITE) flags = 0;

    //     const string& s = p_path.get_path_native();
    //     const CHAR* p = s.c_str();
    //     HANDLE hdl = CreateFile(p,
    //         flags,
    //         FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
    //         NULL,
    //         OPEN_EXISTING,
    //         FILE_ATTRIBUTE_NORMAL,
    //         NULL);

        
    //     return {};
    // }

    Result move(const path& p_from, const path& p_to)
    {
        const CHAR* from = p_from.get_path_native().c_str();
        const CHAR* to = p_to.get_path_native().c_str();

        if (!MoveFile(from, to))
        {
            return Result::failed;
        }

        return Result::success;
    }

    Result copy_file(const path& p_from, const path& p_to, bool p_fail_if_exists)
    {
        const CHAR* from = p_from.get_path_native().c_str();
        const CHAR* to = p_to.get_path_native().c_str();

        if (!CopyFile(from, to, p_fail_if_exists))
        {
            return Result::failed;
        }

        return Result::success;
    }

    Result init()
    {
        CHAR path[MAX_PATH];

        GetModuleFileName(NULL, path, MAX_PATH);

        string s(path);

        string::size_type pos = s.rfind('\\', s.size());
        s = s.substr(0, pos);

        // BK_LOG(cat, info, ("--------   ", s);
        _execution_path = s;
        return Result::success;
    }


    path::path(const string& p_path_string)
    {
        _check_path(p_path_string);
        _fix_path(p_path_string);
    }

    void path::_check_path(const string& p_path)
    {
        #ifndef BASKET_NO_MERCY
        if (p_path.find('\\') != string::npos)
        {
            BK_LOG(core, info, "Path not valid: ", p_path);
            _is_valid = false;
            return;
        }
        #else
        _is_valid = true;
        #endif
    }

    void path::_fix_path(const string& p_path)
    {
        string s;
        if (p_path[0] != '/')
        {
            s = "/" + p_path;
        }
        else
        {
            s = p_path;
        }

        string pp = get_process_path();
        // pp = _replace(pp, '/', '\\');
        string mp = "/modules" + s;
        mp = _replace(mp, '/', '\\');
        // string r = pp + mp;
        string r = pp + mp;
        _full_path_native = r;
        // _full_path_native = _replace(_full_path_native, '/', '\\');
        _full_path = s;
    }


    const string& path::get_path_native() const
    {
        return _full_path_native;
    }

    const string& path::get_path() const
    {
        return _full_path;
    }

    bool path::is_valid() const
    {
        return _is_valid;
    }
}