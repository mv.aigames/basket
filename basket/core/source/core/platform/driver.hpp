#ifndef BASKET_DRIVER_HPP
#define BASKET_DRIVER_HPP

namespace basket
{
    class IDriver
    {
        public:
            virtual void pre_init() = 0;
            virtual void init() = 0;
            virtual void post_init() = 0;

            virtual void pre_destroy() = 0;
            virtual void destroy() = 0;
            virtual void post_destroy() = 0;
    };
}

#endif