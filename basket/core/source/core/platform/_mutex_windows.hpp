#ifndef BASKET__MUTEX_WINDOWS_HPP
#define BASKET__MUTEX_WINDOWS_HPP

#include "../logging.hpp"

#include <Windows.h>

#define __SAFE_RETURN_MUTEX(_HANDLE, MESSAGE) \
    if (_HANDLE == NULL)\
    {\
        BK_LOG(core, error, MESSAGE);\
        return;\
    }\
    else (void)0;

namespace basket
{
    class mutex
    {
        private:
            HANDLE _handle;

        public:
            void lock()
            {
                __SAFE_RETURN_MUTEX(_handle, "MUTEX ERROR: No handle assigned");
                DWORD result = WaitForSingleObject(_handle, INFINITE);

                switch(result)
                {
                    case WAIT_OBJECT_0:

                        break;
                }
            }

            void unlock()
            {
                __SAFE_RETURN_MUTEX(_handle, "MUTEX ERROR: No handle assigned");

                ReleaseMutex(_handle);
            }
        
        public:
            mutex()
            {
                _handle = CreateMutex(NULL, FALSE, NULL);

                __SAFE_RETURN_MUTEX(_handle, "MUTEX ERROR: No handle assigned");
            }

            ~mutex()
            {
                __SAFE_RETURN_MUTEX(_handle, "MUTEX ERROR: No handle assigned");

                CloseHandle(_handle);
            }

    };
}

#endif