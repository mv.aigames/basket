#include <core/os.hpp>

#include <core/pch.hpp>
#include <windows.h>

BK_DECLARE_LOG(core);

namespace basket
{
    static string _vulkan_sdk_path;

    void OS::abort()
    {
        std::abort();
    }

    Result OS::execute(const string& p_name, const string& p_args)
    {
        BK_LOG(core, info, "{0} : {1}",  p_name, p_args);
        // HANDLE out_read;
        // HANDLE out_write;

        // HANDLE in_read;
        // HANDLE in_write;

        // SECURITY_ATTRIBUTES s_attributes;
        // s_attributes.nLength = sizeof(s_attributes);
        // s_attributes.bInheritHandle = TRUE;
        // s_attributes.lpSecurityDescriptor = NULL;

        // if (!CreatePipe(&out_read, &out_write, &s_attributes, 0))
        // {
        //     BK_LOG(cat, info, ("Failed to generate output!");
        //     return;
        // }

        // if (!SetHandleInformation(out_read, HANDLE_FLAG_INHERIT, 0))
        // {
        //     BK_LOG(cat, info, ("Failed to SetHandleInformation our_read!");
        //     return;
        // }

        // if (!CreatePipe(&in_read, &in_write, &s_attributes, 0))
        // {
        //     BK_LOG(cat, info, ("Failed to generate output!");
        //     return;
        // }

        // if (!SetHandleInformation(in_read, HANDLE_FLAG_INHERIT, 0))
        // {
        //     BK_LOG(cat, info, ("Failed to SetHandleInformation our_read!");
        //     return;
        // }


        PROCESS_INFORMATION pi;
        ZeroMemory(&pi, sizeof(pi));
        STARTUPINFO si;
        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);

        string full_command = p_name + " " + p_args;

        // LPSTR args = const_cast<LPSTR>(p_args.c_str());
        LPSTR command = const_cast<LPSTR>(full_command.c_str());
        if(!CreateProcess(NULL, command, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
        {
            BK_LOG(core, info, "Success to run process");
            return Result::failed;
        }
        return Result::success;
    }

    Result OS::console(const string& p_args)
    {
        if (std::system(p_args.c_str()))
        {
            return Result::failed;
        }

        return Result::success;
    }

    string OS::_get_vulkan_sdk_path_native()
    {
        string s = get_environment("VULKAN_SDK");
        return s;
    }

    Result OS::glslc(const string& p_filename, const string& p_out)
    {
        string exe = OS::get_environment("VULKAN_SDK");
        exe += "\\Bin\\glslc.exe";
        return OS::execute(exe, p_filename + " -o " + p_out);
    }
}