#ifndef BASKET_TUPLE_HPP
#define BASKET_TUPLE_HPP

#ifdef BASKET_STD
    #include <tuple>
    namespace basket = std;
#else
    #include "_std/_tuple.hpp"
#endif

#endif