#ifndef BASKET_RESULT_HPP
#define BASKET_RESULT_HPP

namespace basket
{
    // TODO: Find a place for you
    enum class Result
    {
        success,
        failed,
        filepath_failed,
        compile_shader_failed,
        shader_already_exists,
        unknown_file_format,
        system_already_added
    };
}


#endif
