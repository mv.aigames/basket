#ifndef BASKET_COLOR_HPP
#define BASKET_COLOR_HPP

#include "constraints.hpp"

namespace basket
{
    template<concept_numeric T>
    struct Color_Template
    {
        T r;
        T g;
        T b;
        T a;

        Color_Template(): r(0), g(0), b(0), a(0) {}

        Color_Template(const T p_value): r(p_value), g(p_value), b(p_value), a(p_value) {}

        Color_Template(T p_r, T p_g, T p_b, T p_a) : r(p_r), g(p_g), b(p_b), a(p_a) {}

        Color_Template(const Color_Template<T>& p_other) = default;
        Color_Template(Color_Template<T>&& p_other) = default;
        Color_Template& operator=(const Color_Template& p_other) = default;
        Color_Template& operator=(Color_Template&& p_other) = default;
        // TODO: Pasrse string to color;
        Color_Template(std::string_view p_string) {}
    };

    template<concept_numeric T>
    std::ostream& operator<<(std::ostream& os, const Color_Template<T>& p_color)
    {
        os << "(" << p_color.r << ", " << p_color.g << ", " << p_color.b << ", " << p_color.a << ")";
        return os;
    }

    using Color = Color_Template<float>;
}

#endif