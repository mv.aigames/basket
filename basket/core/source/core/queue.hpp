#ifndef BASKET_QUEUE_HPP
#define BASKET_QUEUE_HPP

#ifdef BASKET_STD
    #include <queue>
    namespace basket = std;
#else
    #include "_std/_queue.hpp"
#endif

#endif