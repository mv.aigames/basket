#ifndef BASKET_MEMORY_HPP
#define BASKET_MEMORY_HPP

#ifdef BASKET_STD
    #include <memory>
    namespace basket = std;
#else
    #include "_std/_memory.hpp"
#endif

#endif