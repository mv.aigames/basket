#ifndef BASKET_THREAD_POOL_HPP
#define BASKET_THREAD_POOL_HPP

#include <core/containers/queue.hpp>
#include <core/function.hpp>

namespace basket
{
    /**
     * @brief Only support one job at time.
     * 
     */
    template<typename F, typename Container = Queue<Function<F>>, bool only_main_thread = false>
    class ThreadPool
    {
        public:
            ThreadPool() = default;

            void update()
            {
                if (queue.empty())
                {
                    return;
                }

                _check_threads();
            }

            void push(const Function<F>& p_callable)
            {
                queue.push(p_callable);
            }

            void push(Function<F>&& p_callable)
            {
                queue.push(p_callable);
            }

            ~ThreadPool()
            {

            }


        private:
            bool _is_running_thread = false;
            Container queue;
    };
}

#endif