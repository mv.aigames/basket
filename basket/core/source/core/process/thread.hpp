#ifndef BASKET_THREAD_HPP
#define BASKET_THREAD_HPP

#include <core/pch.hpp>

namespace basket
{
    using Thread = std::thread;
}

#endif