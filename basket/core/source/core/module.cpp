#include "module.hpp"

#include <core/logging.hpp>
#include <core/filesystem.hpp>
#include <core/string.hpp>

BK_DEFINE_LOG(core);

namespace basket::core
{
    Result Module::pre_init() 
    {

        BK_LOG(core, info, "Core pre_init");
        BK_LOG(core, warning, "Core pre_init");
        BK_LOG(core, error, "Core pre_init");
        BK_LOG(core, fatal, "Core pre_init");
        filesystem::init();
        return Result::success;
    }
    Result Module::init() 
    {
        BK_LOG(core, info, "Core init");
        return Result::success;
    }
    Result Module::post_init() 
    {
        BK_LOG(core, info, "Core post_init");
        return Result::success;
    }
    Result Module::pre_destroy() 
    {
        BK_LOG(core, info, "Core pre_destroy");
        return Result::success;
    }
    Result Module::destroy() 
    {
        BK_LOG(core, info, "Core destroy");
        return Result::success;
    }
    Result Module::post_destroy() 
    {
        BK_LOG(core, info, "Core post_destroy");
        return Result::success;
    }

    void Module::update(const real_t p_delta)
    {
        // BK_LOG(core, info, "core update");
    }
}
