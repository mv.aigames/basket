#ifndef BASKET_MATH_HPP
#define BASKET_MATH_HPP

#include "math/constants.hpp"
#include "math/functions.hpp"
#include "math/vector2.hpp"
#include "math/vector3.hpp"
#include "math/vector4.hpp"
#include "math/matrix22.hpp"
#include "math/matrix33.hpp"
#include "math/matrix44.hpp"
#include "math/quaternion.hpp"

#endif