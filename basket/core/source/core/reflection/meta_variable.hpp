#ifndef BASKET_META_VARIABLE_HPP
#define BASKET_META_VARIABLE_HPP

#include <core/string.hpp>

namespace basket
{

    class MetaVariable
    {
        private:
            string name;
            size_t offset;
            void* instance;
        public:
            MetaVariable() = default;
            MetaVariable(const MetaVariable& p_other) = default;
            MetaVariable(MetaVariable&& p_other) = default;
            MetaVariable(const string& p_name, size_t p_offset);

        public:
            MetaVariable& operator=(const MetaVariable& p_other) = default;
            MetaVariable& operator=(MetaVariable&& p_other) = default;

        public:
            template<typename T> T get() { return *(reinterpret_cast<T*>(instance) + offset); }
            template<typename T> void assign(const T& p_value)
            { 
                T* p = reinterpret_cast<T*>(instance) + offset;
                *p = p_value;
            }

            void set_instance(void* p_instance);
    };

    MetaVariable::MetaVariable(const string& p_name, size_t p_offset): name(p_name), offset(p_offset) {}

    void MetaVariable::set_instance(void* p_instance)
    {
        instance = p_instance;
    }
}


#endif