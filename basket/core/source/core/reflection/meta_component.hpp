#ifndef BASKET_META_COMPONENT_HPP
#define BASKET_META_COMPONENT_HPP

#include <core/containers/hash_map.hpp>
#include <core/reflection/meta_variable.hpp>
#include <core/logging.hpp>

BK_DECLARE_LOG(core);

namespace basket
{
    class MetaComponent
    {
        private:
            size_t size;
            void* instance;
            HashMap<string, size_t> variables;

        private:
            size_t get_offset(const string& p_name)
            {
                auto it = variables.find(p_name);
                if (it != variables.end())
                {
                    return it->second;
                }

                BK_LOG(core, info, "Variable {0} not found", p_name);
                return 0;
            }
        public:
            template<typename... Args>
            MetaComponent(size_t p_size, Args&&... p_args);
            template<typename T>
            T get(const string& p_name) { return *(T*)( (char*)instance + get_offset(p_name)); }
            template<typename T>
            void set(const string& p_name, const T& p_value)
            {
                T* p = (T*)( (char*)instance + get_offset(p_name));
                *p = p_value;
            }

            ~MetaComponent()
            {
                free(instance);
            }
    };

    template<typename... Args>
    MetaComponent::MetaComponent(size_t p_size, Args&&... p_args): size(p_size)
    {
        instance = malloc(p_size);
        (variables.insert_or_assign(p_args.first, p_args.second),...);
    }
}


#define BK_REGISTER_COMPONENT_IMPLEMENTATION(COMPONENT, ...) ::basket::MetaComponent(sizeof(COMPONENT), __VA_ARGS__)

#endif