#ifndef BASKET_INITIALIZER_LIST_HPP
#define BASKET_INITIALIZER_LIST_HPP

#ifdef BASKET_STD
    #include <initializer_list>
    namespace basket = std;
#else
    #include "_std/_initializer_list.hpp"
#endif

#endif