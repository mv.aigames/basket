#ifndef BASKET_STRING_VIEW_HPP
#define BASKET_STRING_VIEW_HPP

#ifdef BASKET_STD
    #include <string_view>
    namespace basket = std;
#else
    #include "_std/_string_view.hpp"
#endif

#endif