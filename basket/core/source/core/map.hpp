#ifndef BASKET_MAP_HPP
#define BASKET_MAP_HPP

#ifdef BASKET_STD
    #include <map>
    namespace basket = std;
#else
    #include "_std/_map.hpp"
#endif

#endif