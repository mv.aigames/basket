#include "entity.hpp"

namespace basket::ecs
{
    Entity::Entity(const Entity& p_other): id(p_other.id){}

    Entity& Entity::operator=(const Entity& p_other)
    {
        id = p_other.id;
        return *this;
    }

    Entity::Entity(Entity&& p_other): id(std::move(p_other.id)) {}

    Entity& Entity::operator=(Entity&& p_other)
    {
        id = std::move(p_other.id);
        return *this;
    }

    bool operator==(const Entity& lhs, const Entity& rhs)
    {
        return lhs.id == rhs.id;
    }
}