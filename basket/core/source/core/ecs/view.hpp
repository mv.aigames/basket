#ifndef BASKET_ECS_VIEW_HPP
#define BASKET_ECS_VIEW_HPP

#include <core/pch.hpp>
#include <core/memory/memory.hpp>

namespace basket::ecs
{
    template<typename... Args>
    class View
    {
        // Defining container type.
        public:
            template<typename T>
            using container_type = std::vector<T>;

            template<typename... TupleArgs>
            using tuple_type = std::tuple<TupleArgs...>;

            using internal_type = tuple_type<container_type<Args*>...>;

            using row_type = tuple_type<Args*...>;

        // Iterator.
        public:

            struct Iterator
            {
                using iterator_category = std::forward_iterator_tag;
                using difference_type = std::ptrdiff_t;
                // using value_type = tuple_type<container_type<smart_pointer_type<Args>>...>;
                // using pointer = tuple_type<container_type<smart_pointer_type<Args>>...>*;
                // using reference = tuple_type<container_type<smart_pointer_type<Args>>...>&;
            };

        public:
            View() = default;
            View(const tuple_type<container_type<Args*>...>& p_tuple): items{p_tuple}
            {

            }

            View(tuple_type<container_type<Args*>...>&& p_tuple): items{p_tuple}
            {

            }

            template<typename T, typename... RowArgs>
            static void insert_row(internal_type& view_tuple, T*&& value, RowArgs*&&... args)
            {
                std::get<container_type<T*>>(view_tuple).emplace_back(value);
                insert_row(view_tuple, std::forward<RowArgs*>(args)...);
            }

            template<typename T>
            static void insert_row(internal_type& view_tuple, T*&& value)
            {
                std::get<container_type<T*>>(view_tuple).emplace_back(value);
            }

            void each(auto p_function)
            {
                typename std::tuple_element<0, internal_type>::type first_element = std::get<std::tuple_element<0, internal_type>::type>(items);

                for(int index = 0; index < first_element.size(); index++)
                {
                    // p_function(std::forward<smart_pointer_type>(*std::get<container_pointer_type<Args>>(items)[index])...);
                    p_function(*std::get<container_type<Args*>>(items)[index]...);
                }
            }

            constexpr size_t get_size() const
            {
                return sizeof...(Args);
            }

            size_t get_container_size() const
            {
                typename std::tuple_element<0, internal_type>::type first_element = std::get<std::tuple_element<0, internal_type>::type>(items);

                return first_element.size();
            }
        
        private:
            
            internal_type items;
    };
}

#endif