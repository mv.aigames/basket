#ifndef WORLD_HPP
#define WORLD_HPP

#include "notebook.hpp"

namespace basket
{
    class World
    {
        public:
            static World& get();
            ecs::Notebook& get_notebook();
            
        private:
            World();
            ecs::Notebook notebook;
    };
}

#endif