#include "world.hpp"
#include "notebook.hpp"

namespace basket
{
    World::World()
    {
        
    }

    World& World::get()
    {
        static World world;
        return world;
    }

    ecs::Notebook& World::get_notebook()
    {
        return notebook;
    }
}