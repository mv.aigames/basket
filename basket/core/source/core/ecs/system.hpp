#ifndef BASKET_SYSTEM_HPP
#define BASKET_SYSTEM_HPP

#include <core/core.hpp>

namespace basket
{
    class ISystem
    {

        public:
            virtual ::basket::Result init() = 0;
            virtual void update() = 0;
            virtual void shutdown() = 0;
    };

    namespace _system_internal
    {
        template <typename T, typename = int>
        struct has_get: std::false_type {};

        template<typename T>
        struct has_get<T, decltype(((void)std::is_same<T&, T::get()>::value, 0))>: std::true_type {};
    }

    template<typename T>
    concept concept_system = requires(T t)
    {
        std::is_base_of<ISystem, T>::value;
        _system_internal::has_get<T>::value;

    };
}

#endif
