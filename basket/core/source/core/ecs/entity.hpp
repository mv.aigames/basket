#ifndef BASKET_ENTITY_HPP
#define BASKET_ENTITY_HPP

#include <core/core.hpp>

namespace basket::ecs
{

    class Entity
    {
        friend class world;
        friend class Notebook;
        public:
            // Entity() = default;
            Entity(const Entity& p_other);
            Entity& operator=(const Entity& p_other);
            Entity(Entity&& p_other);
            Entity& operator=(Entity&& p_other);
            bool operator!= (const Entity& p_other) const { return !(*this == p_other); }
            UUID::integer_type get_id() const  { return id.get(); }
        private:
            UUID id;
            friend bool operator==(const Entity& lhs, const Entity& rhs);
            Entity() = default;

    };
}

namespace std
{
    template<>
    struct hash<::basket::ecs::Entity>
    {
        std::size_t operator()(const ::basket::ecs::Entity& entity) const
        {
            return hash<::basket::UUID::integer_type>()(entity.get_id());
        }
    };

    template<>
    struct less<::basket::ecs::Entity>
    {
        bool operator()(const ::basket::ecs::Entity& lhs, const ::basket::ecs::Entity& rhs) const
        {
            return lhs.get_id() < rhs.get_id();
        }
    };
}

#endif