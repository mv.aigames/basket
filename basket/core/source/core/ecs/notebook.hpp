#ifndef NOTEBOOK_HPP
#define NOTEBOOK_HPP

#include "entity.hpp"
#include "view.hpp"

namespace basket
{
    class World;
    namespace ecs
    {
        namespace _internal
        {
            // Use the variabel address in order to give an unique id to each componewnt that is added to the system.
            template<typename T> 
            int _component_hash = 0;
            template<typename T>  
            size_t component_hash_v = (size_t)(&_component_hash<T>);

            // template<typename T>
            // constexpr uint64_t value = (uint64_t)&ComponentHash<T>::_get_hash_code;

            struct ComponentProperty
            {
                size_t size;
                void* object;
                size_t id;
            };
        }


        class Notebook
        {
            friend class ::basket::World;
            
            public:
                template<typename T>
                using added_component_event_object_type = std::function<void(const Entity&, T&)>;
                using entity_being_destroyed_callback_type = std::function<void(const Entity&)>;
                using entity_created_callback_type = std::function<void(const Entity&)>;
                using component_container_type = std::unordered_map<Entity*, std::vector<_internal::ComponentProperty>>;


            public:

                Entity* create_entity()
                {
                    Entity* e = new Entity();
                    entities.insert(e);
                    for (entity_created_callback_type& function: entitiy_created_container)
                    {
                        function(*e);
                    }
                    return e;
                }

                template<typename T>
                Entity* find_entity()
                {
                    return nullptr;
                }


                bool destroy_entity(Entity& p_entity)
                {
                    //Check whether the entity exists or not. return false if doesn't exists.
                    Entity* e = &p_entity;

                    for(entity_being_destroyed_callback_type& function: entitiy_being_destroyed_container)
                    {
                        function(p_entity);
                    }

                    std::set<Entity*>::iterator i = entities.find(e);

                    if (i == entities.end()) { return false; }
                    // Destroy all components attached to the entity. Tricky part.
                    std::vector<_internal::ComponentProperty>& cps = components[e];
                    for(auto& c : cps)
                    {
                        delete c.object;
                    }
                
                    // Destroy entity
                    components.erase(e);
                    // In theory, as Entity is in smart pointer. This should release the memory automatically.
                    entities.erase(i);
                    return true;
                }

                // +582125032777


                template<typename T, typename... Args>
                T* add_component(const Entity& p_entity, Args&&... args)
                {
                    const Entity* e = &p_entity;

                    if (entities.find(&const_cast<Entity&>(p_entity)) == entities.end())
                    {
                        return nullptr;
                    };

                    if (has_component<T>(p_entity)) { return nullptr; }

                    T* component = new T(std::forward<Args>(args)...);

                    _internal::ComponentProperty cp = {
                        .size = sizeof(T),
                        .object = component,
                        // .id = (size_t)(&_internal::component_hash<T>)
                        .id = _internal::component_hash_v<T>
                    };

                    components[&const_cast<Entity&>(p_entity)].push_back(cp);

                    for(auto& function: added_component_event_object_pool<T>)
                    {
                        function(p_entity, *component);
                    }

                    return component;
                }

                template<typename T>
                T* get_component(const Entity& p_entity)
                {
                    auto search = components.find(&const_cast<Entity&>(p_entity));
                    if (search == components.end())
                    {
                        return nullptr;
                    }

                    for(const auto& c : search->second)
                    {
                        if (c.id == _internal::component_hash_v<T>)
                        {
                            return static_cast<T*>(c.object);
                        }
                    }
                    return nullptr;
                }

                /**
                 * @brief Find the first match of T type.
                 * 
                 * @tparam T 
                 * @return T* 
                 */
                template<typename T>
                T* find_component()
                {
                    for (const auto& pair : components)
                    {
                        for (const decltype(component_container_type::value_type::second_type::value_type)& obj : pair)
                        {
                            if (obj.id == _internal::component_hash_v<T>)
                            {
                                return static_cast<T*>(obj.object);
                            }
                        }
                    }
                    return nullptr;
                }

                template<typename T>
                bool remove_component(const Entity& p_entity)
                {
                    component_container_type::iterator search = components.find(&const_cast<Entity&>(p_entity));
                    if (search == components.end())
                    {
                        return false;
                    }

                    // A little long. But I made it just for learning purpose.
                    for(component_container_type::value_type::second_type::iterator it = search->second.begin(); it != search->second.end(); ++it)
                    {
                        
                        if (it->id == _internal::component_hash_v<T>)
                        {
                            delete static_cast<T*>(it->object);
                            search->second.erase(it);
                            return true;
                        }
                    }

                    return false;
                }

                int get_components_attached(const Entity& p_entity) const
                {
                    return 0;
                }

                

                template<typename T>
                void bind_component_added_event_object(std::function<void(const Entity&, T&)>& p_added_component_function)
                {
                    added_component_event_object_pool<T>.emplace_back(p_added_component_function);
                }

                template<typename T>
                bool has_component(const Entity& p_entity)
                {
                    std::vector<_internal::ComponentProperty>& cps = components[&const_cast<Entity&>(p_entity)];
                    for(auto& c: cps)
                    {
                        if (c.id == _internal::component_hash_v<T>)
                        {
                            return true;
                        }
                    }

                    return false;
                }

                void bind_entity_being_detroyed_function(entity_being_destroyed_callback_type& p_function)
                {
                    entitiy_being_destroyed_container.push_back(p_function);
                }

                void bind_entity_create_function(entity_created_callback_type& p_function)
                {
                    entitiy_created_container.push_back(p_function);
                }

                template<typename... Args>
                View<Args...> get_view()
                {
                    /**
                     * Ok. This part is a little bit complicated.
                     * I need to get an array of the components for each entity.
                     * 
                     * I will try to figure this out. wish me luck
                     * 
                     */
                    typename View<Args...>::internal_type valid_components;
                    
                    for(const Entity* entity: entities)
                    {
                        typename View<Args...>::row_type row(get_component<Args>(*entity)...);
                        bool valid_entity = ((nullptr != std::get<Args*>(row)) && ...);
                        
                        if (valid_entity)
                        {
                            View<Args...>::insert_row(valid_components, std::forward<Args*>(get_component<Args>(*entity))...);
                        }
                    }
                    
                    return View<Args...>(std::move(valid_components));
                }
            
            private:

                Notebook() = default;
                Notebook(const Notebook& p_other) = delete;
                Notebook(Notebook&& p_other) = delete;
                Notebook& operator=(const Notebook& p_other) = delete;
                Notebook& operator=(Notebook&& p_other) = delete;

                std::vector<entity_being_destroyed_callback_type> entitiy_being_destroyed_container;
                std::vector<entity_created_callback_type> entitiy_created_container;

                std::set<Entity*> entities; // To keep track of all entities.

                // template<typename E, typename T>
                // static std::unordered_map<E, std::shared_ptr<T>> components;

                template<typename T>
                static std::vector<added_component_event_object_type<T>> added_component_event_object_pool;

                
                component_container_type components;
        };

            
        // template<typename E, typename T>
        // std::unordered_map<E, std::shared_ptr<T>> Notebook::components;

        template<typename T>
        std::vector<Notebook::added_component_event_object_type<T>> Notebook::added_component_event_object_pool;
    }
}

#endif