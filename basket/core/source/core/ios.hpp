#ifndef BASKET_IOS_HPP
#define BASKET_IOS_HPP

#ifdef BASKET_STD
    #include <ios>
    namespace basket = std;
#else
    #include "_std/_ios.hpp"
#endif

#endif