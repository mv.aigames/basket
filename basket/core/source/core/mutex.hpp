#ifndef BASKET_MUTEX_HPP
#define BASKET_MUTEX_HPP

#ifdef BASKET_STD
    #include <queue>
    namespace basket = std;
#else
    #include "_std/_mutex.hpp"
#endif

#endif