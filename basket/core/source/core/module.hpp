#ifndef BASKET_CORE_MODULE_HPP
#define BASKET_CORE_MODULE_HPP

#include <core/module_loader/module.hpp>
#include <core/macros.hpp>
#include <core/logging.hpp>

BK_DECLARE_LOG(core);

namespace basket::core
{
    class BASKET_API Module: public AModule
    {
        public:
            Module(): AModule(AUpdate::Group::pre_physics) {}
        public:
            virtual Result pre_init() override;
            virtual Result init() override;
            virtual Result post_init() override;
            virtual Result pre_destroy() override;
            virtual Result destroy() override;
            virtual Result post_destroy() override;
        
        public:
            virtual void update(const real_t p_delta) override;
    };
}

#endif