#ifndef BASKET_UPDATE_GROUP_HPP
#define BASKET_UPDATE_GROUP_HPP

#include "macros.hpp"
#include "typedefs.hpp"

namespace basket
{


    class BASKET_API AUpdate
    {
        public:
            enum class Group: uint8_t
            {
                pre_physics,
                physics,
                post_physics,
                render,
                game,
                none
            };
        
        public:
            virtual void update(const real_t p_delta) = 0;
            Group get_group() const { return _group; }

        public:
            AUpdate(Group p_group): _group(p_group) {}
            virtual ~AUpdate() {}

        private:
            Group _group;
        
    };
}

#endif