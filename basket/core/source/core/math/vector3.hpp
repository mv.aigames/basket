#ifndef BASKET_VECTOR_3_HPP
#define BASKET_VECTOR_3_HPP


#include <core/math/_impl/_vector3_definition.hpp>
#include <core/math/_impl/_vector3_operators.hpp>
#include <core/math/_impl/_vector3_functions.hpp>
#include <core/math/_impl/_vector3_stream.hpp>

#endif