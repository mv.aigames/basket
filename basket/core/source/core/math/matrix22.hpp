#ifndef BASKET_MATRIX22_HPP
#define BASKET_MATRIX22_HPP

#include <core/math/_impl/_matrix22_definition.hpp>
#include <core/math/_impl/_matrix22_operators.hpp>
#include <core/math/_impl/_matrix22_functions.hpp>
#include <core/math/_impl/_matrix22_stream.hpp>

#endif