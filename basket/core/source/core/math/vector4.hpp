#ifndef BASKET_VECTOR_4_HPP
#define BASKET_VECTOR_4_HPP



#include <core/math/_impl/_vector4_definition.hpp>
#include <core/math/_impl/_vector4_operators.hpp>
#include <core/math/_impl/_vector4_functions.hpp>
#include <core/math/_impl/_vector4_stream.hpp>

#endif
