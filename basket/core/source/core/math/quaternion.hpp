#ifndef BASKET_QUATERNION_HPP
#define BASKET_QUATERNION_HPP

#include <core/math/_impl/_quaternion_definition.hpp>
#include <core/math/_impl/_quaternion_operators.hpp>
#include <core/math/_impl/_quaternion_functions.hpp>

#endif
