#ifndef BASKET_MATH_INACCURATE_HPP
#define BASKET_MATH_INACCURATE_HPP

#include <core/typedefs.hpp>

namespace basket::bmath
{
    inline float unprecise_sqrt2(const float p_value, const uint8_t p_steps)
    {
        float result = 0.0f;
        float one = 1.0f;
        for (int index = 0; index < p_steps; index++)
        {
            result += (p_value / (2.0f + index)) * one;
            one *= -1.0f;
        }

        return result;
    }
}

#endif