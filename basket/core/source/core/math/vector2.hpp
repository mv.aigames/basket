#ifndef BASKET_VECTOR_2_HPP
#define BASKET_VECTOR_2_HPP

#include <core/math/_impl/_vector2_definition.hpp>
#include <core/math/_impl/_vector2_operators.hpp>
#include <core/math/_impl/_vector2_functions.hpp>
#include <core/math/_impl/_vector2_stream.hpp>

#endif