#ifndef BASKET__MATRIX22_STREAM_HPP
#define BASKET__MATRIX22_STREAM_HPP

#include "_matrix22_definition.hpp"

namespace basket
{
    template<typename T>
    inline std::ostream& operator<<(std::ostream& os, const _Matrix22_Template<T>& p_other)
    {
        os <<  "[{" << p_other.row0.x << ", " << p_other.row0.y << "}, \n" << p_other.row1.x << ", " << p_other.row1.y << "}]";
        return os;
    }
}

#endif