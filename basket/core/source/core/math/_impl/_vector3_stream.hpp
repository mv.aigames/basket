#ifndef BASKET__VECTOR3_STREAM_HPP
#define BASKET__VECTOR3_STREAM_HPP

#include "_vector3_definition.hpp"

namespace basket
{
    // Catch complains a lot
    template<typename T>
    inline std::ostream& operator<<(std::ostream& os, const VectorXYZ_Template<T>& p_other)
    {
        os <<  "{" << p_other.x << ", " << p_other.y << ", " << p_other.z << "}";
        return os;
    }
}


#endif