#ifndef BASKET__BASE_OPERATORS_HPP
#define BASKET__BASE_OPERATORS_HPP


template<typename T, typename _RET, typename LT, typename RT>
_RET operator*(const LT p_left, const RT p_right);

#endif