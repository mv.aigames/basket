#ifndef BASKET__MATRIX44_STREAM_HPP
#define BASKET__MATRIX44_STREAM_HPP

#include "_matrix44_definition.hpp"

namespace basket
{
    // Catch complains a lot
    template<typename T>
    inline std::ostream& operator<<(std::ostream& os, const _Matrix44_Template<T>& p_other)
    {
        os <<  "[{" << p_other.row0.x << ", " << p_other.row0.y << ", " << p_other.row0.z << ", " << p_other.row0.w << "}, \n"
            <<  "{" << p_other.row1.x << ", " << p_other.row1.y << ", " << p_other.row1.z << ", " << p_other.row1.w << "}, \n"
            <<  "{" << p_other.row2.x << ", " << p_other.row2.y << ", " << p_other.row2.z << ", " << p_other.row2.w << "}, \n"
            <<  "{" << p_other.row3.x << ", " << p_other.row3.y << ", " << p_other.row3.z << ", " << p_other.row3.w << "}]";
        return os;
    }
}


#endif