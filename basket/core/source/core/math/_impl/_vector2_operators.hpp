#ifndef BASKET__VECTOR2_OPERATORS_HPP
#define BASKET__VECTOR2_OPERATORS_HPP

// #include <core/constraints.hpp>
#include "_vector2_definition.hpp"


constexpr bool operator== (const basket::Vector2& rhs, const basket::Vector2& lhs)
{
    return rhs.x == lhs.x && rhs.y == lhs.y;
}


constexpr basket::Vector2 operator- (const basket::Vector2& v)
{
    return basket::Vector2(-v.x, -v.y);
}



constexpr basket::Vector2 operator+ (const basket::Vector2& v)
{
    return v; // Dahhhh
}



constexpr bool operator!= (const basket::Vector2& rhs, const basket::Vector2& lhs)
{
    return !(rhs == lhs);
}



constexpr basket::Vector2 operator+ (const basket::Vector2& rhs, const basket::Vector2& lhs)
{
    return basket::Vector2(rhs.x + lhs.x, rhs.y + lhs.y);
}



constexpr basket::Vector2 operator- (const basket::Vector2& rhs, const basket::Vector2& lhs)
{
    return basket::Vector2(rhs.x - lhs.x, rhs.y - lhs.y);
}



constexpr basket::Vector2 operator/ (const basket::Vector2& rhs, const basket::Vector2::value_type value)
{
    return basket::Vector2(rhs.x / value, rhs.y / value);
}


inline basket::Vector2 operator+= (basket::Vector2& rhs, const basket::Vector2& lhs)
{
    rhs = rhs + lhs;
    return rhs;
}



inline basket::Vector2& operator-= (basket::Vector2& rhs, const basket::Vector2& lhs)
{
    rhs = rhs - lhs;
    return rhs;
}



inline basket::Vector2& operator/= (basket::Vector2& rhs, const basket::Vector2::value_type value)
{
    rhs = rhs / value;
    return rhs;
}


constexpr basket::Vector2 operator* (const basket::Vector2& rhs, const basket::Vector2::value_type value)
{
    return basket::Vector2(rhs.x * value, rhs.y * value);
}


constexpr basket::Vector2 operator* (const basket::Vector2::value_type value, const basket::Vector2& rhs)
{
    return basket::Vector2(rhs.x * value, rhs.y * value);
}


inline basket::Vector2 operator*= (basket::Vector2& rhs, const basket::Vector2::value_type value)
{
    rhs = rhs * value;
    return rhs;
}

#endif