#ifndef BASKET__MATRIX44_OPERATORS_HPP
#define BASKET__MATRIX44_OPERATORS_HPP

// #include "_vector4_functions.hpp"
#include "_vector4_operators.hpp"
#include "_matrix44_definition.hpp"


constexpr bool operator== (const basket::Matrix44& p_lhs, const basket::Matrix44& p_rhs)
{
    return  p_lhs.row0 == p_rhs.row0 &&
            p_lhs.row1 == p_rhs.row1 &&
            p_lhs.row2 == p_rhs.row2 &&
            p_lhs.row3 == p_rhs.row3;
}


constexpr bool operator!= (const basket::Matrix44& p_lhs, const basket::Matrix44& p_rhs)
{
    return !(p_lhs == p_rhs);
}


constexpr basket::Matrix44::row_type operator* (const basket::Matrix44& p_matrix, const basket::Matrix44::row_type& p_vector)
{
    using row_type = basket::Matrix44::row_type;

    return {
        p_matrix.row0.x * p_vector.x + p_matrix.row0.y * p_vector.y + p_matrix.row0.z * p_vector.z + p_matrix.row0.w * p_vector.w,
        p_matrix.row1.x * p_vector.x + p_matrix.row1.y * p_vector.y + p_matrix.row1.z * p_vector.z + p_matrix.row1.w * p_vector.w,
        p_matrix.row2.x * p_vector.x + p_matrix.row2.y * p_vector.y + p_matrix.row2.z * p_vector.z + p_matrix.row2.w * p_vector.w,
        p_matrix.row3.x * p_vector.x + p_matrix.row3.y * p_vector.y + p_matrix.row3.z * p_vector.z + p_matrix.row3.w * p_vector.w
    };
}


constexpr basket::Matrix44 operator* (const basket::Matrix44& p_left, const basket::Matrix44& p_right)
{
    using row_type = basket::Matrix44::row_type;

    const row_type row0 {
        p_left.row0.x * p_right.row0.x + p_left.row0.y * p_right.row1.x + p_left.row0.z * p_right.row2.x + p_left.row0.w * p_right.row3.x,
        p_left.row0.x * p_right.row0.y + p_left.row0.y * p_right.row1.y + p_left.row0.z * p_right.row2.y + p_left.row0.w * p_right.row3.y,
        p_left.row0.x * p_right.row0.z + p_left.row0.y * p_right.row1.z + p_left.row0.z * p_right.row2.z + p_left.row0.w * p_right.row3.z,
        p_left.row0.x * p_right.row0.w + p_left.row0.y * p_right.row1.w + p_left.row0.z * p_right.row2.w + p_left.row0.w * p_right.row3.w
    };

    const row_type row1 {
        p_left.row1.x * p_right.row0.x + p_left.row1.y * p_right.row1.x + p_left.row1.z * p_right.row2.x + p_left.row1.w * p_right.row3.x,
        p_left.row1.x * p_right.row0.y + p_left.row1.y * p_right.row1.y + p_left.row1.z * p_right.row2.y + p_left.row1.w * p_right.row3.y,
        p_left.row1.x * p_right.row0.z + p_left.row1.y * p_right.row1.z + p_left.row1.z * p_right.row2.z + p_left.row1.w * p_right.row3.z,
        p_left.row1.x * p_right.row0.w + p_left.row1.y * p_right.row1.w + p_left.row1.z * p_right.row2.w + p_left.row1.w * p_right.row3.w
    };

    const row_type row2 {
        p_left.row2.x * p_right.row0.x + p_left.row2.y * p_right.row1.x + p_left.row2.z * p_right.row2.x + p_left.row2.w * p_right.row3.x,
        p_left.row2.x * p_right.row0.y + p_left.row2.y * p_right.row1.y + p_left.row2.z * p_right.row2.y + p_left.row2.w * p_right.row3.y,
        p_left.row2.x * p_right.row0.z + p_left.row2.y * p_right.row1.z + p_left.row2.z * p_right.row2.z + p_left.row2.w * p_right.row3.z,
        p_left.row2.x * p_right.row0.w + p_left.row2.y * p_right.row1.w + p_left.row2.z * p_right.row2.w + p_left.row2.w * p_right.row3.w
    };
    const row_type row3 {
        p_left.row3.x * p_right.row0.x + p_left.row3.y * p_right.row1.x + p_left.row3.z * p_right.row2.x + p_left.row3.w * p_right.row3.x,
        p_left.row3.x * p_right.row0.y + p_left.row3.y * p_right.row1.y + p_left.row3.z * p_right.row2.y + p_left.row3.w * p_right.row3.y,
        p_left.row3.x * p_right.row0.z + p_left.row3.y * p_right.row1.z + p_left.row3.z * p_right.row2.z + p_left.row3.w * p_right.row3.z,
        p_left.row3.x * p_right.row0.w + p_left.row3.y * p_right.row1.w + p_left.row3.z * p_right.row2.w + p_left.row3.w * p_right.row3.w
    };

    return {
        row0,
        row1,
        row2,
        row3
    };
}



constexpr basket::Matrix44 operator* (const basket::Matrix44& p_matrix, const basket::Matrix44::value_type p_value)
{
    return {
        p_matrix.row0 * p_value,
        p_matrix.row1 * p_value,
        p_matrix.row2 * p_value,
        p_matrix.row3 * p_value
    };
}

constexpr basket::Matrix44 operator/ (const basket::Matrix44& p_matrix, const basket::Matrix44::value_type p_value)
{
    return {
        p_matrix.row0 / p_value,
        p_matrix.row1 / p_value,
        p_matrix.row2 / p_value,
        p_matrix.row3 / p_value
    };
}


constexpr basket::Matrix44 operator* (const basket::Matrix44::value_type p_value, const basket::Matrix44& p_matrix)
{
    return p_matrix * p_value;
}


constexpr basket::Matrix44 operator+ (const basket::Matrix44& p_matrix)
{
    return p_matrix;
}


constexpr basket::Matrix44 operator+ (const basket::Matrix44& p_left, const basket::Matrix44& p_right)
{
    return {
        p_left.row0 + p_right.row0,
        p_left.row1 + p_right.row1,
        p_left.row2 + p_right.row2,
        p_left.row3 + p_right.row3
    };
}


constexpr basket::Matrix44 operator- (const basket::Matrix44& p_matrix)
{
    return {
        -p_matrix.row0,
        -p_matrix.row1,
        -p_matrix.row2,
        -p_matrix.row3
    };
}


constexpr basket::Matrix44 operator- (const basket::Matrix44& p_left, const basket::Matrix44& p_right)
{
    return {
        p_left.row0 - p_right.row0,
        p_left.row1 - p_right.row1,
        p_left.row2 - p_right.row2,
        p_left.row3 - p_right.row3
    };
}


constexpr basket::Matrix44& operator+= (basket::Matrix44& p_left, const basket::Matrix44& p_right)
{
    p_left = basket::Matrix44(p_left + p_right);
    return p_left;
}


constexpr basket::Matrix44& operator-= (basket::Matrix44& p_left, const basket::Matrix44& p_right)
{
    p_left = basket::Matrix44(p_left - p_right);
    return p_left;
}


constexpr basket::Matrix44& operator*= (basket::Matrix44& p_left, const basket::Matrix44& p_right)
{
    p_left = basket::Matrix44(p_left * p_right);
    return p_left;
}


constexpr basket::Matrix44& operator*= (basket::Matrix44& p_matrix, const basket::Matrix44::value_type p_value)
{
    p_matrix = basket::Matrix44(p_matrix * p_value);
    return p_matrix;
}


constexpr basket::Matrix44& operator/= (basket::Matrix44& p_matrix, const basket::Matrix44::value_type p_value)
{
    p_matrix = basket::Matrix44(p_matrix / p_value);
    return p_matrix;
}




#endif