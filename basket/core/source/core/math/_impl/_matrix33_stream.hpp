#ifndef BASKET__MATRIX33_STREAM_HPP
#define BASKET__MATRIX33_STREAM_HPP

#include "_matrix33_definition.hpp"

namespace basket
{
    template<typename T>
    inline std::ostream& operator<<(std::ostream& os, const basket::_Matrix33_Template<T>& p_other)
    {
        os <<  "[{" << p_other.row0.x << ", " << p_other.row0.y << ", " << p_other.row0.z <<  "}, \n"
            <<  "{" << p_other.row1.x << ", " << p_other.row1.y << ", " << p_other.row1.z <<  "}, \n"
            <<  "{" << p_other.row2.x << ", " << p_other.row2.y << ", " << p_other.row2.z <<  "}]";
        return os;
    }
}

#endif