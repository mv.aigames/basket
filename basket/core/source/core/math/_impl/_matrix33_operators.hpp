#ifndef BASKET__MATRIX33_OPERATORS_HPP
#define BASKET__MATRIX33_OPERATORS_HPP

#include "_matrix33_definition.hpp"
#include "_vector3_operators.hpp"



constexpr bool operator== (const basket::Matrix33& p_lhs, const basket::Matrix33& p_rhs)
{
    return  p_lhs.row0 == p_rhs.row0 &&
            p_lhs.row1 == p_rhs.row1 &&
            p_lhs.row2 == p_rhs.row2;
}


constexpr bool operator!= (const basket::Matrix33& p_lhs, const basket::Matrix33& p_rhs)
{
    return !(p_lhs == p_rhs);
}


constexpr basket::Matrix33 operator- (const basket::Matrix33& p_lhs, const basket::Matrix33& p_rhs)
{
    return {
        p_lhs.row0 - p_rhs.row0,
        p_lhs.row1 - p_rhs.row1,
        p_lhs.row2 - p_rhs.row2
    };
}


constexpr basket::Matrix33 operator+ (const basket::Matrix33& p_lhs, const basket::Matrix33& p_rhs)
{
    return {
        p_lhs.row0 + p_rhs.row0,
        p_lhs.row1 + p_rhs.row1,
        p_lhs.row2 + p_rhs.row2
    };
}


constexpr basket::Matrix33 operator* (const basket::Matrix33& p_left, const basket::Matrix33& p_right)
{
    using row_type = basket::Matrix33::row_type;

    const basket::Matrix33::row_type row0{
        {p_left.row0.x * p_right.row0.x + p_left.row0.y * p_right.row1.x + p_left.row0.z * p_right.row2.x},
        {p_left.row0.x * p_right.row0.y + p_left.row0.y * p_right.row1.y + p_left.row0.z * p_right.row2.y},
        {p_left.row0.x * p_right.row0.z + p_left.row0.y * p_right.row1.z + p_left.row0.z * p_right.row2.z}
    };

    const basket::Matrix33::row_type row1{
        {p_left.row1.x * p_right.row0.x + p_left.row1.y * p_right.row1.x + p_left.row1.z * p_right.row2.x},
        {p_left.row1.x * p_right.row0.y + p_left.row1.y * p_right.row1.y + p_left.row1.z * p_right.row2.y},
        {p_left.row1.x * p_right.row0.z + p_left.row1.y * p_right.row1.z + p_left.row1.z * p_right.row2.z}
    };

    const basket::Matrix33::row_type row2{
        {p_left.row2.x * p_right.row0.x + p_left.row2.y * p_right.row1.x + p_left.row2.z * p_right.row2.x},
        {p_left.row2.x * p_right.row0.y + p_left.row2.y * p_right.row1.y + p_left.row2.z * p_right.row2.y},
        {p_left.row2.x * p_right.row0.z + p_left.row2.y * p_right.row1.z + p_left.row2.z * p_right.row2.z}
    };

    return {row0, row1, row2};
}


constexpr basket::Matrix33 operator* (const basket::Matrix33::value_type p_value, const basket::Matrix33& p_matrix)
{
    return {
        p_value * p_matrix.row0,
        p_value * p_matrix.row1,
        p_value * p_matrix.row2
    };
}


constexpr basket::Matrix33 operator* (const basket::Matrix33& p_matrix, const basket::Matrix33::value_type p_value)
{
    return p_value * p_matrix;
}


constexpr basket::Matrix33 operator/ (const basket::Matrix33& p_matrix, const basket::Matrix33::value_type p_value)
{
    return {
        p_matrix.row0 / p_value,
        p_matrix.row1 / p_value,
        p_matrix.row2 / p_value
    };
}


constexpr basket::Matrix33 operator-(const basket::Matrix33& p_matrix)
{
    return {
        -p_matrix.row0,
        -p_matrix.row1,
        -p_matrix.row2
    };
}


constexpr basket::Matrix33 operator+(const basket::Matrix33& p_matrix)
{
    return p_matrix;
}


inline basket::Matrix33& operator+= (basket::Matrix33& p_left, const basket::Matrix33& p_right)
{
    // return (p_left = p_left + p_right, p_left); // Valid, but nope.
    p_left = p_left + p_right;
    return p_left;
}


inline basket::Matrix33& operator-= (basket::Matrix33& p_left, const basket::Matrix33& p_right)
{
    // return (p_left = p_left + p_right, p_left); // Valid, but nope.
    p_left = p_left - p_right;
    return p_left;
}


inline basket::Matrix33& operator*= (basket::Matrix33& p_matrix, const basket::Matrix33::value_type p_value)
{
    // return (p_left = p_left + p_right, p_left); // Valid, but nope.
    p_matrix = p_matrix * p_value;
    return p_matrix;
}


inline basket::Matrix33& operator*= (basket::Matrix33& p_left, const basket::Matrix33& p_right)
{
    // return (p_left = p_left + p_right, p_left); // Valid, but nope.
    p_left = p_left * p_right;
    return p_left;
}


constexpr basket::Matrix33& operator/= (basket::Matrix33& p_matrix, const basket::Matrix33::value_type p_value)
{
    // return (p_left = p_left + p_right, p_left); // Valid, but nope.
    p_matrix = p_matrix / p_value;
    return p_matrix;
}



#endif