#ifndef BASKET__VECTOR4_STREAM_HPP
#define BASKET__VECTOR4_STREAM_HPP

#include "_vector4_definition.hpp"

namespace basket
{
    template<typename T>
    inline std::ostream& operator<<(std::ostream& os, const VectorXYZW_Template<T>& p_other)
    {
        os <<  "{" << p_other.x << ", " << p_other.y << ", " << p_other.z << ", " << p_other.w << "}";
        return os;
    }
}


#endif