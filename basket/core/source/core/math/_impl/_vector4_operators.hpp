#ifndef BASKET__VECTOR4_OPERATORS_HPP
#define BASKET__VECTOR4_OPERATORS_HPP

#include "_vector4_definition.hpp"


constexpr bool operator==(const basket::Vector4& p_lhs, const basket::Vector4& p_rhs)
{
    return  p_lhs.x == p_rhs.x &&
            p_lhs.y == p_rhs.y &&
            p_lhs.z == p_rhs.z &&
            p_lhs.w == p_rhs.w;
}

constexpr bool operator!=(const basket::Vector4& p_lhs, const basket::Vector4& p_rhs)
{
    return !(p_lhs == p_rhs);
}

constexpr basket::Vector4 operator-(const basket::Vector4& p_left, const basket::Vector4& p_right)
{
    return {
        p_left.x - p_right.x,
        p_left.y - p_right.y,
        p_left.z - p_right.z,
        p_left.w - p_right.w
    };
}

constexpr basket::Vector4 operator-(const basket::Vector4& p_vector)
{
    return {
        -p_vector.x,
        -p_vector.y,
        -p_vector.z,
        -p_vector.w
    };
}


constexpr basket::Vector4 operator+(const basket::Vector4& p_vector)
{
    return p_vector;
}


constexpr basket::Vector4 operator+(const basket::Vector4& p_left, const basket::Vector4& p_right)
{
    return {
        p_left.x + p_right.x,
        p_left.y + p_right.y,
        p_left.z + p_right.z,
        p_left.w + p_right.w
    };
}


constexpr basket::Vector4 operator/(const basket::Vector4& p_vector, const basket::Vector4::value_type p_value)
{
    return {
        p_vector.x / p_value,
        p_vector.y / p_value,
        p_vector.z / p_value,
        p_vector.w / p_value
    };
}


constexpr basket::Vector4 operator*(const basket::Vector4& p_vector, const basket::Vector4::value_type p_value)
{
    return {
        p_vector.x * p_value,
        p_vector.y * p_value,
        p_vector.z * p_value,
        p_vector.w * p_value
    };
}


constexpr basket::Vector4 operator*(const basket::Vector4::value_type p_value, const basket::Vector4& p_vector)
{
    return {
        p_vector.x * p_value,
        p_vector.y * p_value,
        p_vector.z * p_value,
        p_vector.w * p_value
    };
}


inline basket::Vector4& operator*=(basket::Vector4& p_vector, const basket::Vector4::value_type p_value)
{
        p_vector.x = p_vector.x * p_value;
        p_vector.y = p_vector.y * p_value;
        p_vector.z = p_vector.z * p_value;
        p_vector.w = p_vector.w * p_value;

    return p_vector;
}


inline basket::Vector4& operator+=(basket::Vector4& p_left, const basket::Vector4& p_right)
{
        p_left.x = p_left.x + p_right.x;
        p_left.y = p_left.y + p_right.y;
        p_left.z = p_left.z + p_right.z;
        p_left.w = p_left.w + p_right.w;

    return p_left;
}


inline basket::Vector4& operator-=(basket::Vector4& p_left, const basket::Vector4& p_right)
{
        p_left.x = p_left.x - p_right.x;
        p_left.y = p_left.y - p_right.y;
        p_left.z = p_left.z - p_right.z;
        p_left.w = p_left.w - p_right.w;

    return p_left;
}


inline basket::Vector4& operator/=(basket::Vector4& p_vector, const basket::Vector4::value_type p_value)
{
        p_vector.x = p_vector.x / p_value;
        p_vector.y = p_vector.y / p_value;
        p_vector.z = p_vector.z / p_value;
        p_vector.w = p_vector.w / p_value;

    return p_vector;
}




#endif