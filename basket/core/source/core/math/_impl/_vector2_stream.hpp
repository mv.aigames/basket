#ifndef BASKET__VECTOR2_STREAM_HPP
#define BASKET__VECTOR2_STREAM_HPP

#include "_vector2_definition.hpp"

namespace basket
{
    // Catch complains a lot
    template<typename T>
    inline std::ostream& operator<<(std::ostream& os, const VectorXY_Template<T>& p_other)
    {
        os <<  "{" << p_other.x << ", " << p_other.y << "}";
        return os;
    }
}


#endif