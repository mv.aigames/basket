#include "logging.hpp"

#include "pch.hpp"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

static auto console = spdlog::stdout_color_mt("console");

static std::map<const char*, decltype(spdlog::stdout_color_mt(""))> logs {
    {"engine", spdlog::stdout_color_mt("engine")},
    {"app", spdlog::stdout_color_mt("app")}
};

namespace basket::logging
{
    BASKET_API void init()
    {

    }


    BASKET_API void _log(const char* p_log_name, const char* p_message)
    {
        if (logs.find(p_log_name) != logs.end())
        {
            logs[p_log_name]->error(std::string("Log name: ") + std::string(p_log_name) + std::string(" doesn't exist"));
        }
        else
        {
            spdlog::get(p_log_name)->info(p_message);
        }
    }

    BASKET_API void define_log(const char* p_log_name)
    {
        if (logs.find(p_log_name) != logs.end())
        {
            logs[p_log_name] = spdlog::stdout_color_mt(p_log_name);
        }
    }

    
}

namespace basket
{
    _BK_LogCategory::_BK_LogCategory(const string& p_log_name): _log_name(p_log_name)
    {
        _logger = spdlog::stdout_color_mt(p_log_name.c_str());
    }
}