#ifndef BASKET__STD_DEQUEUE_HPP
#define BASKET__STD_DEQUEUE_HPP

#include "_memory.hpp"
#include <core/utility.hpp>

namespace basket
{

    namespace _internal
    {
        template<typename T>
        struct BDequeueNode
        {
            using base = BDequeueNode<T>;
            base* front;
            base* back;

            T value;

            BDequeueNode(const T& p_value): value(p_value) {}
            template<typename... Args>
            BDequeueNode(Args&&... p_args): value(std::forward<Args>(p_args)...) {}

        };

        struct front {};
        struct back {};
    };

    template<typename T>
    struct DequeueBaseFrontIterator;

    template<typename T, typename AllocatorT = Allocator<T> >
    class BDequeue
    {
            using node_type = _internal::BDequeueNode<T>;
            using node_allocator = allocator_traits<AllocatorT>::template rebind_alloc<node_type>;
            using iterator = DequeueBaseFrontIterator<T>;

            public:
                BDequeue() = default;
                BDequeue(const BDequeue& p_other) = default;
                BDequeue(BDequeue&& p_other) = default;

                BDequeue& operator=(const BDequeue& p_other) = default;
                BDequeue& operator=(BDequeue&& p_other) = default;
            
            public:
                void push_front(const T& p_other)
                {
                    node_type* node = _generate_node(p_other);
                    insert_node<_internal::front>(node);

                }

                void push_front(T&& p_other)
                {
                    node_type* node = _generate_node(p_other);
                    insert_node<_internal::front>(node);

                }

                template<typename... Args>
                void push_front(Args&&... p_args)
                {
                    node_type* node = _generate_node(p_args...);
                    insert_node<_internal::front>(node);
                }

                void push_back(const T& p_other)
                {
                    node_type* node = _generate_node(p_other);
                    insert_node<_internal::back>(node);

                }

                void push_back(T&& p_other)
                {
                    node_type* node = _generate_node(p_other);
                    insert_node<_internal::back>(node);

                }

                template<typename... Args>
                void push_back(Args&&... p_args)
                {
                    node_type* node = _generate_node(p_args...);
                    insert_node<_internal::back>(node);
                }

                void pop_front()
                {
                    if (empty())
                    {
                        return;
                    }

                    remove_node<_internal::front>();
                }

                void pop_back()
                {
                    if (empty())
                    {
                        return;
                    }

                    remove_node<_internal::back>();
                }

                T& back()
                {
                    return back_head->value;
                }

                const T& back() const
                {
                    return back_head->value;
                }

                T& front()
                {
                    return front_head->value;
                }

                const T& front() const
                {
                    return front_head->value;
                }

                bool empty() const
                {
                    return _size == 0;
                }

            public:
                iterator begin()
                {
                    return iterator(back_head);
                }

                iterator end()
                {
                    return iterator(front_head->front);
                }

            private:

                template<typename T>
                enable_if<is_same_v<T, _internal::front>> remove_node()
                {
                    if (_size == 1)
                    {
                        remove_last_node();
                    }
                    else
                    {
                        node_type* temp = front_head;
                        front_head = front_head->back;
                        front_head->front = nullptr;
                        destroy_node(temp);
                    }
                }

                template<typename T>
                enable_if<is_same_v<T, _internal::back>> remove_node()
                {
                    if (_size == 1)
                    {
                        remove_last_node();
                    }
                    else
                    {
                        node_type* temp = back_head;
                        back_head = back_head->front;
                        back_head->back = nullptr;
                        destroy_node(temp);
                    }
                }

                void destroy_node(node_type* p)
                {
                    allocator_node.destroy(p);
                    allocator_node.deallocate(p);
                    --_size;
                }

                void remove_last_node()
                {
                    node_type* node = front_head;
                    front_head = nullptr;
                    back_head = nullptr;

                    allocator_node.destroy(allocator_node, 1);
                }

                template<typename T>
                enable_if<is_same_v<T, _internal::front>, void>::type insert_node(node_type* p_value)
                {
                    if (empty())
                    {
                        insert_first_node(p_value);
                    }
                    else
                    {
                        node_type* temp = p_value;
                        temp->back = front_head;
                        temp->front = nullptr;
                        front_head = temp;
                        _size++;
                    }
                }

                template<typename T>
                enable_if<is_same_v<T, _internal::back>, void>::type insert_node(node_type* p_value)
                {
                    if (empty())
                    {
                        insert_first_node(p_value);
                    }
                    else
                    {
                        node_type* temp = p_value;
                        temp->front = back_head;
                        temp->back = nullptr;
                        back_head = temp;
                        _size++;
                    }
                }

                void insert_first_node(node_type* p_value)
                {
                    front_head = p_value;
                    back_head = p_value;
                    p_value->back = nullptr;
                    p_value->front = nullptr;
                    _size = 1;
                }

                node_type* _generate_node(const T& p_value)
                {
                    node_type* node = allocator.allocate(1);
                    allocator_node.construct(node, p_value);
                    return node;
                }

                node_type* _generate_node(T&& p_value)
                {
                    node_type* node = allocator.allocate(1);
                    allocator_node.construct(node, p_value);
                    return node;
                }

                template<typename... Args>
                node_type* _generate_node(Args&&... p_args)
                {
                    node_type* node = allocator_node.allocate(1);
                    allocator_node.construct(node, std::forward<Args>(p_args)...);
                    return node;
                }

            private:
                node_type* back_head;
                node_type* front_head;

                AllocatorT allocator;
                node_allocator allocator_node;

                size_t _size;


    };

    template<typename T>
    using Dequeue = BDequeue<T>;

    template<typename T>
    struct DequeueBaseFrontIterator
    {
        public:
            using iterator_category = std::forward_iterator_tag;
            using difference_type = std::ptrdiff_t;
            using value_type = T;
            using pointer = T*;
            using reference = T&;
        
        private:
            using node_type = _internal::BDequeueNode<T>;
            using base = DequeueBaseFrontIterator<T>;


        
        public:
            DequeueBaseFrontIterator(node_type* p_ptr): ptr(p_ptr)
            {}

        
        public:
            reference operator*()
            {
                return ptr->value;
            }

            pointer operator->()
            {
                return &(ptr->value);
            }

            node_type& operator++() // pre
            {
                ptr = ptr->front;
                return *this;
            }

            const node_type& operator++() const // pre
            {
                node_type& p = const_cast<node_type&>(ptr);
                p = p->front;
                return *this;
            }

            node_type& operator++(int) // post
            {
                node_type temp = *this;
                ptr = ptr->front;
                return temp;
            }

            const node_type& operator++(int) const // post
            {
                node_type temp = *this;
                node_type& p = const_cast<node_type&>(ptr);
                p = p->front;
                return temp;
            }

        private:
            node_type* ptr;
    };
}

#endif