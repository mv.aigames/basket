#ifndef BASKET__STD_ARRAY_HPP
#define BASKET__STD_ARRAY_HPP

#include "_iterator.hpp"

// #include <core/pch.hpp>

namespace basket
{

    template<typename T, size_t N>
    class array
    {
        T _data[N];
        public:
            using iterator = ContinuousIterator<T>;

        public:
            array() = default;
            array(const array<T, N>& p_other) = default;
            array(array<T, N>&& p_other) = default;
            array& operator=(const array<T, N>& p_other) = default;
            array& operator=(array<T, N>&& p_other) = default;

        public:
            T& operator[](int p_index)
            {
                return _data[p_index];
            }

        public:

            T* data();
            iterator begin();
            iterator end();
            size_t size() const;
    };

    template<typename T, size_t N>
    T* array<T, N>::data()
    {
        return _data;
    }

    template<typename T, size_t N>
    array<T, N>::iterator array<T, N>::begin()
    {
        return iterator(_data);
    }

    template<typename T, size_t N>
    array<T, N>::iterator array<T, N>::end()
    {
        return iterator(_data + N);
    }

    template<typename T, size_t N>
    size_t array<T, N>::size() const
    {
        return N;
    }

    // #ifdef BASKET_STD_CPP
    //     template<typename T, size_t N>
    //     using Array = std::array<T, N>;
    // #else
    //     template<typename T, size_t N>
    //     using Array = array<T, N>;
    // #endif
}

#endif