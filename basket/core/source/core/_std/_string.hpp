#ifndef BASKET__STD_STRING_HPP
#define BASKET__STD_STRING_HPP

#include <string.h>
// #include <compare>
#include "_memory/allocator.hpp"

#include "_string/_char_traits.hpp"

namespace basket
{
    template<typename CharType, typename Traits = char_traits<CharType>, typename Alloc = Allocator<CharType>>
    class basic_string
    {
        public:
            using value_type = CharType;
            using traits_type = Traits;
            using allocator_type = Alloc;
            using pointer = value_type*;
            using const_pointer = const pointer;
            using reference = value_type&;
            using const_reference = reference;
            using size_type = size_t;
            using _base = basic_string<CharType, Traits, Alloc>;

            static constexpr size_type npos = (size_t)-1;

        private:
            pointer _data;
            size_type _length;
            size_type _reserved_size;
            allocator_type _allocator;


        private:
            void _reserve(size_type p_amount)
            {
                _data = _allocator.allocate(p_amount);
            }

            void _resize(size_type p_amount)
            {
                _data = _allocator.allocate(p_amount + 1);
                _length = p_amount;
                memset(_data, 0, p_amount + 1);
            }

            size_type _find_char(CharType p_char, size_type p_pos = npos) const
            {
                if (p_pos >= size())
                {
                    return npos;
                }

                pointer s = c_str();

                for (size_type index = p_pos; index < _length; ++index)
                {
                    if (*(s + index) == p_char)
                    {
                        return index;
                    }
                }

                return npos;
            }

            size_type _rfind_char(CharType p_char, size_type p_pos = npos) const
            {
                int64_t start_index = p_pos >= size() ? size() - 1 : p_pos;

                pointer s = c_str();

                for (int64_t index = start_index; index >= 0; --index)
                {
                    if (*(s + index) == p_char)
                    {
                        return index;
                    }
                }

                return npos;
            }

        public:

            pointer data() const
            {
                return _data;
            }

            void reserve(size_type p_amount)
            {
                _reserve(p_amount);
            }

            void resize(size_type p_amount)
            {
                _resize(p_amount);
            }

            constexpr const_pointer c_str() const
            {
                return _data;
            }

            constexpr size_type size() const
            {
                return _length;
            }

            constexpr size_type length() const
            {
                return _length;
            }

            size_type rfind(CharType p_char, size_type p_pos = npos) const
            {
                return _rfind_char(p_char, p_pos);
            }

            size_type find(CharType p_char, size_type p_pos = 0) const
            {
                return _find_char(p_char, p_pos);
            }

            basic_string<CharType, Traits, Alloc> substr(size_type p_pos = 0, size_type p_count = npos) const
            {
                _base s;
                s.resize(p_count);
                memcpy(s.c_str(), c_str() + p_pos, p_count);
                return s;
            }
        
        private:
            void _copy_cstring(const CharType* const p_cstring)
            {
                size_type l = strlen(p_cstring);
                if (_data)
                {
                    _allocator.deallocate(_data, l);
                }

                _data = _allocator.allocate(l + 1);

                strcpy(_data, p_cstring);
                _length = l;
            }

            void _copy(const basic_string<value_type, traits_type, allocator_type>& p_other)
            {
                if (_data)
                {
                    _allocator.deallocate(_data, _length);
                }

                _data = _allocator.allocate(p_other._length + 1);
                strcpy(_data, p_other._data);
                _length = p_other._length;
            }

            void _move(basic_string<value_type, traits_type, allocator_type>&& p_other)
            {
                _data = p_other._data;
                p_other._data = nullptr;
                _length = p_other._length;
                p_other._length = 0;
            }

            void _append(const CharType* const p_s1, size_type p_l1, const CharType* const p_s2, size_type p_l2)
            {
                size_type total_size = p_l1 + p_l2;
                CharType* new_block = _allocator.allocate(total_size + 1);
                memcpy(new_block, p_s1, p_l1);
                strncpy(new_block + p_l1, p_s2, p_l2);
                *(new_block + total_size) = '\0';
                _allocator.deallocate(_data, _length);
                _data = new_block;
                _length = total_size;
            }

            void _append(const basic_string<CharType, Traits, Alloc>& p_string)
            {
                _append(c_str(), size(), p_string.c_str(), p_string.size());
            }

            void _destroy()
            {
                _allocator.deallocate(_data, _length);
                // _data = nullptr;
                _length = 0;
            }

        public:
            basic_string(): _data(nullptr) {}
            basic_string(const CharType* const p_cstring, const Allocator<CharType>& p_alloc = Allocator<CharType>()): basic_string()
            {
                _copy_cstring(p_cstring);
            }

            basic_string(const basic_string<value_type, traits_type, allocator_type>& p_other): basic_string()
            {
                _copy(p_other);
            }

            basic_string(basic_string<value_type, traits_type, allocator_type>&& p_other): basic_string()
            {
                _move(move(p_other));
            }

            basic_string<value_type, traits_type, allocator_type>& operator=(const basic_string<value_type, traits_type, allocator_type>& p_other)
            {
                _copy(p_other);
                return *this;
            }

            basic_string<value_type, traits_type, allocator_type>& operator=(basic_string<value_type, traits_type, allocator_type>&& p_other)
            {
                _move(move(p_other));
                return *this;
            }

            basic_string<value_type, traits_type, allocator_type>& operator=(const CharType* const p_cstring)
            {
                _copy_cstring(p_cstring);
                return *this;
            }

            ~basic_string()
            {
                _destroy();
            }
        
        public:
            // template<typename BasicStringType>
            bool operator ==(const basic_string<CharType, Traits, Alloc>& p_right)
            {
                return strcmp(c_str(), p_right.c_str()) == 0;
            }

            template<typename BasicStringType>
            friend constexpr BasicStringType operator +(const BasicStringType& p_left, const BasicStringType& p_right);

            template<typename BasicStringType>
            friend constexpr BasicStringType operator +(const typename BasicStringType::value_type* const p_cstring, const BasicStringType& p_right);

            template<typename BasicStringType>
            friend constexpr BasicStringType operator +(const BasicStringType& p_left, const typename BasicStringType::value_type* const p_cstring);

            basic_string<CharType, Traits, Alloc>& operator +=(const basic_string<CharType, Traits, Alloc>& p_right)
            {
                return *this;
            }

            const CharType& operator[] (size_type p_index) const
            {
                return *(_data + p_index);
            }

            CharType& operator[] (size_type p_index)
            {
                return *(_data + p_index);
            }
    };

    template<typename BasicStringType>
    constexpr BasicStringType operator +(const BasicStringType& p_left, const BasicStringType& p_right)
    {
        BasicStringType s(p_left);
        s._append(p_right);
        return s;
    }

    template<typename BasicStringType>
    constexpr BasicStringType operator +(const typename BasicStringType::value_type* const p_cstring, const BasicStringType& p_right)
    {
        BasicStringType s(p_right);
        s._append(p_cstring, strlen(p_cstring), p_right.c_str(), p_right.size());
        return s;
        
    }

    template<typename BasicStringType>
    constexpr BasicStringType operator +(const BasicStringType& p_left, const typename BasicStringType::value_type* const p_cstring)
    {
        BasicStringType s(p_left);
        s._append(p_left.c_str(), p_left.size(), p_cstring, strlen(p_cstring));
        return s;
    }

    using string = basic_string<char>;
}



#endif