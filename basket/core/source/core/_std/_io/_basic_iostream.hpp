#ifndef BASKET__BASIC_IOSTREAM_HPP
#define BASKET__BASIC_IOSTREAM_HPP

#include "_basic_istream.hpp"
#include "_basic_ostream.hpp"

namespace basket
{
    template<typename CharType, typename Traits = char_traits<CharType>>
    class basic_iostream: public basic_istream<CharType>, public basic_ostream<CharType>
    {
        public:

    };

    using iostream = basic_iostream<char>;
}

#endif