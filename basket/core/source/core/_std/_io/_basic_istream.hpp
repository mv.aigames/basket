#ifndef BASKET__BASIC_ISTREAM_HPP
#define BASKET__BASIC_ISTREAM_HPP

#include "_basic_ios.hpp"

#include "../_string.hpp"

namespace basket
{
    template<typename CharType, typename Traits = char_traits<CharType>>
    class basic_istream: virtual public basic_ios<CharType>
    {
        private:
            using _bis = basic_istream<CharType, Traits>;

        public:
            _bis& read(CharType* p_buffer, streamsize p_count)
            {
                this->rdbuf()->sgetn(p_buffer, p_count);
                return *this;
            }

        public:
            _bis& operator>>(basic_string<char>& p_string)
            {
                
                
                return *this;
            }
    };

    using istream = basic_istream<char>;
}

#endif