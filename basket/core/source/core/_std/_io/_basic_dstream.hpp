#ifndef BASKET__BASIC_DSTREAM_HPP
#define BASKET__BASIC_DSTREAM_HPP

#include "_basic_iostream.hpp"
#include "../_stream/_basic_dbuf.hpp"
#include "../_string.hpp"


namespace basket
{
    template<typename CharType, typename Traits = char_traits<CharType>>
    class basic_dstream: public basic_iostream<CharType>
    {
        private:
            using _base_buffer = basic_streambuf<CharType, Traits>;
            using _buffer_type = basic_dbuf<CharType, Traits>;
            _buffer_type _dbuffer;

        private:
            void _open(const char* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {
                _base_buffer* bf = _dbuffer.open(p_filename, p_mode);
                if (!bf)
                {
                    this->clear(ios_base::failbit);
                }
                else
                {
                    this->clear(ios_base::goodbit);
                }
            }

        public:
            _buffer_type* rdbuf() const
            {
                return const_cast<_buffer_type*>(&_dbuffer);    
            }
            
        public:
            basic_dstream<CharType>& operator= (basic_dstream<CharType, Traits>&& p_other)
            {
                return *this;
            }

            void open(const char* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {
                _open(p_filename, p_mode);
            }

            void open(const string& p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {
                _open(p_filename.c_str(), p_mode);
            }

            void close()
            {
                _dbuffer.close();
            }

        public:
            basic_dstream()
            {
                this->init(&_dbuffer);
                
            }

            explicit basic_dstream(const char* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out): basic_dstream()
            {
                _open(p_filename, p_mode);
            }

            explicit basic_dstream(const string* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out): basic_dstream()
            {
                _open(p_filename.c_str(), p_mode);
            }

            template<typename FPathType>
            basic_dstream(const FPathType& filename, ios_base::openmode p_mode = ios_base::in | ios_base::out): basic_dstream()
            {

            }

            basic_dstream(basic_dstream<CharType, Traits>&& p_other): basic_dstream()
            {

            }

            basic_dstream(const basic_dstream<CharType, Traits>&) = delete;
    };

    using dstream = basic_dstream<char>;
}

#endif