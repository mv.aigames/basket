#ifndef BASKET__IOS_BASE_TYPES_HPP
#define BASKET__IOS_BASE_TYPES_HPP

#include <cstdint>

namespace basket
{
    typedef int64_t streamoff;
    typedef size_t streamsize;
}

#endif