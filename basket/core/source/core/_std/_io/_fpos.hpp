#ifndef BASKET__FPOS_HPP
#define BASKET__FPOS_HPP

#include "_ios_base_types.hpp"
// #include "../_string/_char_traits.hpp"

namespace basket
{
    template<typename C>
    struct char_traits;

    template<typename S>
    class fpos
    {
        streamoff _position;
        S _state;
    };

    // using streampos = fpos<char_traits<char>::state_type>;
}

#endif