#ifndef BASKET__IOS_BASE_HPP
#define BASKET__IOS_BASE_HPP

#include <cstdint>
// #include "_fpos.hpp"
// #include "../_string/_char_traits.hpp"

namespace basket
{
    

    

    

    class ios_base
    {
        public:
            typedef uint8_t openmode;

            static constexpr openmode app       = 1;
            static constexpr openmode binary    = 1 << 1;
            static constexpr openmode in        = 1 << 2;
            static constexpr openmode out       = 1 << 3;
            static constexpr openmode trunc     = 1 << 4;
            static constexpr openmode ate       = 1 << 5;
        
        public:
            typedef uint32_t fmtflags;

            static constexpr fmtflags dec           = 1;
            static constexpr fmtflags oct           = 1 << 1;
            static constexpr fmtflags hex           = 1 << 2;
            static constexpr fmtflags basefield     = dec | oct | hex;

            static constexpr fmtflags left          = 1 << 3;
            static constexpr fmtflags right         = 1 << 4;
            static constexpr fmtflags internal      = 1 << 5;
            static constexpr fmtflags adjustfield   = left | right | internal;

            static constexpr fmtflags scientific    = 6;
            static constexpr fmtflags fixed         = 7;
            static constexpr fmtflags floatfield    = scientific | fixed;

            static constexpr fmtflags boolalpha     = 1 << 8;
            static constexpr fmtflags showbase      = 1 << 9;
            static constexpr fmtflags showpoint     = 1 << 10;
            static constexpr fmtflags showpos       = 1 << 11;
            static constexpr fmtflags skipws        = 1 << 12;
            static constexpr fmtflags unitbuf       = 1 << 13;
            static constexpr fmtflags uppercase     = 1 << 14;

        public:
            typedef uint8_t iostate;

            static constexpr iostate goodbit        = 0;
            static constexpr iostate badbit         = 1;
            static constexpr iostate failbit        = 1 << 1;
            static constexpr iostate eofbit         = 1 << 2;
        
        public:
            typedef uint8_t seekdir;

            static constexpr seekdir beg            = 1;
            static constexpr seekdir end            = 1 << 1;
            static constexpr seekdir cur            = 1 << 2;

        private:
            fmtflags _flags;

        public:
            fmtflags flags() const
            {
                return _flags;
            }

            fmtflags flags(fmtflags p_flags)
            {
                _flags = p_flags;
                return _flags;
            }
        
        
        protected:
            void init()
            {

            }



        public:
            ios_base(const ios_base&) = delete;
            ios_base& operator= (const ios_base&) = delete;
            virtual ~ios_base() {}

        protected:
            ios_base()
            {

            }

    };
}

#endif