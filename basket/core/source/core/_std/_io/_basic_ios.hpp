#ifndef BASKET__BASIC_IOS_HPP
#define BASKET__BASIC_IOS_HPP

#include "_ios_base.hpp"
#include "../_string/_char_traits.hpp"
#include "../_utility.hpp"
#include "../_stream/_basic_streambuf.hpp"

namespace basket
{
    template<typename CharType, typename Traits = char_traits<CharType>>
    class basic_ios: public ios_base
    {
        private:
            using _bf = basic_streambuf<CharType, Traits>;
        public:
            using char_type = CharType;
            using traits_type = Traits;
            static_assert(is_same_v<char_type, traits_type::char_type>, "CharType is not equal to char_traits<CharType>::char_type");
            using int_type = traits_type::int_type;
            using pos_type = traits_type::pos_type;
            using off_type = traits_type::off_type;
        
        private:
            _bf* _buffer;
            ios_base::iostate _state;

        private:
            void _clear(ios_base::iostate p_state = ios_base::goodbit)
            {
                _state = p_state;
            }

        public:
            bool good() const
            {
                return rdstate() == goodbit;
            }

            bool fail() const
            {
                return (rdstate() & ios_base::badbit) != 0 || (rdstate() & ios_base::failbit) != 0;
            }

            bool bad() const
            {
                return (rdstate() & ios_base::badbit) != 0;
            }

            bool eof() const
            {
                return (rdstate() & ios_base::eofbit) != 0;
            }

            bool operator!() const
            {
                return fail();
            }

            operator bool() const
            {
                return !fail();
            }

            _bf* rdbuf() const
            {
                return _buffer;
            }

            _bf* rdbuf(_bf* p_buffer) const
            {
                _buffer = p_buffer;
                return _buffer;
            }

            ios_base::iostate rdstate() const
            {
                return _state;
            }

            ios_base::iostate setstate(ios_base::iostate p_state)
            {
                _clear(rdstate() | p_state);
            }

            void clear(ios_base::iostate p_state = ios_base::goodbit)
            {
                _clear(p_state);
            }



            

        protected:
            void setrdbuf(_bf* p_sb)
            {
                _buffer = p_sb;
            }

            basic_ios(): ios_base()
            {

            }

            explicit basic_ios(basic_streambuf<CharType, Traits>* p_sb)
            {
                init(p_sb);
            }

            void init(_bf* p_sb)
            {
                setrdbuf(p_sb);
                ios_base::iostate state;
                state = rdbuf() ? ios_base::goodbit : ios_base::badbit;
                flags(ios_base::skipws | ios_base::dec);
            }

        public:

            virtual ~basic_ios()
            {

            }
    };
}

#endif