#ifndef BASKET__BASIC_OSTREAM_HPP
#define BASKET__BASIC_OSTREAM_HPP

#include "_basic_ios.hpp"

namespace basket
{
    template<typename CharType, typename Traits = char_traits<CharType>>
    class basic_ostream: virtual public basic_ios<CharType>
    {
        private:
            using _bo = basic_ostream<CharType, Traits>;

        public:
            _bo& operator << (string& p_string)
            {
                this->rdbuf()->sputn(p_string.c_str(), p_string.size());
                return *this;
            }


    };

    using ostream = basic_ostream<char>;
}

#endif