#ifndef BASKET__BASIC_FSTREAM_HPP
#define BASKET__BASIC_FSTREAM_HPP

#include "_basic_iostream.hpp"
#include "../_stream/_basic_filebuf.hpp"
#include "../_stream/_basic_dbuf.hpp"
#include "../_string.hpp"


namespace basket
{
    template<typename CharType, typename Traits = char_traits<CharType>>
    class basic_fstream: public basic_iostream<CharType>
    {
        private:
            using _buffer_type = basic_dbuf<CharType, Traits>;
            _buffer_type _filebuffer;

        public:
            _buffer_type* rdbuf() const
            {
                return const_cast<_buffer_type*>(&_filebuffer);    
            }
            
        public:
            basic_fstream<CharType>& operator= (basic_fstream<CharType, Traits>&& p_other)
            {
                return *this;
            }

            void open(const char* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {

            }

            void open(const string& p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {

            }

        public:
            basic_fstream() {}
            explicit basic_fstream(const char* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out) {}

            explicit basic_fstream(const string* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out) {}
            template<typename FPathType>
            basic_fstream(const FPathType& filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
            {

            }

            basic_fstream(basic_fstream<CharType, Traits>&& p_other)
            {

            }

            basic_fstream(const basic_fstream<CharType, Traits>&) = delete;
    };

    using fstream = basic_fstream<char>;
}

#endif