#ifndef BASKET__CHAR_TRAITS_HPP
#define BASKET__CHAR_TRAITS_HPP

#include <cstdio>
#include <cstring>
#include <cwchar>

#include "../_io/_ios_base_types.hpp"
#include "../_io/_fpos.hpp"

namespace basket
{
    template<typename CharType>
    struct _base_char_traits;

    template<>
    struct _base_char_traits<char>
    {
        using char_type     = char;
        using int_type      = int;
        using state_type    = mbstate_t;
        using pos_type      = fpos<state_type>;
        using off_type      = streamoff;

        static constexpr int _eof = EOF;

    };

    template<typename T>
    struct char_traits
    {
        using _traits       = _base_char_traits<T>;
        using char_type     = _traits::char_type;
        using int_type      = _traits::int_type;
        using state_type    = _traits::state_type;
        using pos_type     = _traits::pos_type;
        using off_type      = _traits::off_type;

        static constexpr bool eq (const char_type& p_left, const char_type& p_right)
        {
            return p_left = p_right;
        }

        static constexpr bool lt (const char_type& p_left, const char_type& p_right)
        {
            return p_left < p_right;
        }

        static constexpr bool gt (const char_type& p_left, const char_type& p_right)
        {
            return p_left > p_right;
        }

        static constexpr void assign (char_type& p_left, const char_type& p_right)
        {
            return p_left = p_right;
        }

        static constexpr int eof ()
        {
            return _traits::_eof;
        }

        static constexpr int8_t compare(char_type* p_left, const char_type* p_right)
        {
            return strcmp(p_left, p_right);
        }
    };
}

#endif