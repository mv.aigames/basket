#ifndef BASKET_COMPRESSED_PAIR_HPP
#define BASKET_COMPRESSED_PAIR_HPP

namespace basket
{


    template<typename TF, typename TS>
    struct compressed_pair final: TF
    {
        TS second;

        struct OneArg{};
        struct ZeroArg{};

        compressed_pair(TF&& p_first, TS&& p_second): TF(p_first), second(p_second) {}
        compressed_pair(OneArg, TF&& p_first): TF(p_first) {}
        compressed_pair(const TS& p_value): second(p_value) {}
        compressed_pair(TS&& p_value): second(p_value) {}

        TF& get_first()
        {
            return *this;
        }

        const TF& get_first() const
        {
            return *this;
        }
    };
}

#endif