#ifndef BASKET__STD_UTILITY_HPP
#define BASKET__STD_UTILITY_HPP

namespace basket
{
    template<typename T>
    class reference_wrapper
    {
        private:
            T& value;
        public:

    };


    template<typename T, T v>
    struct integral_constant
    {
        using type = integral_constant;
        using value_type = T;
        static constexpr value_type value = v;
        value_type operator()() const { return value; }
        // value_type operator value_type() const { return value; }
    };

    using true_type = integral_constant<bool, true>;
    using false_type = integral_constant<bool, false>;

    template<typename T, typename U>
    struct is_same: false_type {};

    template<typename T>
    struct is_same<T, T>: true_type {};

    template<typename T, typename V>
    constexpr bool is_same_v = is_same<T, V>::value;

    template<bool C, typename T1, typename T2>
    struct conditional
    {
        using type = T1;
    };

    template<typename T1, typename T2>
    struct conditional<false, T1, T2>
    {
        using type = T2;
    };

    template<bool C, typename T1, typename T2>
    using conditional_t = typename conditional<C, T1, T2>::type;


    template<typename T>
    struct remove_reference
    {
        using type = T;
        using _original_type = T;
    };

    template<typename T>
    struct remove_reference<T&>
    {
        using type = T;
        using _original_type = T&;
    };

    template<typename T>
    struct remove_reference<T&&>
    {
        using type = T;
        using _original_type = T&&;
    };

    template<typename T>
    using remove_reference_t = typename remove_reference<T>::type;

    template<typename T>
    constexpr T& forward(remove_reference_t<T>& p_value)
    {
        return p_value;
    }

    template<typename T>
    constexpr T&& forward(remove_reference_t<T>&& p_value)
    {
        return p_value;
    }

    

    // template<typename T>
    // using decay = std::decay<T>;

    template<typename T, T... Args>
    struct integer_sequence
    {
        using value_type = T;
        static constexpr size_t size() noexcept
        {
            return sizeof...(Args);
        }
    };

    #ifdef BASKET_WINDOWS
    template<typename T, size_t N>
    using make_integer_sequence = __make_integer_seq<integer_sequence, T, N>;
    #else
        #error "integer sequence only for windows"
    #endif

    template<size_t... Args>
    using index_sequence = integer_sequence<size_t, Args...>;

    template<size_t N>
    using make_index_sequence = make_integer_sequence<size_t, N>;

    template<typename... Args>
    using index_sequence_for = make_index_sequence<sizeof...(Args)>;

    template<typename T>
    constexpr remove_reference_t<T>&& move(T&& p_value)
    {
        return static_cast<remove_reference_t<T>&&>(p_value);
    }

    template<bool, class = void>
    struct enable_if
    {

    };

    template<class T>
    struct enable_if<true, T>
    {
        using type = T;
    };

    template<bool C, typename T>
    using enable_if_t = enable_if<C, T>::type;

    template<typename... Args>
    using void_t = void;

    template<typename T>
    struct remove_cv
    {
        using type = T;
        using _original = T;
    };

    template<typename T>
    struct remove_cv<const T>
    {
        using type = T;
        using _original = const T;
    };

    template<typename T>
    struct remove_cv<volatile T>
    {
        using type = T;
        using _original = volatile T;
    };

    template<typename T>
    struct remove_cv<const volatile T>
    {
        using type = T;
        using _original = const volatile T;
    };


    template<typename T>
    using remove_cv_t = remove_cv<T>::type;

    template<typename T>
    struct add_lvalue_reference
    {
        using type = remove_reference<T>::type&;
    };

    template<typename T>
    using add_lvalue_reference_t = add_lvalue_reference<T>::type;

    template<typename T>
    struct add_rvalue_reference
    {
        using type = remove_reference<T>::type&&;
    };

    template<typename T>
    using add_rvalue_reference_t = add_rvalue_reference<T>::type;

    template<typename T>
    struct remove_extent
    {
        using type = T;
    };

    template<typename T>
    struct remove_extent<T[]>
    {
        using type = T;
    };

    template<typename T, int Index>
    struct remove_extent<T[Index]>
    {
        using type = T;
    };

    template<typename T>
    struct is_array: false_type {};

    template<typename T>
    struct is_array<T[]>: true_type {};

    template<typename T, int Index>
    struct is_array<T[Index]>: true_type {};

    template<typename T>
    constexpr auto is_array_v = is_array<T>::value;

    template<typename T>
    struct add_pointer
    {
        using type = remove_reference<T>::type*;
    };

    template<typename T>
    struct add_pointer<T*>
    {
        using type = T*;
        using _base = T;
    };

    template<typename T>
    using add_pointer_t = add_pointer<T>::type;


    template<typename T>
    struct is_pointer: false_type {};

    template<typename T>
    struct is_pointer<T*> : true_type {};

    template<typename T>
    struct is_pointer<T* const> : true_type {};

    template<typename T>
    struct is_pointer<T* volatile> : true_type {};

    template<typename T>
    struct is_pointer<T* const volatile> : true_type {};

    template<typename T>
    constexpr auto is_pointer_v = is_pointer<T>::value;

    template<typename T>
    struct is_lvalue_reference: false_type {};

    template<typename T>
    struct is_lvalue_reference<T&>: true_type {};

    template<typename T>
    constexpr auto is_lvalue_reference_v = is_lvalue_reference<T>::value;

    template<typename T>
    struct is_rvalue_reference: false_type {};

    template<typename T>
    struct is_rvalue_reference<T&&>: true_type {};

    template<typename T>
    constexpr auto is_rvalue_reference_v = is_rvalue_reference<T>::value;

    template<typename T>
    struct is_reference: false_type {};

    template<typename T>
    struct is_reference<T&>: true_type {};

    template<typename T>
    struct is_reference<T&&>: true_type {};

    template<typename T>
    struct is_function: false_type {};

    template<typename R, typename... Args>
    struct is_function<R(Args...)>: true_type{};

    template<typename T>
    constexpr auto is_function_v = is_function<T>::value;

    template<typename T>
    struct decay
    {
        using _base = remove_reference_t<T>;

        using type = conditional_t<
            is_array_v<_base>,
            typename remove_extent<_base>::type*,
            conditional_t<is_function_v<_base>,
                add_pointer_t<_base>,
                remove_cv_t<remove_reference_t<_base>>
            >
        >;
    };

    template<typename T>
    using decay_t = decay<T>::type;
}

#endif