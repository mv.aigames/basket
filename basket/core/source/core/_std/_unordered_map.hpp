#ifndef BASKET__STD_UNORDERED_MAP_HPP
#define BASKET__STD_UNORDERED_MAP_HPP

// #include <unordered_map>
#include "_memory.hpp"
#include "_pair.hpp"

namespace basket
{
    template<   typename KType, typename VType,
                typename HashOperator = std::hash<KType>,
                typename EqualOperator = std::equal_to<KType>,
                typename AllocatorType = std::allocator<pair<KType, VType>>>
    class BHashMap: public std::unordered_map<KType, VType, HashOperator, EqualOperator, AllocatorType>
    {

    };

    // template<   typename KType, typename VType>
    // using HashMap = std::unordered_map<KType, VType>;
}

#endif