#ifndef BASKET_ALLOCATOR_HPP
#define BASKET_ALLOCATOR_HPP

#include <core/pch.hpp>
#include "allocator_traits.hpp"


namespace basket
{
    



    class AllocatorTracker
    {
        private:
            int64_t total;

        public:
            static AllocatorTracker& get()
            {
                static AllocatorTracker instance;
                return instance;
            }
            static void allocate(int64_t p_size) { get().update_allocate(p_size); }
            static void deallocate(int64_t p_size) { get().update_deallocate(p_size); }
            static int64_t get_memory_use() { return get().memory_use(); }
            
        private:
            AllocatorTracker(): total(0) {}
            void update_allocate(int64_t p_size)  { total += p_size; }
            void update_deallocate(int64_t p_size)  { total -= p_size; }
            int64_t memory_use()  { return total; }

    };

#ifdef BASKET_ENABLE_ALLOCATOR_TRACK
    #define TRACK_ALLOCATE(p_size) AllocatorTracker::allocate(p_size);
    #define TRACK_DEALLOCATE(p_size) AllocatorTracker::deallocate(p_size);
#else
    #define TRACK_ALLOCATE(p_size)
    #define TRACK_DEALLOCATE(p_size)
#endif


    template<typename T>
    class Allocator
    {
        public:
            using value_type = T;
            // using reference = T&;
            using pointer = T*;
            using size_type = std::size_t;
            using difference_type = std::ptrdiff_t;
            using propagate_on_container_move_assignment = std::true_type;
            using traits = allocator_traits<Allocator<T>>;

            template<typename U>
            struct rebind
            {
                using other = Allocator<U>;
            };
        
        public:
            [[nodiscard]]
            pointer allocate(size_type p_n, const void* p_hint = 0)
            {
                // TRACK_ALLOCATE(sizeof(T) * p_n);
                return reinterpret_cast<T*>(malloc(sizeof(T) * p_n));
            }
            
            template<typename... Args>
            void construct(pointer p_ptr, Args&&... p_args)
            {
                // static_assert(!is_same_v<value_type, Args> && ..., "they are the same");
                ::new((void*)p_ptr) value_type(std::forward<Args>(p_args)...);
            }

            void construct(pointer p_ptr, const T& p_value)
            {
                ::new((void*)p_ptr) value_type(p_value);
            }

            void destroy(pointer p_p)
            {
                p_p->~T();
            }

            void deallocate(T* p_pointer, size_t p_n);


        public:
            Allocator()  = default;
            // constexpr Allocator()  = default;
            Allocator(const Allocator& p_other)  = default;
            // constexpr Allocator(const Allocator& p_other)  = default;
            ~Allocator() = default;
            // constexpr ~Allocator() = default;

    };

    template<typename T>
    void Allocator<T>::deallocate(T* p_pointer, size_t p_n)
    {
        // TRACK_DEALLOCATE(sizeof(T) * p_n);
        free(p_pointer);
    }
}

#endif
