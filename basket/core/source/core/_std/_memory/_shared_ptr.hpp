#ifndef BASKET__STD_SHARED_PTR_HPP
#define BASKET__STD_SHARED_PTR_HPP

#include <core/typedefs.hpp>
#include "_ref_counter.hpp"

namespace basket
{
    template<typename T>
    class shared_ptr
    {
        private:
            _Ref_Counter<T>* _ref_counter;
        
        public:
            size_t use_count() const
            {
                return _ref_counter.count();
            }
        
        private:
            void _increment_ref_counter()
            {
                ++(*_ref_counter);
            }

            void _decrement_ref_counter()
            {
                --(*_ref_counter);
                if (_ref_counter->count() == 0)
                {
                    delete _ref_counter->_ref;
                    delete _ref_counter;
                    _ref_counter = nullptr;
                }
            }

            /**
             * Initialize with a raw pointer.
             * 
             * 1 - Check if already has a _ref assigned.
             * 2 - If already exist, destroy it.
             * 3 - Create a new one.
             */
            void _initialize(T* p_pointer)
            {
                if (_ref_counter)
                {
                    _decrement_ref_counter();
                }

                if (p_pointer)
                {
                    _ref_counter = new _Ref_Counter<T>(p_pointer);
                }
            }

            template<typename U>
            void _initialize_from_shared_ptr(const shared_ptr<U>& p_shared)
            {
                _ref_counter = p_shared._ref_counter;
                if (this->_ref_counter)
                {
                    
                    _increment_ref_counter();
                    return;
                }

                if (_ref_counter)
                {

                }
            }

            template<typename U>
            void _copy(const shared_ptr<U>& p_shared)
            {
                #ifndef BBK_NO_MERCY
                    if (_ref_counter == p_shared._ref_counter)
                    {
                        return;
                    }
                #endif

                if (_ref_counter)
                {
                    _decrement_ref_counter();
                    _ref_counter = nullptr;
                }

                _ref_counter = p_shared._ref_counter;

                if (_ref_counter)
                {
                    _increment_ref_counter();
                }
            }

            template<typename U>
            void _move(shared_ptr<U>&& p_shared)
            {
                if (_ref_counter)
                {
                    _decrement_ref_counter();
                    // _ref_counter = nullptr;
                }

                _ref_counter = p_shared._ref_counter;
            }

        public:
            T& operator*()
            {
                return *_ref_counter->_ref;
            }

            T* operator->()
            {
                return _ref_counter->_ref;
            }

            constexpr operator bool()
            {
                return _ref_counter != nullptr;
            }

            T* get() const
            {
                return _ref_counter->_ref;
            }

        public:
            shared_ptr<T>& operator=(const shared_ptr<T>& p_other)
            {
                _copy(p_other);
                return *this;
            }

            template<typename U>
            shared_ptr<T>& operator=(const shared_ptr<U>& p_other)
            {
                _copy(p_other);
                return *this;
            }

            shared_ptr<T>& operator=(shared_ptr<T>&& p_other)
            {
                _move(p_other);
                return *this;
            }

            template<typename U>
            shared_ptr<T>& operator=(shared_ptr<U>&& p_other)
            {
                _move(p_other);
                return *this;
            }


        public:
            shared_ptr(): _ref_counter(nullptr)
            {
                _initialize(nullptr);
            }

            shared_ptr(nullptr_t): shared_ptr()
            {

            }

            template<typename U>
            shared_ptr(U* p_pointer): shared_ptr(nullptr)
            {
                _initialize(p_pointer);
            }

            shared_ptr(const shared_ptr<T>& p_other): shared_ptr()
            {
                _copy(p_other);
            }

            template<typename U>
            shared_ptr(const shared_ptr<U>& p_other): shared_ptr()
            {
                _copy(p_other);
            }

            shared_ptr(shared_ptr<T>&& p_other): shared_ptr()
            {
                _move(p_other);
            }

            template<typename U>
            shared_ptr(shared_ptr<U>&& p_other): shared_ptr()
            {
                _move(p_other);
            }

            template<typename U, typename D>
            shared_ptr(U* p_pointer, D p_deleter)
            {

            }

            ~shared_ptr()
            {
                if (_ref_counter)
                {
                    _decrement_ref_counter();
                }
            }
    };
}

#endif