#ifndef BASKET_UNIQUE_PTR_HPP
#define BASKET_UNIQUE_PTR_HPP

// #include "../_std/_compressed_pair.hpp"
#include "../_compressed_pair.hpp"
#include <core/typedefs.hpp>
#include <core/logging.hpp>

#ifndef BK_NO_MERCY
#define _BK_UNIQUE_PTR_SAFE_RELEASE(P_PTR) \
        if (P_PTR)\
            delete P_PTR
#else
    #define _BK_UNIQUE_PTR_SAFE_RELEASE(P_PTR) delete P_PTR
#endif

namespace basket
{

    template<typename T>
    struct default_deleter
    {
        void operator()(T* p_ptr)
        {
            BK_LOG(core, info, "Destroying object");
            _BK_UNIQUE_PTR_SAFE_RELEASE(p_ptr);

        }
    };

    template<typename T, typename D = default_deleter<T>>
    class unique_ptr
    {
        using deleter_type = D;

        using value_type = T;
        using pointer = T*;
        using const_pointer = const pointer;
        using reference = value_type&;
        using const_reference = const reference;

        private:
            compressed_pair<D, pointer> _pair;
        
        private:
            template<typename U>
            void _move(unique_ptr<U>&& p_other)
            {
                _pair.get_first()(release());
                _pair.second = p_other.release();
            }
        
        public:
            constexpr reference operator*()
            {
                return *_pair.second;
            }

            constexpr const_reference operator*() const
            {
                return *_pair.second;
            }

            constexpr pointer operator->()
            {
                return _pair.second;
            }

            template<typename U>
            constexpr bool operator==(const unique_ptr<U>& p_other)
            {
                return *get() == *p_other.get();
            }

            constexpr operator bool()
            {
                return _pair.second != nullptr;
            }

        public:
            pointer get()
            {
                return _pair.second;
            }

            pointer get() const
            {
                return _pair.second;
            }

            pointer release()
            {
                pointer p = _pair.second;
                _pair.second = nullptr;
                return p;
            }

            deleter_type& get_deleter()
            {
                return _pair.get_first();
            }

            template<typename T2>
            void swap(unique_ptr<T2>&& p_other)
            {
                pointer p = release();
                _pair.second = p_other.release();
                p_other._pair.second = p;
            }

        public:
            unique_ptr(): _pair(nullptr){}
            unique_ptr(nullptr_t): _pair(nullptr) {}

            unique_ptr(const unique_ptr<T>& p_other) = delete;
            unique_ptr(pointer p_value): _pair(p_value) {}
            unique_ptr(unique_ptr<T>&& p_other)
            {
                _move(p_other);
            }

            unique_ptr<T>& operator=(const unique_ptr<T>& p_other) = delete;
            unique_ptr<T>& operator=(unique_ptr<T>&& p_other)
            {
                _move(p_other);
                return *this;
            }

            ~unique_ptr()
            {
                _pair.get_first()(_pair.second);
                _pair.second = nullptr;
            }

        

    };

    // template<typename T, typename D>
    // unique_ptr<T> make_unique(const unique_ptr<T>& p_ptr)
    // {
    //     return unique_ptr(p_ptr);
    // }

    template<typename T, typename... Args>
    unique_ptr<T> make_unique(Args&&... p_args)
    {
        return unique_ptr<T>(new T(std::forward<Args>(p_args)...));
    }
}

#endif