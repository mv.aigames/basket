#ifndef BASKET__ARENA_HPP
#define BASKET__ARENA_HPP

#include <core/typedefs.hpp>
// #include <core/memory.hpp>
#include "allocator.hpp"
#include <core/macros.hpp>
#include <core/utility.hpp>


namespace basket
{
    /**
     * @brief Manage a block of memory.
     * 
     * There is no intention to support copy operations.
     * 
     * @tparam T 
     * @tparam AllocatorType 
     */
    template<typename T, typename AllocatorType = Allocator<T>>
    class Arena
    {
        public:
            using type = T;
            using pointer = type*;
        public:
            Arena(): _size(0), _block(nullptr) {}

            Arena(size_t p_size): _size(p_size)
            {
                _reserve_memory();
            }

            Arena(Arena<T>&& p_other)
            {
                _move(p_other);
            }

            Arena& operator=(Arena<T>&& p_other)
            {
                _move(p_other);
                return *this;
            }

        public:
            pointer get(size_t p_index)
            {
                if (p_index >= _size)
                {
                    return nullptr;
                }

                return _get(p_index);
            }

            
            void construct(size_t p_index, const T& p_value)
            {
                _allocator.construct(_get(p_index), p_value);
            }

            template<typename... Args>
            void construct(size_t p_index, Args&&... p_args)
            {
                _allocator.construct(_get(p_index), forward<Args>(p_args)...);
            }

            size_t size() const
            {
                return _size;
            }

            bool empty() const
            {
                return _block == nullptr;
            }

        private:
            void _reserve_memory()
            {
                _block = _allocator.allocate(_size);
                BK_ASSERT(_block != nullptr, "Failed to reserve memory");
            }

            pointer _get(size_t p_index)
            {
                return _block + p_index;
            }

            void _move(Arena<T>&& p_other)
            {
                _block = p_other._block;
                p_other._block = nullptr;
                _size = p_other._size;
                p_other._size = 0;
            }



        public:
            pointer begin()
            {
                return _block;
            }

            const pointer begin() const
            {
                return _block;
            }

            pointer end()
            {
                return _block + _size;
            }

            const pointer end() const
            {
                return _block + _size;
            }

        private:
            AllocatorType _allocator;
            size_t _size;
            pointer _block;
    };
}

#endif