#ifndef BASKET__REF_COUNTER_HPP
#define BASKET__REF_COUNTER_HPP

namespace basket
{
    template<typename T>
    class _Ref_Counter
    {
        int64_t _counter = 0;
        public:
            _Ref_Counter<T>& operator++()
            {
                _counter++;
                return *this;
            }

            _Ref_Counter<T>& operator--()
            {
                _counter--;
                return *this;
            }


            int64_t count() const
            {
                return _counter;
            }

        public:
            _Ref_Counter(T* p_pointer)
            {
                _ref = p_pointer;
                _counter = 1;
            }

        public:
            T* _ref;
    };
}

#endif