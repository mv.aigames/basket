#ifndef BASKET_SMART_POINTER_HPP
#define BASKET_SMART_POINTER_HPP

#include <core/pch.hpp>

namespace basket
{
    // template<typename T>
    // using unique_pointer = std::unique_ptr<T>;

    template<typename T>
    using shared_pointer = std::shared_ptr<T>;

    template<typename T>
    using weak_pointer = std::weak_ptr<T>;
}

#endif