#ifndef BASKET_ALLOCATOR_TRAITS_HPP
#define BASKET_ALLOCATOR_TRAITS_HPP

#include <core/utility.hpp>

namespace basket
{

    namespace _internal
    {

        #define BK_ALLOCATOR_ALIAS(ALIAS_NAME, DEFAULT_ALIAS)                   \
        template<typename T, typename = void>                                   \
        struct get_allocator_##ALIAS_NAME                                       \
        {                                                                       \
            using ALIAS_NAME = typename T::##DEFAULT_ALIAS;                     \
        };                                                                      \
                                                                                \
        template<typename T>                                                    \
        struct get_allocator_##ALIAS_NAME<T, void_t<typename T::##ALIAS_NAME>>  \
        {                                                                       \
            using ALIAS_NAME = typename T::##ALIAS_NAME;                        \
        }

        BK_ALLOCATOR_ALIAS(pointer, value_type*);
        BK_ALLOCATOR_ALIAS(reference, value_type&);
        // BK_ALLOCATOR_ALIAS(const_pointer, const value_type*);
    }

    template<typename AllocT>
    struct allocator_traits
    {
        using value_type = AllocT::value_type;
        using pointer = _internal::get_allocator_pointer<AllocT>::pointer;

        template<typename U>
        using rebind_alloc = AllocT::template rebind<U>::other;
    };
}

#endif