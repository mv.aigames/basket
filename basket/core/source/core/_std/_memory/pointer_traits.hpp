#ifndef BASKET_POINTER_TRAITS_HPP
#define BASKET_POINTER_TRAITS_HPP

namespace basket
{
    template<typename T>
    struct pointer_traits;

    template<typename T>
    struct pointer_traits<T*>
    {
        using pointer = T*;
        using element_type = T;
        using difference_type = ptrdiff_t;
    };
}


#endif