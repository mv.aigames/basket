#ifndef BASKET__STD_SET_HPP
#define BASKET__STD_SET_HPP

#include <core/pch.hpp>
#include "tree.hpp"
#include <core/functor.hpp>
#include "_memory.hpp"
#include "_iterator.hpp"

namespace basket
{
    template<
        typename K,
        typename KC,
        typename AllocatorType>
    struct TreeSetTraits
    {
        using key_type = K;
        using value_type = K;
        using key_compare = KC;
        using allocator_type = AllocatorType;
        using node_type = key_type;
        // using iterator_type = TreeIterator<key_type>;

        static const key_type& key_value(const value_type& p_value)
        {
            return p_value;
        }
    };

    template<
        typename KeyT,
        typename CompareT = less<KeyT>,
        typename AllocatorType = Allocator<KeyT>>
    class set: public BSTree<TreeSetTraits<KeyT, CompareT, AllocatorType>>
    {
        public:
            using base = BTree<TreeSetTraits<KeyT, CompareT, AllocatorType>>;
            using key_type = KeyT;
            using iterator_type = base::typename iterator_type;

        public:
            set() = default;
            set(const set<KeyT, CompareT, AllocatorType>& p_other) = default;
            set(set<KeyT, CompareT, AllocatorType>&& p_other) = default;
        
        public:
            iterator_type insert(const key_type& p_value)
            {
                return _insert(p_value);
            }

    };
}

#endif