#ifndef BASKET__STD_FILESYSTEM_HPP
#define BASKET__STD_FILESYSTEM_HPP

// #include <core/pch.hpp>
#include "_string.hpp"
#include <core/logging.hpp>
#include <core/results.hpp>
#include <core/utility.hpp>
// #include "../io/types.hpp"
// #include <core/io/file.hpp>


// namespace basket
// {
//     class File;
// }

namespace basket
{ 
    // class File;

    namespace filesystem
    {

        class BASKET_API path
        {
            public:
                using string_type = string;
            public:
                path(const string& p_path_string);
                const string& get_path() const;
                const string& get_path_native() const;
                bool is_valid() const;
                path remove_extension() const
                {
                    string::size_type pos = _full_path.rfind('/');
                    if (pos == string::npos)
                    {
                        return *this;
                    }

                    return path(_full_path.substr(0, pos));
                }

                string get_extension() const
                {
                    string::size_type pos = _full_path.rfind('.');
                    if (pos == string::npos)
                    {
                        return _full_path.substr(pos, _full_path.size());
                    }

                    return "";
                }
            
            private:
                void _check_path(const string& p_path);
                void _fix_path(const string& p_path);
            private:
                // string _path;
                string _full_path;
                string _full_path_native;
                bool _is_valid;
            public:
                path(): _is_valid(false) {}
                path(const path& p_other): 
                    _full_path(p_other._full_path),
                    _full_path_native(p_other._full_path_native),
                    _is_valid(p_other._is_valid)
                    {}

                path(path&& p_other): 
                    _full_path(::basket::move(p_other._full_path)),
                    _full_path_native(::basket::move(p_other._full_path_native)),
                    _is_valid(p_other._is_valid)
                    {}
        };

        string BASKET_API get_process_path();
        Result BASKET_API create_file(const path& p_path);
        // File::handle_type BASKET_API open_file(const path& p_filename);
        // _FileHandle BASKET_API open_file(const path& p_path, OpenMode p_flags);

        Result BASKET_API init();
        Result BASKET_API move(const path& p_from, const path& p_to);
        Result BASKET_API copy_file(const path& p_from, const path& p_to, bool p_fail_if_exists = true);
    }
}

#endif