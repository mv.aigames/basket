#ifndef BASKET__STD_STRING_VIEW_HPP
#define BASKET__STD_STRING_VIEW_HPP

#include <core/typedefs.hpp>

#include "_string/_char_traits.hpp"
#include "_memory/allocator.hpp"

namespace basket
{
    template<typename CharType, typename Traits = char_traits<CharType>, typename Alloc = Allocator<CharType>()>
    class basic_string_view
    {
        public:
            using value_type = CharType;
            using pointer = value_type*;
            using const_pointer = const pointer;
            using reference = value_type&;
            using const_reference = const reference;
            using size_type = size_t;
            
        public:
            constexpr size_type size() const
            {
                return (size_type)(_end - _begin);
            }

            constexpr const_reference front()
            {
                return *_begin;
            }

            constexpr const_reference back()
            {
                return *_end;
            }

            constexpr pointer data()
            {
                return _begin;
            }

            constexpr const_pointer data() const
            {
                return _begin;
            }

            constexpr bool empty() const
            {
                return _begin != nullptr;
            }


        public:
            constexpr basic_string_view(): _begin(nullptr), _end(nullptr)
            {

            }

            constexpr basic_string_view(nullptr_t) = delete;

            constexpr basic_string_view(const basic_string_view<CharType, Traits, Alloc>& p_other) = default;

            template<typename L, typename M>
            constexpr basic_string_view(L p_begin, M p_end): _begin(p_begin), _end(p_end) {}

            constexpr basic_string_view(pointer p_begin, size_type p_count) : basic_string_view(p_begin, p_begin + p_count)
            {
                
            }

            constexpr basic_string_view(pointer p_begin)
            {
                _begin = p_begin;
                _end = strlen(p_begin);
            }



            
        
        private:
            pointer _begin;
            pointer _end;
    };

    using string_view = basic_string_view<char>;
}

#endif