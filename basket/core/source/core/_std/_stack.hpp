#ifndef BASKET__STD_STACK_HPP
#define BASKET__STD_STACK_HPP

#include "_dequeue.hpp"

namespace basket
{
    template<typename T, typename QueueClass = Dequeue<T>>
    class stack
    {
        public:
            using value_type = T;
            using pointer = value_type*;
            using const_pointer = const value_type*;
            using reference = value_type&;
            using const_reference = const value_type&;


        public:
            void push(const value_type& p_value)
            {
                _queue.push_front(p_value);
            }

            void push(value_type&& p_value)
            {
                _queue.push_front(p_value);
            }

            value_type& front()
            {
                return _queue.front();
            }

            const value_type& front() const
            {
                return _queue.front();
            }

            void pop()
            {
                pop_front();
            }

            size_t size() const
            {
                return _queue.size();
            }

        private:
            QueueClass _queue;
    }
}

#endif