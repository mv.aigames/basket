#ifndef BASKET__STREAMPOS_HPP
#define BASKET__STREAMPOS_HPP



namespace basket
{
    using streampos = fpos<char_traits<char>::state_type>;
}

#endif