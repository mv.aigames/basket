#ifndef BASKET__BASIC_FILEBUF_HPP
#define BASKET__BASIC_FILEBUF_HPP


#include "../_string/_char_traits.hpp"
#include "_basic_streambuf.hpp"


#ifdef BASKET_WINDOWS
    #include "../../platform/basic_filebuf_windows.hpp"
#else
    #error "Only windows is supported"
#endif

#endif