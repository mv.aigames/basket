#ifndef BASKET__BASIC_STREAMBUF_HPP
#define BASKET__BASIC_STREAMBUF_HPP


#include "../_string/_char_traits.hpp"

namespace basket
{
    template<typename CharType, typename Traits = char_traits<CharType>>
    class basic_streambuf
    {
        private:
            using _base = basic_streambuf<CharType, Traits>;
        public:
            
            using traits_type = Traits;
            using char_type = traits_type::char_type;
            using int_type = traits_type::int_type;
            using off_type = traits_type::off_type;
            using pos_type = traits_type::pos_type;

        private:
            /*
            * I dont think I am gonna need a vector to use it.
            */
            CharType* _eback;
            CharType* _gptr;
            CharType* _egptr;
            
            CharType* _pbase;
            CharType* _pptr;
            CharType* _epptr;

        // Positioning
        protected:

            virtual _base* setbuf(char_type* p_s, streamsize p_n) = 0;
            virtual pos_type seekoff(
                                off_type p_off,
                                ios_base::seekdir p_dir,
                                ios_base::openmode p_which = ios_base::in | ios_base::out) = 0;
            virtual pos_type seekpos(
                                off_type p_off,
                                ios_base::openmode p_which = ios_base::in | ios_base::out) = 0;
            virtual streamsize xsgetn(char_type* p_s, streamsize p_count) = 0;

            virtual streamsize xsputn(const char* p_stream, streamsize p_count) = 0;

        
        public:
            streamsize sputn(const char* p_stream, streamsize p_count)
            {
                return xsputn(p_stream, p_count);
            }
        
        public:
            _base* pubsetbuf(char_type* p_s, streamsize p_n)
            {
                return setbuf(p_s, p_n);
            }

            pos_type pubseekoff(
                                off_type p_off,
                                ios_base::seekdir p_dir,
                                ios_base::openmode p_which = ios_base::in | ios_base::out)
            {
                return seekoff(p_off, p_dir, p_which);
            }

            pos_type pubseekpos(off_type p_off, ios_base::openmode p_which = ios_base::in | ios_base::out)
            {
                return seekpos(p_off, p_which);
            }

            streamsize sgetn(char_type* p_s, streamsize p_count)
            {
                return xsgetn(p_s, p_count);
            }

        public:
            char_type* eback()
            {
                return _eback;
            }

            char_type* gptr()
            {
                return _gptr;
            }

            char_type* egptr()
            {
                return _egptr;
            }

        protected:
            void setg(char_type* p_b, char_type* p_c, char_type* p_e)
            {
                _eback = p_b;
                _gptr = p_c;
                _egptr = p_e;
            }

        public:
            char_type* pbase()
            {
                return _pbase;
            }

            char_type* pptr()
            {
                return _pptr;
            }

            char_type* epptr()
            {
                return _epptr;
            }
        
        protected:
            void setp(char_type* p_b, char_type* p_c, char_type* p_e)
            {
                _eback = p_b;
                _gptr = p_c;
                _egptr = p_e;
            }

        protected:
            void _init()
            {
                setg(nullptr, nullptr, nullptr);
                setp(nullptr, nullptr, nullptr);
            }

        protected:
            basic_streambuf()
            {
                _init();
            }

            basic_streambuf(const basic_streambuf<CharType, Traits>&) {}
    };
}

#endif