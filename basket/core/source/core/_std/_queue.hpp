#ifndef BASKET__STD_QUEUE_HPP
#define BASKET__STD_QUEUE_HPP

#include "dequeue.hpp"

namespace basket
{
    template<typename T, typename QueueClass = Dequeue<T>>
    class Queue
    {
        public:
            Queue() = default;
        
        public:
            void push(const T& p_value)
            {
                _queue_impl.push_back(p_value);
            }

            void push(T&& p_value)
            {
                _queue_impl.push_back(p_value);
            }

            void pop()
            {
                _queue_impl.pop_front();
            }

            T& front()
            {
                return _queue_impl.front();
            }

            const T& front() const
            {
                return _queue_impl.front();
            }

            T& back()
            {
                return _queue_impl.back();
            }

            const T& back() const
            {
                return _queue_impl.back();
            }

            bool empty() const
            {
                return _queue_impl.empty();
            }

            
        private:
            QueueClass _queue_impl;
    };
}

#endif