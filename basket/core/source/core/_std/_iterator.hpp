#ifndef BASKET__STD_ITERATOR_HPP
#define BASKET__STD_ITERATOR_HPP


namespace basket
{

    template<typename T>
    struct BaseSimpleIterator
    {
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using pointer = T*;
        using reference = T&;
    };

    template<typename IT>
    class ReverseIterator
    {
        private:
            using base = ReverseIterator<IT>;

            using difference_type = std::ptrdiff_t;
            using value_type = IT::value_type;
            using pointer = IT::pointer;
            using reference = IT::reference;

        public:
            ReverseIterator(IT p_it): _it(p_it) {}

        public:
            base operator++() // pre
            {
                _it--;
                return *this;
            }

            base operator++() const // pre
            {
                IT& it = const_cast<IT&>(_it);
                --it;
                return *this;
            }

            base operator++(int) // post
            {
                base temp = *this;
                _it--;
                return temp;
            }

            base operator++(int) const // post
            {
                IT& it = const_cast<IT&>(_it);
                --it;
                return *this;
            }

        private:
            IT _it;
    };

    template<typename T>
    bool operator==(const ReverseIterator<T>& p_left, const ReverseIterator<T>& p_right)
    {
        return p_left.ptr == p_right.ptr;
    }

    template<typename T>
    bool operator!=(const ReverseIterator<T>& p_left, const ReverseIterator<T>& p_right)
    {
        return !(p_left == p_right);
    }

    template<typename T>
    class TreeIterator
    {
        public:
            using iterator_category = std::forward_iterator_tag;
            using difference_type = std::ptrdiff_t;
            using value_type = T;
            using pointer = T*;
            using reference = T&;
    };

    template<typename T>
    class ContinuousIterator
    {
        public:
            using iterator_category = std::forward_iterator_tag;
            using difference_type = std::ptrdiff_t;
            using value_type = T;
            using pointer = T*;
            using reference = T&;
        
        private:
            pointer ptr;

        public:
            ContinuousIterator(pointer p_ptr);

            reference operator*() const;
            pointer operator->();

            ContinuousIterator<T>& operator++();
            ContinuousIterator<T>& operator++(int);

            template<typename U>
            friend bool operator==(const ContinuousIterator<U>& p_left, const ContinuousIterator<U>& p_right);
            template<typename U>
            friend bool operator!=(const ContinuousIterator<U>& p_left, const ContinuousIterator<U>& p_right);
    };

    template<typename T>
    ContinuousIterator<T>::ContinuousIterator(ContinuousIterator<T>::pointer p_ptr)
    {
        ptr = p_ptr;
    }

    template<typename T>
    ContinuousIterator<T>::reference ContinuousIterator<T>::operator*() const
    {
        return *ptr;
    }

    template<typename T>
    ContinuousIterator<T>::pointer ContinuousIterator<T>::operator->()
    {
        return ptr;
    }

    template<typename T>
    ContinuousIterator<T>& ContinuousIterator<T>::operator++()
    {
        ptr++;
        return *this;
    }

    template<typename T>
    ContinuousIterator<T>& ContinuousIterator<T>::operator++(int)
    {
        ContinuousIterator<T> tmp = *this;
        ptr++;
        return tmp;
    }


    template<typename T>
    bool operator==(const ContinuousIterator<T>& p_left, const ContinuousIterator<T>& p_right)
    {
        return p_left.ptr == p_right.ptr;
    }

    template<typename T>
    bool operator!=(const ContinuousIterator<T>& p_left, const ContinuousIterator<T>& p_right)
    {
        return !(p_left == p_right);
    }

    
}

#endif