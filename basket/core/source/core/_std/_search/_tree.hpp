#ifndef BASKET_TREE_HPP
#define BASKET_TREE_HPP

// #include <core/pch.hpp>
#include "../_pair.hpp"
#include "../_utility.hpp"
#include "../_compressed_pair.hpp"

namespace basket
{
    template<typename TreeNoveValueT>
    struct _TreeNode
    {
        using base = _TreeNode<TreeNoveValueT>;
        using value_type = TreeNoveValueT;
        using node_pointer = base*;

        node_pointer _parent;
        node_pointer _left;
        node_pointer _right;


        /**
         * Is leaf if _right and _left are null
         * 
         */

        value_type value;
        
        uint8_t is_leaf:1 = 1;


        template<typename... Args>
        _TreeNode<value_type>(Args&&... p_args): value(forward<Args>(p_args)...) {}

        _TreeNode<value_type>(const value_type& p_value): value(p_value) {}
        _TreeNode<value_type>(value_type&& p_value): value(p_value) {}

    };

    
    template<typename V>
    struct _TreeIterator
    {
        using value_type = V;
        using pointer = value_type*;
        using const_pointer = const pointer;
        using base = _TreeIterator<value_type>;
        using node_type = _TreeNode<value_type>;

        template<typename T>
        friend class _stree;

        _TreeIterator(node_type* p_node): _ptr(p_node) {}

        bool operator== (const _TreeIterator& p_value)
        {
            return p_value._ptr == _ptr;
        }

        bool operator!= (const _TreeIterator& p_value)
        {
            return !(p_value == *this);
        }

        pointer operator->()
        {
            return &_ptr->value;
        }

        const_pointer operator->() const
        {
            return &_ptr->value;
        }

        const V& operator*() const
        {
            return _ptr->value;
        }

        V& operator*()
        {
            return _ptr->value;
        }

        private:
            node_type* _ptr;
    };

    template<typename TreeTraitsT>
    class _stree
    {

        public:
            using _traits = TreeTraitsT;
            using key_type = _traits::key_type;
            using key_compare = _traits::key_compare;
            using value_type = _traits::value_type;
            using node_type = _TreeNode<value_type>;
            using allocator_type = _traits::allocator_type;
            using tree_node_allocator_t = allocator_type::template rebind<_TreeNode<value_type>>::other;
            using iterator_type = _TreeIterator<value_type>;
            using iterator = iterator_type;
            using const_iterator = const iterator;
            using size_type = size_t;
        
        private:
            struct _node_head
            {
                size_t _size = 0;
                node_type* _node = nullptr;
            };

        private:
            compressed_pair<key_compare, _node_head> _head;
            tree_node_allocator_t _node_allocator;
            

            // char _fake_node_char;
            // // For _fake_node iterator end.
            // node_type* _fake_node = reinterpret_cast<node_type*>(&_fake_node_char);


        public:
            _stree(): _head(compressed_pair<key_compare, _node_head>::OneArg(), key_compare()) {}
            _stree(const _stree<TreeTraitsT>& p_other) = default;
            _stree(_stree<TreeTraitsT>&& p_other) = default;

        public:

            
            
            bool empty() const
            {
                return _head.second._size == 0;
            }

            size_type size() const
            {
                return _head.second._size;
            }

        public:

            iterator_type begin()
            {
                return iterator_type(_get_lower());
            }

            const iterator_type cbegin() const
            {
                return iterator_type(_get_lower());
            }

            iterator_type end()
            {
                return iterator_type(nullptr);
            }

            const iterator_type cend() const
            {
                return iterator_type(nullptr);
            }
        private:
            node_type* _insert_node(node_type* p_parent, node_type** p_leaf, node_type* p_node)
            { 
                *p_leaf = p_node;
                p_node->_parent = p_parent;
                _increment();
                return p_node;
            }

        protected:
            

            key_compare& _get_compare()
            {
                return _head.get_first();
            }

            const key_compare& _get_compare() const
            {
                return _head.get_first();
            }

            size_type _erase(const key_type& p_key)
            {
                auto it = _find(p_key);
                if (it == end())
                {
                    return 0;
                }

                _erase_node(it._ptr);
                return 0;
            }

            void _delete(node_type* p_node)
            {
                _decrement();
                delete p_node;
            }

            void _erase_node(node_type* p_node)
            {
                
                _isolate_node(p_node);
                _delete(p_node);
            }

            void _find_place_for_node(node_type* p_node)
            {
                node_type* n = _head.second._node;

                const key_compare& kcf = _get_compare();

                while(true)
                {
                    const key_type& nkey = _traits::_key(n->value);
                    const key_type& pkey = _traits::_key(p_node->value);
                    if (kcf(nkey, pkey))
                    {
                        if (!n->_left)
                        {
                            p_node->_parent = n;
                            n->_left = p_node;
                            return;
                        }
                        n = n->_left;
                    }
                    else
                    {
                        if (!n->_right)
                        {
                            p_node->_parent = n;
                            n->_right = p_node;
                            return;
                        }
                        n = n->_right;
                    }
                }
            }

            //TODO: optimize. Lambda is a good candidate
            void _isolate_node(node_type* p_node)
            {
                if (size() == 1)
                {
                    return;
                }
                // If parent. Choose the new root.
                if (!p_node->_parent)
                {
                    node_type* child = p_node->_left;
                    node_type* other_child = p_node->_right;
                    if (!child)
                    {
                        child = p_node->_right;
                        other_child = nullptr;
                    }

                    child->_parent = nullptr;
                    _head.second._node = child;
                    // child->_parent = p_node->_parent;
                    // if (p_node->_parent->_left == p_node)
                    // {
                    //     p_node->_parent->_left = child;
                    // }
                    // else
                    // {
                    //     p_node->_parent->_right = child;
                    // }
                    if (other_child)
                    {
                        _find_place_for_node(other_child);
                    }
                }
                else if (!p_node->_left && !p_node->_right)
                {
                    if (p_node->_parent->_left == p_node)
                    {
                        p_node->_parent->_left = nullptr;
                    }
                    else
                    {
                        p_node->_parent->_right = nullptr;
                    } 
                    p_node->_parent = nullptr;
                }
                else
                {
                    node_type* child = p_node->_left;
                    node_type* other_child = p_node->_right;
                    if (!child)
                    {
                        child = p_node->_right;
                        other_child = nullptr;
                    }

                    child->_parent = p_node->_parent;
                    if (p_node->_parent->_left == p_node)
                    {
                        p_node->_parent->_left = child;
                    }
                    else
                    {
                        p_node->_parent->_right = child;
                    }
                    if (other_child)
                    {
                        _find_place_for_node(other_child);
                    }
                }
            }

            iterator_type _insert(const value_type& p_value)
            {
                if (empty())
                {
                    node_type* n =_generate_node(p_value);
                    _insert_first_node(n);
                    return iterator_type(_head.second._node);
                }
                else
                {
                    node_type* n = _head.second._node;
                    const key_compare& kf = _get_compare();
                    while(true)
                    {
                        const key_type& pkey = TreeTraitsT::_key(p_value);
                        const key_type& nkey = TreeTraitsT::_key(n->value);

                        if (pkey == nkey)
                        {
                            return iterator_type(nullptr);
                        }

                        if (kf(nkey, pkey))
                        {
                            if (!n->_left)
                            {
                                return iterator_type(_insert_node(n , &n->_left, _generate_node(p_value)));
                            }
                            n = n->_left;
                             
                        }
                        else
                        {
                            if (!n->_right)
                            {
                                // node_type* v = _generate_node(p_value);
                                // v->_parent = n;
                                // n->_right = v;
                                // return iterator_type(v);
                                return iterator_type(_insert_node(n, &n->_right, _generate_node(p_value)));
                            }
                            n = n->_right;
                        }
                    }
                }
            }

            iterator _find(const key_type& p_key)
            {
                if (empty())
                {
                    return iterator(nullptr);
                }

                node_type* node = _head.second._node;

                const key_compare kcf = _get_compare();

                while(node)
                {
                    const key_type& pkey = p_key;
                    const key_type& nkey = TreeTraitsT::_key(node->value);
                    if (pkey == nkey)
                    {
                        return iterator(node);
                    }

                    if (kcf(nkey, pkey))
                    {
                        node = node->_left;
                    }
                    else
                    {
                        node = node->_right;
                    }
                }

                return iterator(nullptr);
            }

            const_iterator _find(const key_type& p_key) const
            {
                if (empty())
                {
                    return const_iterator(nullptr);
                }

                node_type* node = _head.second._node;

                const key_compare kcf = _get_compare();

                while(node)
                {
                    const key_type& pkey = p_key;
                    const key_type& nkey = TreeTraitsT::_key(node->value);
                    if (pkey == nkey)
                    {
                        return const_iterator(node);
                    }

                    if (kcf(nkey, pkey))
                    {
                        node = node->_left;
                    }
                    else
                    {
                        node = node->_right;
                    }
                }

                return const_iterator(nullptr);
            }

            void _increment()
            {
                _head.second._size++;
            }

            void _decrement()
            {
                _head.second._size--;
            }

            void _insert_first_node(node_type* p_node)
            {
                _head.second._node = p_node;
                // _head.second._size++;
                _increment();
            }

            node_type* _generate_node(const value_type& p_value)
            {
                node_type* new_node = _node_allocator.allocate(1);
                _node_allocator.construct(new_node, p_value);
                new_node->_parent = nullptr;
                new_node->_left = nullptr;
                new_node->_right = nullptr;
                return new_node;
            }

            node_type* _generate_node(value_type&& p_value)
            {
                node_type* new_node = _node_allocator.allocate(1);
                _node_allocator.construct(new_node, p_value);
                new_node->_parent = nullptr;
                new_node->_left = nullptr;
                new_node->_right = nullptr;
                return new_node;
            }

            node_type* _get_lower() const
            {
                if (empty())
                {
                    return nullptr;
                }

                node_type* node = _head.second._node;
                while(node->_left)
                {
                    node = node->_left;
                }

                return node;
            }

            node_type* _get_higher() const
            {
                if (empty())
                {
                    return nullptr;
                }

                node_type* node = _node_head._head;
                while(node->_right)
                {
                    node = node->_right;
                }

                return node;
            }
    
            // I don't know the performance impact of recursive functions.
            void _delete_node(node_type* p_node)
            {
                if (p_node->_left)
                {
                    _delete_node(p_node->_left);
                }

                if (p_node->_right)
                {
                    _delete_node(p_node->_right);
                }

                _delete(p_node);
            }

            void _clear()
            {
                _delete_node(_head.second._node);
            }
    };
}

#endif