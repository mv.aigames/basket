#ifndef BASKET__STD_TUPLE_HPP
#define BASKET__STD_TUPLE_HPP

#include "_tuple_traits.hpp"

namespace basket
{
    template<typename... Types>
    class tuple;

    template<>
    class tuple<>
    {
        public:
            // tuple() = default;
            // tuple(const tuple& p_other) = default;
    };

    template<typename FT, typename... Types>
    class tuple<FT, Types...>: private tuple<Types...>
    {
        using type = FT;
        using _base = tuple<FT, Types...>;

        public:
            tuple(const FT& p_value, Types&&... p_args): tuple<Types...>(std::forward<Types>(p_args)...), value(p_value) {}
        public:

            template<size_t I, typename... Types>
            friend struct _get_tuple_value;

        private:
            FT value;
    };

    

    template<size_t I, typename... Types>
    struct _get_tuple_value;

    
    template<size_t I, typename Head, typename... Types>
    struct _get_tuple_value<I, tuple<Head, Types...>>
    {
        static constexpr tuple_element<I, tuple<Head, Types...>>::type& value(tuple<Head, Types...>& p_tuple)
        {
            return _get_tuple_value<I - 1, tuple<Types...>>::value(static_cast<tuple<Types...>&>(p_tuple));
        }
    };

    template<typename Head, typename... Types>
    struct _get_tuple_value<0, tuple<Head, Types...>>
    {
        static constexpr tuple_element<0, tuple<Head, Types...>>::type& value(tuple<Head, Types...>& p_tuple)
        {
            return p_tuple.value;
        }
    };

    template<size_t I, typename... Types>
    constexpr typename tuple_element<I, tuple<Types...>>::type& get(tuple<Types...>& p_tuple)
    {
        return _get_tuple_value<I, tuple<Types...>>::value(p_tuple);
    }

    template<typename... Types>
    struct tuple_size;

    template<typename... Types>
    struct tuple_size<tuple<Types...>>: integral_constant<size_t, sizeof...(Types)> {};

    template<typename T>
    constexpr size_t tuple_size_v = tuple_size<T>::value;
}

#endif