#ifndef BASKET__STD_LIMITS_HPP
#define BASKET__STD_LIMITS_HPP

#include <core/typedefs.hpp>

namespace basket
{
    template<typename T>
    struct numeric {};

    template<>
    struct numeric<char>
    {
        static constexpr size_t max() { return SCHAR_MAX; }
        static constexpr size_t min() { return SCHAR_MIN; }
    };
}


#endif