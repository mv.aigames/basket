#ifndef BASKET__STD_FUNCTIONAL_TRAITS_HPP
#define BASKET__STD_FUNCTIONAL_TRAITS_HPP


#include "../_tuple.hpp"

namespace basket
{
    namespace _internal
    {
        template<int N>
        struct ph
        {
            static_assert(N > 0 ,"Not valid placeholder");
        };
    }

    namespace placeholders
    {
        constexpr ::basket::_internal::ph<1> _1{};
        constexpr ::basket::_internal::ph<2> _2{};
        constexpr ::basket::_internal::ph<3> _3{};
        constexpr ::basket::_internal::ph<4> _4{};
        constexpr ::basket::_internal::ph<5> _5{};
        constexpr ::basket::_internal::ph<6> _6{};
        constexpr ::basket::_internal::ph<7> _7{};
        constexpr ::basket::_internal::ph<8> _8{};
        constexpr ::basket::_internal::ph<9> _9{};
    }

    template<class T>
    struct is_placeholder: integral_constant<int, 0>{ };

    template<int N>
    struct is_placeholder<_internal::ph<N>>: integral_constant<int, N> {};

    template<typename T>
    struct is_placeholder<const T>: is_placeholder<T> {};

    template<typename T>
    struct is_placeholder<volatile T>: is_placeholder<T> {};

    template<typename T>
    struct is_placeholder<const volatile T>: is_placeholder<T> {};
    
    template<class T>
    constexpr int is_placeholer_v = is_placeholder<T>::value;

    template<typename F>
    struct function_traits;

    template<typename R, typename... Args>
    struct function_traits<R(Args...)>
    {
        using return_type = R;
        static constexpr size_t arity = sizeof...(Args);
        using params = tuple<Args...>;
    };

    template<typename C, typename R, typename... Args>
    struct function_traits<R(C::*)(Args...)>
    {
        using return_type = R;
        static constexpr size_t arity = sizeof...(Args);
        using params = tuple<Args...>;
    };

    template<typename R, typename... Args>
    struct function_traits<R(*)(Args...)>
    {
        using return_type = R;
        static constexpr size_t arity = sizeof...(Args);
        using params = tuple<Args...>;
    };

    template<typename R, typename... Args>
    struct function_traits<R(&)(Args...)>
    {
        using return_type = R;
        static constexpr size_t arity = sizeof...(Args);
        using params = tuple<Args...>;
    };

    

    template<typename F>
    struct is_function_big
    {
        static constexpr bool value = alingof(F) > sizeof(std::max_align_t);
    };

    constexpr size_t function_objects = 8;
    
}

#endif