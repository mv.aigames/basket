#ifndef BASKET__STD_FUNCTIONAL_BIND_HPP
#define BASKET__STD_FUNCTIONAL_BIND_HPP

#include  "_functional_traits.hpp"
#include <core/_std/_tuple.hpp>

namespace basket
{

    

    template<size_t Index, typename BoundArg, typename... CallArgs>
    enable_if_t<(0 > is_placeholer_v<BoundArg>), BoundArg>& _swap_arg(BoundArg& p_bound, tuple<CallArgs...>& p_args)
    {
        return p_bound;
    }

    template<size_t Index, typename BoundArg, typename... CallArgs>
    enable_if_t<(0 < is_placeholer_v<BoundArg>), tuple_element_t<Index, tuple<CallArgs...>>>& _swap_arg(BoundArg& p_bound, tuple<CallArgs...>& p_args)
    {
        return get<Index>(p_args);
    }

    template<typename F, size_t... I, typename... BoundArgs, typename... CallArgs>
    typename function_traits<F>::return_type _bind_call(F& p_callable, index_sequence<I...> p_sequence, tuple<BoundArgs...>& p_bound, tuple<CallArgs...>& p_call_args)
    {

        BK_LOG(cat, info, "_BIND_CALL with args");
        return std::invoke(forward<F>(p_callable), _swap_arg<I>(get<I>(p_bound), p_call_args)...);
    };


    struct _guess_return_type {};

    template<typename R, typename F>
    struct BindingResult
    {
        using return_type = conditional<is_same_v<R, _guess_return_type>, typename function_traits<F>::return_type, R>::type;
    };

    template<typename R, typename F, typename... Args>
    struct BinderFunction
    {
        using result = BindingResult<R, F>;
        using sequence = index_sequence_for<Args...>;
        using FirstT = F;
        using _ParamsT = tuple<decay_t<Args>...>;

        template<typename T>
        constexpr BinderFunction(T&& p_callable, Args&&... p_args):
            _callable(forward<FirstT>(p_callable)),
            _params(forward<Args>(p_args)...)
            {}

        template<typename... CallArgs>
        constexpr typename result::return_type operator()(CallArgs&&... p_args)
        {
            return _do_call(p_args...);
        }

        template<typename... CallArgs>
        constexpr typename result::return_type operator()(CallArgs&&... p_args) const
        {
            return _do_call(p_args...);
        }

        private:
            template<typename... CallArgs>
            result::return_type _do_call(CallArgs&&... p_args)
            {
                auto args = tuple<CallArgs...>(forward<CallArgs>(p_args)...);
                // static_assert(is_same_v<decltype(_params), float>, "Not same");
                return _bind_call(forward<FirstT>(_callable), sequence(), _params, args);
            }

            FirstT _callable;
            _ParamsT _params;
    };

    template<typename R, typename F, typename... Args>
    constexpr BinderFunction<R, F, Args...> bind(F&& p_callable, Args&&... p_args)
    {
        return BinderFunction<R, F, Args...>(p_callable, p_args...);
    }


    template<typename F, typename... Args>
    constexpr BinderFunction<_guess_return_type, F, Args...> bind(F&& p_callable, Args&&... p_args)
    {
        return BinderFunction<_guess_return_type, F, Args...>(p_callable, p_args...);
    }
}

#endif