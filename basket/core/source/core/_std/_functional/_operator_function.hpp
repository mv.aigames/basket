#ifndef BASKET__OPERATOR_FUNCTION_HPP
#define BASKET__OPERATOR_FUNCTION_HPP

namespace basket
{
    template<typename T = void>
    struct less
    {
        constexpr bool operator()(const T& p_left, const T& p_right) const
        {
            return p_left < p_right;
        }
    };

    template<typename T = void>
    struct less_equal
    {
        constexpr bool operator()(const T& p_left, const T& p_right) const
        {
            return p_left <= p_right;
        }
    };

    // template<typename T = void>
    // struct equal
    // {
    //     constexpr bool operator()(const T& p_left, const T& p_right) const
    //     {
    //         return p_left == p_right;
    //     }
    // };

    template<typename T = void>
    struct great_equal
    {
        constexpr bool operator()(const T& p_left, const T& p_right) const
        {
            return p_left >= p_right;
        }
    };

    template<typename T = void>
    struct great
    {
        constexpr bool operator()(const T& p_left, const T& p_right) const
        {
            return p_left > p_right;
        }
    };
}

#endif