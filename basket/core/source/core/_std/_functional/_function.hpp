#ifndef BASKET__STD_FUNCTIONAL_FUNCTION_HPP
#define BASKET__STD_FUNCTIONAL_FUNCTION_HPP

#include <cstdlib>
#include "../_utility.hpp"

namespace basket
{
    template<typename R, typename... Args>
    class FunctionBase
    {
        public:
            virtual R call(Args&&... p_args) = 0;
    };

    template<typename R, typename... Args>
    class FunctionImpl: public FunctionBase<R, Args...>
    {
        public:
            template<typename T>
            FunctionImpl(T&& p_callable): callable(forward<T>(p_callable)) {}

            virtual R call(Args&&... p_args) override
            {
                if (callable == nullptr)
                {
                    BK_LOG(core, error, "trying to call a nullptr function");
                    return R();
                }

                return std::invoke(move(callable), forward<Args>(p_args)...);
            }

        public:
            decay<R(Args...)>::type callable;
    };

    template<typename R, typename... Args>
    class FunctionClass
    {
        public:
            using FPtr = FunctionBase<R, Args...>;
            using _base = FunctionBase<R, Args...>;
            using return_type = R;

        ~FunctionClass()
        {
            _clear();
        }

        protected:
            return_type do_call(Args&&... p_args)
            {
                return reinterpret_cast<FPtr*>(_get())->call(move(p_args)...);
            }

            void _clear()
            {
                if (_get() != nullptr)
                {
                    delete _get();
                }
            }

            template<typename F>
            void _reset(F&& p_callable)
            {
                _clear();
                using impl = FunctionImpl<R, Args...>;

                _set(::new impl(move(p_callable)));
            }

            void _set(FPtr* p_ptr)
            {
                storage.p[function_objects - 1] = p_ptr;
            }

            FPtr* _get() const
            {
                return storage.p[function_objects - 1];
            }
            

        public:
            return_type operator()(Args&&... args)
            {
                return do_call(move(args)...);
            }

        protected:
            
            void _copy(const auto& p_other)
            {
                this->_clear();
                using impl = FunctionImpl<R, Args...>;
                impl* p = new impl(reinterpret_cast<impl*>(p_other._get())->callable);
                this->_set(p);

            }

            
            void _move(auto&& p_other)
            {
                this->_clear();
                this->_set(p_other._get());
                p_other._set(nullptr);
                p_other._reset(nullptr);
            }
            

        private:

            union FunctionStorage
            {
                FPtr* p[function_objects];
            };

            FunctionStorage storage;
    };

    template<typename F>
    struct GetFunctionClass;

    template<typename R, typename... Args>
    struct GetFunctionClass<R(Args...)>
    {
        using type = FunctionClass<R, Args...>;
    };

    

    template<typename F>
    class function: public GetFunctionClass<F>::type
    {
        using _base = GetFunctionClass<F>::type;

        

        public:
            function()
            {
                this->_set(nullptr);
                this->_reset(nullptr);
            }

            function(nullptr_t): function()
            {
                // _reset(move(nullptr));
            }

            function(const function<F>& p_other): function()
            {
                this->_copy(p_other);
            }

            function(function<F>&& p_other): function()
            {
                this->_move(p_other);
            }

            template<typename C>
            function(C&& p_callable)
            {
                this->_reset(p_callable);
            }


            template<typename _Callable>
            function<F>& operator=(const _Callable& p_callable)
            {
                this->_reset(p_callable);
                return *this;
            }

            
            function<F>& operator=(const function<F>& p_other)
            {
                this->_copy(p_other);
                return *this;
            }

            template<typename C>
            enable_if_t<is_same_v<function<F>, C>, function<F>>& operator=(C&& p_callable)
            {
                this->_reset(p_callable);
                return *this;
            }


            function<F>& operator=(function<F>&& p_other)
            {
                this->_move(p_other);
                return *this;
            }
    };
}
#endif