#ifndef BASKET__STD_DSTREAM_HPP
#define BASKET__STD_DSTREAM_HPP

// #include "_string/_char_traits.hpp"
// #include "_io/_basic_iostream.hpp"
// #include "_stream/_basic_dbuf.hpp"

#include "_io/_basic_dstream.hpp"


// namespace basket
// {
//     template<typename CharType, typename Traits = char_traits<CharType>>
//     class basic_dstream: public basic_iostream<CharType, Traits>
//     {
//         private:
//             using _buffer_type = basic_dbuf<CharType, Traits>;
//             _buffer_type _dbuffer;

//         public:
//             void open(const char* p_filename, ios_base::openmode p_mode = ios_base::in | ios_base::out)
//             {
//                 _dbuffer.open();
//             }

//             _buffer_type* rdbuf() const
//             {
//                 return const_cast<_buffer_type*>(&_dbuffer);
//             }

//             void close()
//             {
//                 _dbuffer.close();
//             }
//     };

//     using dstream = basic_dstream<char>;
// }

#endif