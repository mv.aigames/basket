#ifndef BASKET__STD_IOS_HPP
#define BASKET__STD_IOS_HPP


#include "_io/_ios_base.hpp"
#include "_io/_basic_ios.hpp"
#include "_io/_basic_istream.hpp"
#include "_io/_basic_ostream.hpp"
#include "_io/_basic_iostream.hpp"
#include "_io/_basic_fstream.hpp"

#endif