#ifndef BASKET__TUPLE_TRAITS_HPP
#define BASKET__TUPLE_TRAITS_HPP

namespace basket
{
    template<typename... Types>
    class tuple;

    template<size_t I, typename... Types>
    struct tuple_element;

    template<size_t Index, typename Head,  typename... Tail>
    struct _get_type_index
    {
        using type = _get_type_index<Index - 1, Tail...>::type;
    };

    template<typename Head,  typename... Tail>
    struct _get_type_index<0, Head, Tail...>
    {
        using type = Head;
    };

    template<size_t Index, typename... Types>
    struct tuple_element<Index, tuple<Types...>>
    {
        using type = _get_type_index<Index, Types...>::type;
    };

    template<size_t I, typename... Types>
    using tuple_element_t = tuple_element<I, Types...>::type;

    template<size_t I, typename... Types>
    constexpr tuple_element<I, Types...>::type& get(tuple<Types...>& p_tuple);
}

#endif