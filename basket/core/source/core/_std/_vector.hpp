#ifndef BASKET__STD_VECTOR_HPP
#define BASKET__STD_VECTOR_HPP

#include <core/macros.hpp>
#include <core/memory.hpp>
#include "_iterator.hpp"
#include <core/logging.hpp>
#include <core/utils/algorithm.hpp>
#include <core/utility.hpp>
#include <core/initializer_list.hpp>

namespace basket
{
    
    template<typename T, typename AllocatorType = Allocator<T>>
    class vector
    {
        public:
            using type = T;
            using iterator = ContinuousIterator<type>;
            using const_iterator = const iterator;
            using pointer = type*;
            using reference = type&;
            using const_reference = const reference;

        private:
            T* _data;
            size_t _size;
            size_t _capacity;
            AllocatorType allocator;
            
        
        private:
            void reallocate(size_t p_new_capacity)
            {
                T* new_block = allocator.allocate(p_new_capacity);

                // size_t old_size = _size;
                size_t new_capacity = p_new_capacity;

                if (new_capacity < _size)
                {
                    // Destroy the elements out of the new capacity.
                    for (size_t index = new_capacity; index < _size; index++)
                    {
                        allocator.destroy(_data + index);
                    }
                    _size = new_capacity;
                }


                if (_data != nullptr)
                {
                    // Move to new block.
                    for (int index = 0; index < _size; index++)
                    {
                        allocator.construct(new_block + index, std::move(*(_data + index)));
                    }
                    allocator.deallocate(_data, _capacity);
                }
                // else
                // {
                // }
                for (int index = _size; index < new_capacity; index++)
                {
                    allocator.construct(new_block + index);
                }
                
                _capacity = p_new_capacity;
                _data = new_block;

            }

        public:
            void clear()
            {
                if (!_data)
                {
                    return;
                }

                for (size_t index = 0; index < _size; index++)
                {
                    allocator.destroy(_data + index);
                }
                allocator.deallocate(_data, _capacity);
                _data = nullptr;
                _size = 0;
                _capacity = 0;
            }

        public:
            void push_back(const T& p_value)
            {
                if (_size >= _capacity)
                {
                    reallocate(_capacity + 1);
                }

                _data[_size] = p_value;
                _size++;
            }

            void push_back(T&& p_value)
            {
                if (_size >= _capacity)
                {
                    reallocate(_capacity + 1);
                }

                // std::cout << "Size: " << _size << "\n";

                _data[_size] = p_value;
                _size++;
            }
            
            template<typename... Args>
            T& emplace_back(Args&&... p_args)
            {
                if (_size >= _capacity)
                {
                    reallocate(_capacity + 1);
                }

                allocator.construct(_data + _size, std::forward<Args>(p_args)...);
                _size++;
                return _data[_size - 1];
            }

            iterator begin()
            {
                return iterator(_data);
            }

            const_iterator begin() const
            {
                return iterator(_data);
            }

            iterator end()
            {
                return iterator(_data + _size);
            }
            
            const iterator end() const
            {
                return iterator(_data + _size);
            }

            size_t size() const
            {
                return _size;
            }

            size_t capacity() const
            {
                return _capacity;
            }

            pointer data()
            {
                return _data;
            }

            const pointer data() const
            {
                return _data;
            }

            void reserve(size_t p_capacity)
            {
                BK_SAFE_RETURN(_capacity >= p_capacity);

                T* new_block = allocator.allocate(p_capacity);
                for (size_t index = 0; index < _size; index++)
                {
                    allocator.construct(new_block + index, std::move(*(_data + index)));
                }

                allocator.deallocate(_data, _capacity);
                _data = new_block;
                _capacity = p_capacity;
            }

            void resize(size_t p_new_capacity)
            {
                #ifndef BK_NO_MERCY
                if (_capacity == p_new_capacity)
                {
                    return;
                }
                #endif

                if (p_new_capacity == 0)
                {
                    clear();
                }
                else
                {
                    reallocate(p_new_capacity);
                }

                for(size_t index = _size; index < _capacity; ++index)
                {
                    allocator.construct(_data + index);
                }

                _size = _capacity;
            }

            void pop_back()
            {
                BK_SAFE_RETURN(_size == 0);

                _size--;
                _data[_size].~T();
            }


            vector(): _size(0), _capacity(0), _data(nullptr) { }

            vector(const vector<T>& p_other): vector()
            {
                // BK_SAFE_RETURN(&p_other == this);

                _copy(p_other);
            }

            vector(vector<T>&& p_other): vector()
            {
                _move(std::move(p_other));
            }

            vector(const T* p_data, size_t p_size): vector()
            {
                resize(p_size);
                for (size_t index = 0; index < p_size; index++)
                {
                    allocator.construct(_data + index, *(p_data + index));
                }

                // _size = p_size;
            }

            vector(size_t p_size, const T& p_data): vector()
            {
                resize(p_size);
                for (size_t index = 0; index < p_size; index++)
                {
                    allocator.construct(_data + index, p_data);
                }

                // _size = p_size;
            }
            
            vector(const T* p_begin, const T* p_end)
            {
                _copy_range(p_begin, p_end);
            }



            vector(std::initializer_list<T> p_values, const AllocatorType& p_allocator = AllocatorType()): vector()
            {
                _copy_range(p_values.begin(), p_values.end());
            }

            ~vector()
            {
                clear();
            }

        public:
            reference operator[](int index)
            {
                return *(_data + index);
            }

            const_reference operator[](int index) const
            {
                return *(_data + index);
            }

            vector<T>& operator=(const vector<T>& p_other)
            {
                _copy(p_other);
                return *this;
            }
            vector<T>& operator=(vector<T>&& p_other)
            {
                _move(std::move(p_other));
                return *this;
            }
        private:
        
            // [from, to)
            void _copy_range(const T* p_begin, const T* p_end)
            {
                ptrdiff_t elements = (p_end - p_begin);

                T* new_block = allocator.allocate(elements);


                for (ptrdiff_t index = 0; index < elements; index++)
                {
                    allocator.construct(new_block + index, *(p_begin + index));
                }

                _size = elements;
                _capacity = elements;
                _data = new_block;
            }

            void _copy(const vector<T>& p_other)
            {
                clear();
                if (p_other.capacity() == 0)
                {
                    return;
                }
                T* new_block = allocator.allocate(p_other.capacity());

                for (size_t index = 0; index < p_other.size(); index++)
                {
                    allocator.construct(new_block + index, *(p_other._data + index));
                }

                _data = new_block;
                _size = p_other.size();
                _capacity = p_other.capacity();
            }

            void _move(vector<T>&& p_other)
            {
                clear();
                _size = p_other.size();
                _capacity = p_other.capacity();
                _data = p_other._data;

                p_other._data = nullptr;
                p_other._size = 0;
                p_other._capacity = 0;
            }

        // Operators
        public:
        template<typename U>
        friend constexpr bool operator==(const vector<U>& p_left, const vector<U>& p_right);
        template<typename U>
        friend constexpr bool operator!=(const vector<U>& p_left, const vector<U>& p_right);

    };

    /*
    * Conditions:
    *   - p_capacity < _capacity
    *   - p_capacity > _capacity
    */
    

    template<typename T>
    constexpr bool operator==(const vector<T>& p_left, const vector<T>& p_right)
    {
        if (p_left.size() != p_right.size())
        {
            return false;
        }

        return equal(p_left.begin(), p_left.end(), p_right.begin());
    }

    template<typename T>
    constexpr bool operator!=(const vector<T>& p_left, const vector<T>& p_right)
    {
        return !(p_left == p_right);
    }
}

#endif