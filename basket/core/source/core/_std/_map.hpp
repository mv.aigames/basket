#ifndef BASKET__STD_MAP_HPP
#define BASKET__STD_MAP_HPP

#include "_memory.hpp"
#include "_functional/_operator_function.hpp"
#include "_search/_tree.hpp"

namespace basket
{
    template<typename V>
    struct _TreeNode;

    // template<typename ValueType,  typename KC>
    // struct _TreeMapNodeValue
    // {
    //     using value_type = ValueType;
    //     value_type value;

    //     struct value_compare : KC
    //     {
    //         bool operator()(const value_type& p_left, const value_type& p_right)
    //         {
    //             return (*this)(p_left.first, p_right.first);
    //         }
    //     };
    // };

    template<typename K, typename V, typename KC, typename Alloc>
    struct _TreeMapTraits
    {
        using key_type = K;
        using value_type = pair<K, V>;
        using key_compare = KC;
        using iterator_type = TreeIterator;
        using allocator_type = Alloc;
        // using node_value_type = _TreeMapNodeValue;

        static constexpr const K& _key(const value_type& p_value)
        {
            return p_value.first;
        }
    };

    

    template<typename K, typename V, typename KC = less<K>, typename AllocatorT = Allocator<pair<K, V>>>
    class map: public _stree<_TreeMapTraits<K, V, KC, AllocatorT>>
    {
        public:
            using _base = _stree<_TreeMapTraits<K, V, KC, AllocatorT>>;
            using size_type = _base::size_type;
            using value_type = _base::value_type;
            using key_type = _base::key_type;
            using iterator = _base::iterator;

        public:

            size_type erase(const K& p_key)
            {
                return this->_erase(p_key);
            }

            void insert(const value_type& p_value)
            {
                this->_insert(p_value);
            }

            void clear()
            {
                this->_clear();
            }

        public:
            V& operator[](K& p_key)
            {
                auto it = this->_find(p_key);
                if (it == this->end())
                {
                    it = this->_insert({p_key, {}});
                }

                return it->second;
                
            }

            V& operator[](const K& p_key)
            {
                iterator it = this->_find(p_key);
                if (it == this->end())
                {
                    it = this->_insert({p_key, {}});
                }

                return it->second;
            }

        public:
            ~map()
            {
                this->_clear();
            }
    };
}

#endif