#ifndef BASKET__STD_LIST_HPP
#define BASKET__STD_LIST_HPP

#include "_memory.hpp"
#include <core/macros.hpp>

namespace basket
{
    namespace _internal
    {
        template<typename T>
        struct ListNode
        {
            using base = ListNode<T>;

            base* previous;
            base* next;
            T value;

            template<typename... Args>
            ListNode<T>(Args&&... p_args): value(std::forward<Args>(p_args)...) {}

            ListNode<T>() = default;
            ListNode<T>(const ListNode<T>& p_other): value(p_other.value), previous(p_other.value), next(p_other.next) {}
            ListNode<T>(ListNode<T>&& p_other): value(std::move(p_other.value)), previous(p_other.value), next(p_other.next)
            {
                p_other.next = nullptr;
                p_other.previous = nullptr;
            }
        };
    }

    template<typename T>
    struct ListIterator;

    template<typename T, typename AllocatorT = Allocator<T>>
    class BList
    {
        public:
            using node_type = _internal::ListNode<T>;
            using value_type = T;
            using pointer = value_type*;
            using const_pointer = const value_type*;
            using reference = value_type&;
            using const_reference = const value_type&;
            using iterator = ListIterator<value_type>;
            using const_iterator = const iterator;

        public:
            BList(): _size(0), _head(nullptr), _tail(nullptr) { }

            BList(const BList<T>& p_other)
            {
                _copy(p_other);
            }

            BList(BList<T>&& p_other)
            {
                _move(p_other);
            }

            BList<T>& operator=(const BList<T>& p_other)
            {
                _copy(p_other);
                return *this;
            }

            BList<T>& operator=(BList<T>&& p_other)
            {
                _move(p_other);
                return *this;
            }

        public:

            value_type& back()
            {
                return _tail->value;
            }

            const value_type& back() const
            {
                return _tail->value;
            }

            value_type& front()
            {
                return _head->value;
            }

            const value_type& front() const
            {
                return _head->value;
            }

            size_t size() const
            {
                return _size;
            }

            bool empty() const
            {
                return _size == 0;
            }

            void clear()
            {
                _clear();
            }

            void push_back(const T& p_value)
            {
                node_type* new_node = _generate_node(p_value);
                _add_tail(new_node);
            }

            void push_back(T&& p_value)
            {
                node_type* new_node = _generate_node(p_value);
                _add_tail(new_node);
            }

            template<typename... Args>
            void emplace_back(Args&&... p_args)
            {
                node_type* new_node = _generate_node(p_args...);
                _add_tail(new_node);
            }

            void push_front(const T& p_value)
            {
                node_type* new_node = _generate_node(p_value);
                _add_head(new_node);
            }

            void push_front(T&& p_value)
            {
                node_type* new_node = _generate_node(p_value);
                _add_head(new_node);
            }

            template<typename... Args>
            void emplace_front(Args&&... p_args)
            {
                node_type* new_node = _generate_node(p_args...);
                _add_head(new_node);
            }

        public:
            iterator begin()
            {
                return iterator(_tail);
            }

            iterator end()
            {
                return iterator(_head->next);
            }
            
        private:

            void _add_tail(node_type* p_ptr)
            {
                if (!size())
                {
                    _add_first(p_ptr);
                }
                else
                {
                    p_ptr->next = _tail;
                    _tail->previous = p_ptr;
                    _tail = p_ptr;
                }

                _increment();
            }

            void _add_head(node_type* p_ptr)
            {
                if (!size())
                {
                    _add_first(p_ptr);
                }
                else
                {
                    p_ptr->previous = _head;
                    _head->next = p_ptr;
                    _head = p_ptr;
                }

                _decrement();
            }

            void _add_first(node_type* p_ptr)
            {
                _tail = _head = p_ptr;
            }

            node_type* _generate_node(const T& p_value)
            {
                node_type* new_node = node_allocator.allocate(1);
                node_allocator.construct(new_node, p_value);
                return new_node;
            }

            node_type* _generate_node(T&& p_value)
            {
                node_type* new_node = node_allocator.allocate(1);
                node_allocator.construct(new_node, p_value);
                return new_node;
            }

            template<typename... Args>
            node_type* _generate_node(Args&&... p_args)
            {
                node_type* new_node = node_allocator.allocate(1);
                node_allocator.construct(new_node, forward<Args>(p_args)...);
                return new_node;
            }

            BK_FORCEINLINE void _increment()
            {
                ++_size;
            }

            BK_FORCEINLINE void _decrement()
            {
                --_size;
            }

            void _move(BList<T>&& p_other)
            {
                _tail = p_other._tail;
                _head = p_other._head;
                _size = p_other._size;

                p_other._tail = nullptr;
                p_other._head = nullptr;
                p_other._size = 0;
            }

            void _copy(const BList<T>& p_other)
            {
                node_type* p = p_other._tail;

                #ifndef BK_NO_MERCY
                if (!p)
                {
                    return;
                }
                #endif

                node_type* new_node = _generate_node_from(p);
                _tail = new_node;
                node_type* previous = _tail;
                p = p->next;

                while(p != p_other._head->next)
                {
                    new_node = _generate_node_from(p);
                    previous->next = new_node;
                    new_node->previous = previous;
                    previous = new_node;
                    p = p->next;
                }

                _head = new_node;

                _size = p_other.size();
            }

            node_type* _generate_node_from(node_type* p_ptr)
            {
                node_type* new_node = node_allocator.allocate(1);
                node_allocator.construct(new_node, *p_ptr);
                return new_node;
            }
            
            void _clear()
            {
                while(!empty())
                {
                    _remove_tail();
                }
            }

            void _remove_tail()
            {
                #ifndef BK_NO_MERCY
                if (size() == 0)
                {
                    return;
                }
                #endif

                if (size() == 1)
                {
                    _remove_last_one();
                }
                else
                {
                    node_type* temp = _tail;
                    _tail = _tail->next;
                    _tail->previous = nullptr;
                    node_allocator.destroy(temp);
                    node_allocator.deallocate(temp, 1);
                    _decrement();
                }
            }

            void _remove_last_one()
            {
                node_type* temp = _tail;
                node_allocator.destroy(temp);
                node_allocator.deallocate(temp, 1);
                _tail = _head = nullptr;
                _decrement();
            }


        private:
            using node_allocator_t = allocator_traits<AllocatorT>::template rebind_alloc<node_type>;
            node_allocator_t node_allocator;
            size_t _size;
            node_type* _head;
            node_type* _tail;
    };

    template<typename T>
    using List = BList<T>;

    template<typename T>
    struct ListIterator
    {
        using type = T;
        using node_type = _internal::ListNode<T>;
        using base = ListIterator<T>;

        public:
            ListIterator(node_type* p_ptr): ptr(p_ptr) {}
        
        public:
            base& operator++() // pre
            {
                ptr = ptr->next;
                return *this;
            }

            const base& operator++() const // pre
            {
                node_type* p = const_cast<node_type*>(ptr);
                p = p->next;
                return *this;
            }

            base& operator++(int) // pre
            {
                base temp = *this;
                ptr = ptr->next;
                return temp;
            }

            const base& operator++(int) const // pre
            {
                base temp = *this;
                node_type* p = const_cast<node_type*>(ptr);
                p = p->next;
                return temp;
            }
        public:
            bool operator==(const ListIterator<T>& p_other) const
            {
                return ptr == p_other.ptr;
            }

            bool operator!=(const ListIterator<T>& p_other) const
            {
                return !(*this == p_other);
            }

        private:
            node_type* ptr;
    };



}

#endif