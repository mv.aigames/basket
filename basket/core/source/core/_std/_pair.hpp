#ifndef BAKET__STD_PAIR_HPP
#define BAKET__STD_PAIR_HPP

namespace basket
{
    template<typename FirstT, typename SecondT>
    struct pair
    {
        using first_type = FirstT;
        using second_type = SecondT;

        first_type first;
        second_type second;

        pair() = default;
        pair(const pair<FirstT, SecondT>& p_other) = default;
        pair(pair<FirstT, SecondT>&& p_other) = default;
        pair(const FirstT& p_first, const SecondT& p_second);

        pair<FirstT, SecondT>& operator=(const pair<FirstT, SecondT>& p_other) = default;
        pair<FirstT, SecondT>& operator=(pair<FirstT, SecondT>&& p_other) = default;

    };

    template<typename F, typename S>
    pair<F, S> make_pair(F&& p_first, S&& p_second)
    {
        return pair<F, S>(std::forward<F>(p_first), std::forward<S>(p_second));
    }

    template<typename FirstT, typename SecondT>
    constexpr bool operator==(const pair<FirstT, SecondT>& p_left, const pair<FirstT, SecondT>& p_right)
    {
        return p_left.first == p_right.first && p_left.second == p_right.second;
    }

    template<typename FirstT, typename SecondT>
    pair<FirstT, SecondT>::pair(const FirstT& p_first, const SecondT& p_second): first(p_first), second(p_second)
    {
        
    }

}

#endif