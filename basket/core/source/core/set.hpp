#ifndef BASKET_SET_HPP
#define BASKET_SET_HPP

#ifdef BASKET_STD
    #include <set>
    namespace basket = std;
#else
    #include "_std/_set.hpp"
#endif

#endif