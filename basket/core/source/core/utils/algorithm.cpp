#include "algorithm.hpp"

namespace basket
{
    vector<unsigned char> bgr_to_rgba(const unsigned char* p_array, size_t p_size)
    {
        std::vector<unsigned char> new_data((p_size / 3) * 4);
        for(uint64_t array_index = 0, new_array_index = 0; array_index < p_size; array_index += 3, new_array_index += 4)
        {
            new_data[new_array_index] = p_array[array_index + 2];
            new_data[new_array_index + 1] = p_array[array_index + 1];
            new_data[new_array_index + 2] = p_array[array_index ];
            new_data[new_array_index + 3] = static_cast<unsigned char>(255);
        }

        return new_data;
    }
}