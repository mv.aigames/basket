#ifndef BASKET_ALGORITHM_HPP
#define BASKET_ALGORITHM_HPP

#include <core/vector.hpp>

namespace basket
{
    template<typename IterType>
    constexpr bool equal(IterType p_first1, IterType p_last1, IterType p_first2)
    {
        for(; p_first1 != p_last1; ++p_first1, ++p_first2)
        {
            if (*p_first1 != *p_first2)
            {
                return false;
            }
        }

        return true;
    }

    // template<typename T>
    // consteval T shift_left(T p_value)
    // {
    //     return T(1) << p_value;
    // }

    // template<typename T>
    // consteval T shift_right(T p_value)
    // {
    //     return T(1) >> p_value;
    // }

    // vector<uint8_t> inflate(const uint8_t* p_data, const size_t p_size);
    // inline vector<uint8_t> inflate(std::vector<uint8_t>& p_data) { return inflate(p_data.data(), p_data.size()); }  
    // vector<unsigned char> deflate(const std::vector<unsigned char>& p_data);

    // inline uint32_t swap_endian(const uint32_t p_value)
    // {
    //     const unsigned char* v = reinterpret_cast<const unsigned char*>(&p_value);
    //     return 
    //     (static_cast<uint32_t>(v[0]) << 24) +
    //     (static_cast<uint32_t>(v[1]) << 16) +
    //     (static_cast<uint32_t>(v[2]) << 8) +
    //     (static_cast<uint32_t>(v[3]) << 0);
    // }

    // vector<unsigned char> bgr_to_rgba(const unsigned char* p_array, size_t p_size);
}

#endif