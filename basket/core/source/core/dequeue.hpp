#ifndef BASKET_DEQUEUE_HPP
#define BASKET_DEQUEUE_HPP

#ifdef BASKET_STD
    #include <dequeue>
    namespace basket = std;
#else
    #include "_std/_dequeue.hpp"
#endif

#endif