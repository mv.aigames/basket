#ifndef BASKET_TYPEDEFS_HPP
#define BASKET_TYPEDEFS_HPP

#include "pch.hpp"
#include <stdint.h>

namespace basket
{
    

using nullptr_t = decltype(nullptr);

#ifdef BASKET_SIMPLE_PRECISION
    using real_t = float;
    using double_real_t = double;
    using max_real_t = long double;
#else
    using real_t = double;
    using double_real_t = double;
    using max_real_t = long double;
#endif

    
}

#endif