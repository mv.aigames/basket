#ifndef BASKET_LIMITS_HPP
#define BASKET_LIMITS_HPP

#ifdef BASKET_STD
    #include <limits>
    namespace basket = std;
#else
    #include "_std/_limits.hpp"
#endif

#endif