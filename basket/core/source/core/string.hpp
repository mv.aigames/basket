#ifndef BASKET_STRING_HPP
#define BASKET_STRING_HPP

#ifdef BASKET_STD
    #include <string>
    namespace basket = std;
#else
    #include "_std/_string.hpp"
#endif

#endif