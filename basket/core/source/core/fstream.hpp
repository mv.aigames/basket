#ifndef BASKET_FSTREAM_HPP
#define BASKET_FSTREAM_HPP

#ifdef BASKET_STD
    #include <fstream>
    namespace basket = std;
#else
    #include "_std/_fstream.hpp"
#endif

#endif