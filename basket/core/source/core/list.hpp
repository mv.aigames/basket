#ifndef BASKET_LIST_HPP
#define BASKET_LIST_HPP

#ifdef BASKET_STD
    #include <LIST>
    namespace basket = std;
#else
    #include "_std/_list.hpp"
#endif

#endif