#ifndef BASKET_VECTOR_HPP
#define BASKET_VECTOR_HPP

#ifdef BASKET_STD
    #include <vector>
    namespace basket = std;
#else
    #include "_std/_vector.hpp"
#endif

#endif