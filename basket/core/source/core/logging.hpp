#ifndef BASKET_LOGGING_HPP
#define BASKET_LOGGING_HPP

#include "pch.hpp"
#include "macros.hpp"
#include <core/string.hpp>
#include <spdlog/spdlog.h>
#include <spdlog/fmt/bundled/format.h>
#include <core/utility.hpp>


#ifndef BASKET_STD
template<>
struct fmt::formatter<basket::string>
{
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin())
    {
        return ctx.end();
    }

    template <typename FormatContext>
    auto format(const basket::string& input, FormatContext& ctx) -> decltype(ctx.out())
    {
        return format_to(ctx.out(),
            "{}",
            input.c_str());
    }
};
#endif


namespace basket
{
    enum class LogLevel: uint8_t
    {
        info,
        warning,
        error,
        fatal
    };

    

    struct BASKET_API _BK_LogCategory
    {
        string _log_name;
        std::shared_ptr<spdlog::logger> _logger;
        _BK_LogCategory(const string& p_log_name);
        _BK_LogCategory() {}
        template<typename... Args>
        static void print_log(LogLevel p_level, const string& p_message, Args&&... p_args)
        {
            switch(p_level)
            {
                case LogLevel::info:
                    spdlog::info(fmt::runtime(p_message.c_str()), p_args...);
                    break;
                case LogLevel::warning:
                    spdlog::warn(fmt::runtime(p_message.c_str()), p_args...);
                    break;
                case LogLevel::error:
                    spdlog::error(fmt::runtime(p_message.c_str()), p_args...);
                    break;
                case LogLevel::fatal:
                    spdlog::critical(fmt::runtime(p_message.c_str()), p_args...);
                    break;
            }
        }
        template<typename... Args>
        void print(LogLevel p_level, const string& p_message, Args&&... p_args)
        {
            spdlog::info("dummy");
            // switch(p_level)
            // {
            //     case LogLevel::info:
            //         _logger->info(fmt::runtime(p_message), p_args...);
            //         break;
            //     case LogLevel::warning:
            //         _logger->warn(fmt::runtime(p_message), p_args...);
            //         break;
            //     case LogLevel::error:
            //         _logger->error(fmt::runtime(p_message), p_args...);
            //         break;
            //     case LogLevel::fatal:
            //         _logger->critical(fmt::runtime(p_message), p_args...);
            //         break;
            // }
        }
    };
}


#define _BK_LOG_VARIABLE(LOG_NAME) _BK_Log_##LOG_NAME


// #define BK_DECLARE_LOG(LOG_NAME) BASKET_API extern ::basket::_BK_LogCategory _BK_LOG_VARIABLE(LOG_NAME)
// #define BK_DEFINE_LOG(LOG_NAME) ::basket::_BK_LogCategory _BK_LOG_VARIABLE(LOG_NAME)
#define BK_DECLARE_LOG(LOG_NAME)
#define BK_DEFINE_LOG(LOG_NAME)

// #define BK_LOG(LOG_NAME, LOG_LEVEL, LOG_MESSAGE, ...) ::_BK_LOG_VARIABLE(LOG_NAME).print(::basket::LogLevel::LOG_LEVEL, LOG_MESSAGE, __VA_ARGS__)
#define BK_LOG(LOG_NAME, LOG_LEVEL, LOG_MESSAGE, ...) ::basket::_BK_LogCategory::print_log(::basket::LogLevel::LOG_LEVEL, LOG_MESSAGE, __VA_ARGS__)




#endif