#include "uuid.hpp"

#include "pch.hpp"

static std::random_device device;
static std::mt19937_64 generator(device());
static std::uniform_int_distribution<uint64_t> distribution(std::numeric_limits<uint64_t>::min(), std::numeric_limits<uint64_t>::max());


namespace basket
{
    UUID::UUID(): id(distribution(generator))
    {

    }

    UUID::UUID(const UUID& p_other): id(p_other.id)
    {

    }

    UUID& UUID::operator=(const UUID& p_other)
    {
        id = p_other.id;
        return *this;
    }

    UUID::UUID(UUID&& p_other): id(p_other.id)
    {
        p_other.id = invalid_id;
    }

    UUID& UUID::operator=(UUID&& p_other)
    {
        p_other.id = invalid_id;
        return *this;
    }

    bool operator== (const UUID& lhs, const UUID& rhs)
    {
        return lhs.id == rhs.id;
    }

    bool operator< (const UUID& lhs, const UUID& rhs)
    {
        return lhs.id < rhs.id;
    }

    typename UUID::integer_type UUID::get() const
    {
        return id;
    }
}