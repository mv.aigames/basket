#ifndef BASKET_MODULE_HPP
#define BASKET_MODULE_HPP

#include <core/results.hpp>
#include <core/update_group.hpp>

namespace basket
{
    class BASKET_API AModule: public AUpdate
    {
        // AUpdate
        public:
            virtual void update(const real_t p_delta) override {}
        // End AUpdate

        public:
            AModule(AUpdate::Group p_group): AUpdate(p_group) {}
            
            virtual Result pre_init() = 0;
            virtual Result init() = 0;
            virtual Result post_init() = 0;

            virtual Result pre_destroy() = 0;
            virtual Result destroy() = 0;
            virtual Result post_destroy() = 0;
    };
}


#endif