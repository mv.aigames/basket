#ifndef BASKET_CONSTRAINTS_HPP
#define BASKET_CONSTRAINTS_HPP

#include "pch.hpp"

namespace basket
{
    // vector generic. It can be Vector2 or Vector3
    template<typename T, typename = int>
    struct is_vector3: std::false_type {};

    template<typename T>
    struct is_vector3<T, decltype((void)T::x, (void)T::y, (void)T::z, 0)>: std::true_type {};

    template<typename T>
    concept concept_vector = is_vector3<T>::value;

    template<typename T, typename = int>
    struct is_vector2: std::false_type {};

    template<bool = false>
    struct value_if { };

    template<>
    struct value_if<true> { static constexpr int value = 1; };

    template<typename T, typename = void>
    struct has_vector_z_member_helper: value_if<true> {};

    template<typename T>
    struct has_vector_z_member_helper<T, std::void_t<decltype(std::declval<T>().z)> >: value_if<false> {};

    template<typename T>
    struct is_vector2<T, decltype((void)T::x, (void)T::y, (void)has_vector_z_member_helper<T>::value, 0)>: std::true_type {};

    template<typename T>
    concept concept_vector2 = is_vector2<T>::value;

    template<typename T>
    concept concept_numeric = std::is_integral_v<T> || std::is_floating_point_v<T>;

    template<typename T>
    concept concept_is_component = std::is_trivial<T>::value;

    
}

#endif