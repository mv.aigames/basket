#ifndef BASKET_OS_HPP
#define BASKET_OS_HPP

#include <core/macros.hpp>
#include <core/results.hpp>
#include <core/string.hpp>
#include <core/vector.hpp>
#include <cstdlib>



namespace basket
{
    class BASKET_API OS
    {
        public:
            static void abort();
            static Result execute(const string& p_name, const string& p_args);
            static Result console(const string& p_args);
            static string get_environment(const string& p_env) { return std::getenv(p_env.c_str()); }
            static string _get_vulkan_sdk_path_native();
            static Result glslc(const string& p_filename, const string& p_out);
    };
}

#endif