#ifndef BASKET_UNORDERED_MAP_HPP
#define BASKET_UNORDERED_MAP_HPP

#ifdef BASKET_STD
    #include <unordered_map>
    namespace basket = std;
#else
    #include "_std/_unordered_map.hpp"
#endif

#endif