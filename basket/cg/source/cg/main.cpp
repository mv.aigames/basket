#include "cg.hpp"

#include <core/logging.hpp>
#include <core/io/file.hpp>

using namespace basket;

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        BK_LOG(cat, error, "Not enough params");
        return -1;
    }


    File file;
    if(file.open(argv[1]) != Result::success)
    {
        BK_LOG(cat, error, "Failed to open the file");
        return -1;
    }

    string content = file.get_content();

    CG cg;
    cg.compile(content);

    return 0;
}