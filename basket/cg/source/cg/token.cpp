#include "token.hpp"

namespace basket
{
    Token::Token(const Token& p_other): value(p_other.value) {}

    Token::Token(Token&& p_other): value(p_other.value) {}

    Token::Token(const string& p_name, Token::TokenValue p_value): value(p_name, p_value)
    {
        
    }

    Token& Token::operator=(const Token& p_other)
    {
        value = p_other.value;
        return *this;
    }

    Token& Token::operator=(Token&& p_other)
    {
        value = std::move(p_other.value);
        return *this;
    }

    Token Token::generate(const string& p_name, Token::TokenValue p_value)
    {
        return Token(p_name, p_value);
    }
}