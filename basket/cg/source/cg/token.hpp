#ifndef BASKET_TOKEN_HPP
#define BASKET_TOKEN_HPP

#include <core/macros.hpp>
#include <core/string.hpp>
#include <core/containers/pair.hpp>

namespace basket
{
    class BASKET_API Token
    {
        public:
            enum TokenValue
            {
                eof,
                identifier,
                open_parenthesis,close_parenthesis,
                open_braket, close_braket,
                open_square_braket, close_square_braket,

                return_key, // return
                colon_key, // :
                semicolo_key, // ;
                dot_key, // .
                native_key, // native
                single_line_coment, // //
                start_multiline_comment, // /*
                end_multiline_comment, // */
                preprocessor_directive, // #
                equal_operator, // =
                equal_compare_operator, // ==
                inequality_operator, // !=
                negate_operator, // !

            };
        
        public:
            pair<string, TokenValue> value;
        
        public:
            Token() = default;
            Token(const Token& p_other);
            Token(Token&& p_other);
            Token(const string& p_name, TokenValue p_value);

        public:
            Token& operator=(const Token& p_other);
            Token& operator=(Token&& p_other);
        
        public:
            static Token generate(const string& p_name, TokenValue p_value);
            friend constexpr bool operator==(const Token& p_left, const Token& p_right);
            friend struct std::hash<Token>;
    };

    constexpr bool operator==(const Token& p_left, const Token& p_right)
    {
        return p_left.value.first == p_right.value.first;
    }

    
}

namespace std
{
    template<>
    struct hash<::basket::Token>
    {
        size_t operator()(const ::basket::Token& p_token) const
        {
            return p_token.value.second;
        }
    };
}

#define BK_GENERATE_TOKEN(VALUE) Token::generate(#VALUE, VALUE)
#define BK_GENERATE_CONSTRUCTOR_TOKEN(VALUE) Token(#VALUE, VALUE)

#endif