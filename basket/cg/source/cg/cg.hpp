#ifndef BASKET_CG_HPP
#define BASKET_CG_HPP

#include <core/macros.hpp>
#include <core/results.hpp>

namespace basket
{
    class BASKET_API CG
    {
        public:
            Result compile(const string& p_source);
    };
}

#endif