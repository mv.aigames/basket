#ifndef BASKET_LEXER_HPP
#define BASKET_LEXER_HPP

#include <core/macros.hpp>
#include <core/results.hpp>
#include <core/vector.hpp>
#include <core/containers/hash_map.hpp>
#include <cg/token.hpp>

namespace basket
{
    class BASKET_API Lexer
    {
        public:
            struct ScanResult
            {
                vector<Token> tokens;
                bool sucess;
            };

        private:
            static HashMap<Token, string> keywords;
        

        public:
            ScanResult scan(const string& p_source);
    };

    HashMap<Token, string> Lexer::keywords = {
        {BK_GENERATE_CONSTRUCTOR_TOKEN(Token::close_braket), "}"}
    };
}

#endif