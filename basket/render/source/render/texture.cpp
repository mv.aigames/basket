#include "texture.hpp"

// #define STB_IMAGE_IMPLEMENTATION
// #include <stb_image.h>
// #include <zlib.h>

#include <core/functions.hpp>
#include <core/logging.hpp>
#include <core/macros.hpp>
#include <io/file.hpp>
#include <utils/algorithms.hpp>

using namespace basket;

/*******************************/
//          AImage
/*******************************/

Color AImage::get_color(size_t p_x, size_t p_y) const noexcept
{
    // each cell is 4 inner-cell.
    const byte_type* color = &data[p_y * width + p_y * 4];
    return Color(
        static_cast<float>(color[0]) / 255.0f,
        static_cast<float>(color[1]) / 255.0f,
        static_cast<float>(color[2]) / 255.0f,
        static_cast<float>(color[3]) / 255.0f
    );
}

/*******************************/
//         End AImage
/*******************************/



/*******************************/
//          PNGReader
/*******************************/

std::vector<uint8_t> PNGReader::decompress_idat(const uint8_t* p_data, size_t p_size) const
{
    // z_stream stream;
    // stream.zalloc = Z_NULL;
    // stream.zfree = Z_NULL;
    // stream.opaque = Z_NULL;
    // stream.next_in = Z_NULL;
    // stream.avail_in = 0;
    // int value = inflateInit2(&stream, -MAX_WBITS);
    // if (value != Z_OK)
    // {
    //     return {};
    // }

    // std::vector<uint8_t> out;
    // out.resize(p_size);

    // stream.avail_in = p_size;
    // stream.next_in = const_cast<Bytef*>(p_data);
    // stream.avail_out = p_size * 5;
    // stream.next_out = out.data();

    
    // int err = ::inflate(&stream, Z_NO_FLUSH);
    // if (err != Z_OK)
    // {
    //     BK_LOG(render, info, ("Error using inflate");
    // }
    // else if (err == Z_STREAM_END)
    // {
    //     BK_LOG(render, info, ("Stream end");
    // }

    // inflateEnd(&stream);

    // return out;
    return {};
}


Result PNGReader::read(const container_type& content)
{
    if (content.size() == 0) return Result::failed;
    uint64_t index = 8;
    const uint32_t header_id = 0x49484452;
    const uint32_t header_file_id = swap_endian(*reinterpret_cast<const uint32_t*>(&content[index + 4]));
    if (header_id != header_file_id)
    {
        BK_ASSERT(false, "Cannot read IHDR");
        return Result::failed;
    }

    ihdr.length = swap_endian(*reinterpret_cast<const uint32_t*>(&content[index]));
    ihdr.width = swap_endian(*reinterpret_cast<const uint32_t*>(&content[index + 8]));
    ihdr.height = swap_endian(*reinterpret_cast<const uint32_t*>(&content[index + 12]));
    ihdr.bit_depth = static_cast<uint8_t>(content[index + 16]);
    ihdr.color_type = static_cast<uint8_t>(content[index + 17]);
    ihdr.compression_method = static_cast<uint8_t>(content[index + 18]);
    ihdr.filter_method = static_cast<uint8_t>(content[index + 19]);
    ihdr.interlance_method = static_cast<uint8_t>(content[index + 20]);
    ihdr.crc = swap_endian(*reinterpret_cast<const uint32_t*>(&content[index + 21]));

    // Now it is time to read IDAT
    index += ihdr.length + 4 + 4 + 4;
    uint32_t chunk_idat_id = 0x49444154;
    uint32_t chunk_iend_id = 0x49454E44;
    while(index < content.size())
    {
        uint32_t length = swap_endian(*reinterpret_cast<const uint32_t*>(&content[index]));
        uint32_t chunk_file_id = swap_endian(*reinterpret_cast<const uint32_t*>(&content[index + 4]));
        // TODO: Add support for multiple chunk types.
        if (chunk_idat_id != chunk_file_id)
        {
            index += length + 4 + 4 + 4;
            continue;
        }
        else
        {
            std::vector data = decompress_idat(content.data() + index + 8, length);
            set_data(std::move(data));
            index += length + 4 + 4 + 4;
        }

        if (chunk_iend_id == chunk_file_id)
        {
            BK_LOG(render, info, "Reached end of file!");
            break;
        }
    }
    return Result::success;
}


/*******************************/
//         End PNGReader
/*******************************/

/*******************************/
//          BMPReader
/*******************************/

Result BMPReader::read(const container_type& content)
{
    int32_t width = *reinterpret_cast<const int32_t*>(&content[0x12]);
    int32_t height = *reinterpret_cast<const int32_t*>(&content[0x16]);
    uint32_t offset = *reinterpret_cast<const int32_t*>(&content[0xA]);

    info_header.width = width;
    info_header.height = height;
    
    uint16_t bpp = *reinterpret_cast<const uint16_t*>(&content[0x1C]);
    info_header.bits_per_pixel = bpp;
    
    container_type formatted_data;
    switch(info_header.bits_per_pixel)
    {
        case 0x20: // 32 bits
        {
            set_pixel_format(PixelFormat::bgra);
            container_type data = container_type(content.data() + offset, content.data() + (offset + width * height * 4));
            formatted_data = std::move(data);
        }
            break;
        case 0x18: // 24 bits
            set_pixel_format(PixelFormat::rgb);
            formatted_data = container_type(content.data() + offset, content.data() + (offset + width * height * 3));
            formatted_data = bgr_to_rgba(formatted_data.data(), formatted_data.size());
            break;
        default:
            BK_LOG(render, info, "No supported bits per pixel format");
            break;
    }

    set_data(formatted_data);
    set_width(width);
    set_height(height);
    return Result::success;
}

AImage::container_type BMPReader::read_data_32bits(const container_type& p_data) const
{
    container_type new_data;
    new_data.reserve(p_data.size());
    for(size_t index = 0; index < p_data.size(); index += 4)
    {
        new_data.push_back(p_data[index + 1]);
        new_data.push_back(p_data[index + 2]);
        new_data.push_back(p_data[index + 3]);
        new_data.push_back(p_data[index]);
    }

    return new_data;
}

/*******************************/
//         End BMPReader
/*******************************/

Texture::Texture()
{
    readers[File::Format::png] = std::make_shared<PNGReader>();
    readers[File::Format::bmp] = std::make_shared<BMPReader>();
    readers[File::Format::unknown] = std::make_shared<EmptyImage>();
}

Texture::Texture(const std::string& p_filepath) : Texture()
{
    open(p_filepath);
}

Result Texture::open(const std::string& p_filepath)
{
    File file;

    if (file.open(p_filepath) == Result::success)
    {
        File::Format f = file.get_file_format();
        // TODO: Replace it with container type
        auto it = readers.find(f);
        if (it != readers.end())
        {
            reader = it->second;
            return reader->read(file.get_data());
        }
        else
        {
            reader = readers[File::Format::unknown];
            return Result::unknown_file_format;
        }
    }
    else
    {
        reader = readers[File::Format::unknown];
        return Result::filepath_failed;
    }



    return Result::success;
}

Result Texture::load_bmp_file(const std::vector<unsigned char>& p_file_content)
{
    BK_ASSERT(false, "No support for BMP file yet");
    return Result::failed;
}

Result Texture::load_png_file(const std::vector<unsigned char>& p_file_content)
{
    return Result::success;
}

uint32_t Texture::get_magic_number(const AImage::container_type& p_file_content) const
{
    return 0;
}
