#include "module.hpp"

#include <core/logging.hpp>
#include <render/render.hpp>
#include <core/glfw.hpp>
#include <core/os.hpp>
#include <render/shader/shader.hpp>

BK_DEFINE_LOG(render);

static void glfw_error_callback(int error, const char* description)
{
    BK_LOG(render, info, "GLFW Error ", error, " ", description);
}

namespace basket::render
{
    Result Module::pre_init() 
    {
        glfwSetErrorCallback(glfw_error_callback);
        if (!glfwInit())
        {
            BK_LOG(render, info, "GLFW couldn't be initialized properly");
            OS::abort();
        }
        Shader::compile(string("render/tests/triangle.vert"));
        Shader::compile(string("render/tests/triangle.frag"));
        _render->startup();
        BK_LOG(render, info, "Render pre_init");
        return Result::success;
    }
    Result Module::init() 
    {
        BK_LOG(render, info, "Render init");
        _render->present();
        return Result::success;
    }
    Result Module::post_init() 
    {
        BK_LOG(render, info, "Render post_init");
        return Result::success;
    }
    Result Module::pre_destroy() 
    {
        BK_LOG(render, info, "Render pre_destroy");
        return Result::success;
    }
    Result Module::destroy() 
    {
        _render->shutdown();
        BK_LOG(render, info, "Render destroy");
        return Result::success;
    }
    Result Module::post_destroy() 
    {
        BK_LOG(render, info, "Render post_destroy");
        delete _render;
        return Result::success;
    }

    void Module::update(const real_t p_delta)
    {
        _render->draw(p_delta);
    }

    
    ::basket::Render* Module::get_render()
    {
        return _render;
    }

    Module::Module(): AModule(AUpdate::Group::render)
    {
        _render = new ::basket::Render();
    }
}
