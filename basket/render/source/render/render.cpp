#include "render.hpp"

#include "drivers/vulkan_driver.hpp"

#include <core/glfw.hpp>
#include <render/window/glfw_window.hpp>

namespace basket
{
    void Render::startup(const Driver p_driver)
    {
        

        switch (p_driver)
        {
            case Driver::Vulkan:
                driver = new VulkanDriver;
                break;
            case Driver::DirectX:
                BK_ASSERT(false, "DirectX is not supported");
                break;
            default:
                BK_ASSERT(false, "Unknown Redner driver selected");
        }

        if (driver)
        {
            // driver->init();
        }
    }

    void Render::present()
    {
        // driver->set_window(_window);
        // driver->present();
    }

    void Render::generate_window(const struct WindowGenerationData& p_data)
    {
        _window = new GLFWWindow(p_data.title.c_str(), p_data.width, p_data.height);
    }

    class AWindow* Render::get_window()
    {
        return _window;
    }

    void Render::draw(const real_t p_delta)
    {
        // driver->draw(p_delta);
    }

    void Render::shutdown()
    {
        // if (driver)
        // {
        //     driver->stop();
        // }
    }

    const vector<RenderDevice> Render::get_device_list() const
    {
        // BK_SAFE_RETURN(!driver);
        if (driver)
        {
            return driver->get_device_list();
        }
        return {};

    }

    void Render::select_device(const string& p_device)
    {
        BK_SAFE_RETURN(!driver);
        return driver->select_device(p_device);
    }
}