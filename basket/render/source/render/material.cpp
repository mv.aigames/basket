#include "material.hpp"

using namespace basket;

void Material::activate() noexcept
{
    if (shared_pointer<AShader> s = shader.lock())
    {
        s->use();
    }
}

void Material::set_param(const std::string& p_id, int p_value)
{
    auto it = integer_params.find(p_id);
    if (it != integer_params.end())
    {
        it->second = p_value;
        if(auto s = shader.lock())
        {
            s->set_integer(p_id, p_value);
        }
    }
}

void Material::set_param(const std::string& p_id, float p_value)
{
    auto it = floating_params.find(p_id);
    if (it != floating_params.end())
    {
        it->second = p_value;
        if(auto s = shader.lock())
        {
            s->set_float(p_id, p_value);
        }
    }
}

void Material::set_param(const std::string& p_id, const Texture& p_value)
{
    auto it = texture_params.find(p_id);
    if (it != texture_params.end())
    {
        it->second = p_value;
        if(auto s = shader.lock())
        {
            s->set_texture(p_id, p_value);
        }
    }
}

void Material::set_param(const std::string& p_id, const Color& p_value)
{
    auto it = vector4_params.find(p_id);
    if (it != vector4_params.end())
    {
        it->second = p_value;
        if(auto s = shader.lock())
        {
            s->set_vector(p_id, p_value);
        }
    }
}

void Material::set_param(const std::string& p_id, const Matrix44& p_value)
{
    auto it = matrix44_params.find(p_id);
    if (it != matrix44_params.end())
    {
        it->second = p_value;
        if(auto s = shader.lock())
        {
            s->set_matrix(p_id, p_value);
        }
    }
}

void Material::set_param(const std::string& p_id, const Vector3& p_value)
{
    auto it = vector3_params.find(p_id);
    if (it != vector3_params.end())
    {
        it->second = p_value;
        if(auto s = shader.lock())
        {
            s->set_vector(p_id, p_value);
        }
    }
}

void Material::set_shader(shared_pointer<AShader>& p_shader) noexcept
{
    shader = weak_pointer<AShader>(p_shader);
    load_params();
}

void Material::load_params()
{
    integer_params.clear();
    floating_params.clear();
    matrix44_params.clear();
    vector3_params.clear();
    vector4_params.clear();
    texture_params.clear();

    if (auto s = shader.lock())
    {
        std::vector<AShader::VariableInfo> params = s->get_params();
        for (const auto& vi : params)
        {
            switch(vi.type)
            {
                case AShader::VariableType::integer:
                    integer_params[vi.name] = 0;
                break;
                case AShader::VariableType::floating:
                    floating_params[vi.name] = 0.0f;
                break;
                case AShader::VariableType::matrix44:
                    matrix44_params[vi.name] = Matrix44::identity;
                break;
                case AShader::VariableType::vector3:
                    vector3_params[vi.name] = 0.0f;
                break;
                case AShader::VariableType::vector4:
                    vector4_params[vi.name] = 0.0f;
                break;
                case AShader::VariableType::sampler2d:
                    texture_params[vi.name] = Texture();
                    break;
            }
        }
    }
}

void Material::update_params()
{
    // Iterate over all params.

}

void Material::draw()
{
    if(auto s = shader.lock())
    {
        s->draw();
    }
}