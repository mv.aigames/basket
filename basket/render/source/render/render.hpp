#ifndef BASKET_ERENDER_HPP
#define BASKET_ERENDER_HPP

#include <core/macros.hpp>
#include <core/memory.hpp>
#include <core/string.hpp>

#include <render/drivers/render_driver.hpp>
#include <core/typedefs.hpp>


namespace basket
{

    

    class BASKET_API Render
    {
        public:
            enum class Driver
            {
                Vulkan,
                DirectX
            };

            class IRenderDriver* driver;
            class AWindow* _window;
        public:
            void startup(const Driver p_driver = Driver::Vulkan);
            void present();
            void generate_window(const struct WindowGenerationData& p_data);
            void draw(const real_t p_delta);
            class AWindow* get_window();
            void shutdown();
            class IRenderDriver* get_render_driver() { return driver; }
            const vector<RenderDevice> get_device_list() const;
            void select_device(const string& p_device);
    };
}

#endif