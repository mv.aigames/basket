#ifndef BASKET_IDRIVER_HPP
#define BASKET_IDRIVER_HPP

#include <core/string.hpp>
#include <core/vector.hpp>
#include <core/macros.hpp>
#include <core/typedefs.hpp>

namespace basket
{

    struct RenderDevice
    {
        uint8_t _id;
        string name;
        // uint64_t memory; // In bytes
    };
    /**
     * @brief This interface is only for initialization purpose.
     * It is the job of the implementation to handle all the needs.
     * 
     */
    class BASKET_API IRenderDriver
    {
        public:
            virtual void init() = 0;
            virtual void stop() = 0;
            virtual void present() = 0;
            virtual void set_window(class AWindow* p_window) = 0;
            virtual class AWindow* get_window() const = 0;
            virtual void select_device(const string& p_device) = 0;
            virtual const vector<RenderDevice> get_device_list() const = 0;
            virtual void use_device(const RenderDevice& p_device) = 0;
            virtual ~IRenderDriver() {}
            virtual void draw(float p_delta) = 0;
    };
}

#endif