#ifndef BASKET_VULKAN_DRIVER_HPP
#define BASKET_VULKAN_DRIVER_HPP

#include "render_driver.hpp"

#include <core/macros.hpp>
#include <core/vector.hpp>

#ifdef BASKET_WINDOWS
    #define VK_USE_PLATFORM_WIN32_KHR
#else
    #error "Only windows is supported"
#endif
#include <vulkan/vulkan.h>

#include <render/render.hpp>
#include <render/render_results.hpp>
#include <render/vulkan/vulkan_record_command.hpp>

#define BK_VK_ASSERT(COND, MESSAGE) BK_ASSERT(COND, MESSAGE)

namespace basket
{

    struct _SwapchainSupportDetails
    {
        VkSurfaceCapabilitiesKHR capabilities;
        vector<VkSurfaceFormatKHR> formats;
        vector<VkPresentModeKHR> present_modes;
    };

    struct _SyncObject
    {
        VkSemaphore render_ready_sempahore;
        VkSemaphore image_ready_semaphore;
        VkFence in_flight_fence;
    };

    struct _CommandGroup
    {
        VkCommandPool _command_pool;
        vector<VkCommandBuffer> _command_buffers;
    };

    struct _FrameData
    {
        vector<_SyncObject> sync_objects;
        vector<_CommandGroup> commands;
    };

    struct VulkanSubmitData
    {
        int queue_index;
        VkSubmitInfo info;
    };

    class BASKET_API VulkanDriver: public IRenderDriver
    {
        
        public:
            static VulkanDriver& get();

        private:
            uint8_t _max_frames_in_flight = 2;
            class AWindow* _window;
            vector<_FrameData> _frames_data;
            
        // IRenderDrive
        public:
            virtual void init() override;
            virtual void stop() override;
            virtual void present() override;
            virtual void select_device(const string& p_device) override {}
            virtual void use_device(const RenderDevice& p_device) override { _physical_device = _physical_devices[p_device._id]; }
            virtual const vector<RenderDevice> get_device_list() const override { return _render_devices; }
            virtual void set_window(class AWindow* p_window) override { _window = p_window; }
            virtual AWindow* get_window() const { return _window; }
            virtual void draw(float p_delta) override;
        // End IRenderDrive

        private:
            uint32_t _current_frame;
            vector<_CommandGroup> _command_groups;
            vector<VulkanSubmitData> _submits_data;
            VkQueue _graphic_queue;
            vector<VkRenderPass> _render_passes;
            vector<VkFence> _fences;
            vector<vector<VkSemaphore>> _semaphores;
            // Transfer section.
            VkCommandPool _transfer_command_pool;
            VkQueue _transfer_queue;
            // End Transfer section.

            // vector<VkSubmitInfo> _submits_info;
            vector<VkCommandBuffer> _command_buffers_to_submit;
        public:

            uint32_t create_command_pool(VkCommandPoolCreateInfo& p_info);
            VkCommandPool get_command_pool(uint32_t p_index);
            VkCommandBuffer get_command_buffer(uint32_t p_pool_index, uint32_t p_buffer_index);
            decltype(_current_frame) get_current_frame() const { return _current_frame; }
            decltype(_max_frames_in_flight) get_max_frames_in_flight() const { return _max_frames_in_flight; }
            void add_submit_data(const VulkanSubmitData& p_data);
            VkQueue get_graphic_queue() const { return _graphic_queue; }
            VkQueue get_transfer_queue() const { return _transfer_queue; }
            void create_command_buffers(uint32_t p_command_pool_index, VkCommandBufferAllocateInfo& p_info);
            uint32_t create_render_pass();
            VkShaderModule create_shader_module(const vector<char>& p_code);
            VkFramebuffer get_framebuffer(uint32_t p_image_index) { return _swapchain_frame_buffers[p_image_index]; }
            uint32_t create_fences(uint32_t p_amount);
            uint32_t create_semaphores(uint32_t p_amount);
            void add_submit(const VkSubmitInfo& p_info);
            void _recreate_swapchain();
            VkFence& get_fence(uint32_t p_index) const { return _fences[p_index]; }
            VkSemaphore get_frame_semaphore(uint32_t p_index, uint32_t p_frame_index) { return _semaphores[p_index][p_frame_index]; }
            void add_command_buffer_to_submit(VkCommandBuffer p_command_buffer) { _command_buffers_to_submit.push_back(p_command_buffer); }
            // void add_command_buffer_to_submit(VkCommandBuffer p_command);
            // uint32_t create_sync_objects()
        private:
            void _graphics_records(int32_t p_image_index);

        // Dummy
        public:
            // VkCommandPool _get_command_pool() const { return _command_pool; }
            // VkCommandBuffer _get_command_buffer() const { return _command_buffers[0]; }
            // VkCommandBuffer _get_current_command_buffer() const { return _command_buffers[_current_frame]; }
            // VkSemaphore _get_image_available_semaphore() const { return _image_available_semaphores[_current_frame]; }
            // VkSemaphore _get_render_finished_semaphores() const { return _render_finished_semaphores[_current_frame]; }
            VkSwapchainKHR _get_swapchain() const { return _swapchain; }
            // VkRenderPass _get_render_pass() const { return _render_pass; }
            VkFramebuffer _get_frame_buffer() const { return _swapchain_frame_buffers[_current_frame]; }
            VkFramebuffer get_framebuffer(uint32_t p_index) const { return _swapchain_frame_buffers[p_index]; }
            VkExtent2D _get_extent() const { return _swapchain_extent; }
            // VkFence _get_current_fence() const { return _in_flight_fences[_current_frame]; }
            RenderResult create_buffer(VkDeviceSize p_size, VkBufferUsageFlags p_usage, VkMemoryPropertyFlags p_properties, VkBuffer& p_buffer, VkDeviceMemory& p_buffer_memory);
            void copy_buffer(VkBuffer p_from, VkBuffer p_to, VkDeviceSize p_size);
        // End Dummy


        /************************************************
         *                  Records
         ***********************************************/
        private:
            vector<IVulkanRecordCommand*> _graphic_records;
        public:
            void add_graphic_record(IVulkanRecordCommand* p_record);
        
        /************************************************
         *                End Records
         ***********************************************/


        public:
            

        public:
            VkInstance get_instance() { return instance; }
            VkSurfaceKHR get_surface() { return _surface; }
            int32_t get_graphic_queue_family_index() { return _find_queue_family_by_flags(_physical_device, VK_QUEUE_GRAPHICS_BIT); }
            VkRenderPass get_render_pass(uint32_t p_index) { return _render_passes[p_index]; }
            VkPhysicalDevice get_physical_device() { return _physical_device; }
            VkDevice get_device() { return _device; }
            VkQueue get_queue() { return _graphic_queue; }

        private:
            
            void _load_physical_devices();

        private:
            void _pick_physical_device();
            void _create_logical_device();
            bool _check_layers();
            vector<const char*> _get_required_extensions() const;
            void _create_instance();
            void _setup_debug_callback();
            void clean();
            bool _is_device_suitable(const VkPhysicalDevice& p_device);
            int32_t _find_queue_family_by_flags(VkPhysicalDevice p_device, uint32_t p_flags);
            void _create_surface();
            
            _SwapchainSupportDetails _query_swapchain_support(VkPhysicalDevice p_device);
            VkSurfaceFormatKHR _choose_swapchain_surface_format(const vector<VkSurfaceFormatKHR>& p_formats);
            VkPresentModeKHR _choose_swapchain_present_mode(const vector<VkPresentModeKHR>& p_presents);
            VkExtent2D _choose_swapchain_extents(const VkSurfaceCapabilitiesKHR& p_capabilities);
            void _create_swapchain();
            void _create_image_views();
            bool _create_render_pass(VkRenderPass& p_render_pass);
            void _create_framebuffers();
            void _create_command_pool(VkCommandPool& p_command_pool, uint32_t p_queue_family);
            // void _create_command_buffer();
            // void _create_sync_objects();
            // void record_command_buffer(VkCommandBuffer p_command_buffer, int32_t p_image_index);
            
            void _clean_swapchain();
            // void _create_vertex_buffer();
            // void _create_index_buffer(const vector<uint16_t>& p_indices);
            uint32_t _find_memory_type(uint32_t p_type_filter, VkMemoryPropertyFlags p_flags);
            
            

        private:
            vector<RenderDevice> _render_devices;
            #ifdef BK_DEBUG
            bool enableValidationLayer = true;
            #else
            bool enableValidationLayer = false;
            #endif

            

            vector<RenderDevice> devices;
            VkDebugUtilsMessengerEXT debugMessenger;
            VkInstance instance;
            int selected_device_index;
            VkDevice _device;
            vector<const char*> _validation_layers;
            VkPhysicalDevice _physical_device = VK_NULL_HANDLE;
            vector<VkPhysicalDevice> _physical_devices;
            VkSurfaceKHR _surface;
            
            VkSwapchainKHR _swapchain = VK_NULL_HANDLE;
            vector<VkImage> _swapchain_images;
            VkFormat _swapchain_format;
            VkExtent2D _swapchain_extent;
            vector<VkImageView> _image_views;
            // VkRenderPass _render_pass;
            vector<VkFramebuffer> _swapchain_frame_buffers;
            
    };
}

#endif