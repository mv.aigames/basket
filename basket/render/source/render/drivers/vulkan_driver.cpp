#include "vulkan_driver.hpp"

#include <core/logging.hpp>
#include <core/string.hpp>
#include <core/glfw.hpp>
#include <core/os.hpp>
#include <core/glfw.hpp>
#include <core/math/functions.hpp>
// #include <core/io/file.hpp>
#include <core/math/vector2.hpp>
#include <core/math/vector3.hpp>
#include <core/array.hpp>

#include <render/window/window.hpp>
#include <core/math/matrix44.hpp>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <core/dstream.hpp>




BK_DECLARE_LOG(render);





static VKAPI_ATTR VkBool32 VKAPI_CALL _basket_debug_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT,
    VkDebugUtilsMessageTypeFlagsEXT,
    const VkDebugUtilsMessengerCallbackDataEXT* p_data,
    void* /* User data */)
{
    BK_LOG(render, info, "Vulkan callback Message: {0}", p_data->pMessage);

    return VK_FALSE;
}

static VkResult _create_debug_utils_messenger_EXT(
    VkInstance instance,
    const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDebugUtilsMessengerEXT* pDebugMessenger)
{
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr) {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

static void _destroy_debug_utils_messenger_EXT(
    VkInstance instance,
    VkDebugUtilsMessengerEXT debugMessenger,
    const VkAllocationCallbacks* pAllocator)
{
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) {
        func(instance, debugMessenger, pAllocator);
    }
}
static basket::vector<char> __load_shader(const basket::string& p_path)
{
    basket::dstream d;
    // using namespace basket;
    // string path = p_path + ".spv";
    // File f(path, OpenMode::Read);
    // const vector<uint8_t>& content = f.raw_data();
    // return vector<char>(reinterpret_cast<char*>(content.data()), content.size());
    return {};
}



void _initialize_debug_message_create_info(VkDebugUtilsMessengerCreateInfoEXT& p_info)
{
    p_info = {};

    p_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    p_info.messageSeverity =    VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                                VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

    p_info.messageType =    VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                            VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

    p_info.pfnUserCallback = _basket_debug_callback;
}

namespace basket
{
    struct _UniformBufferObject
    {
        glm::mat4 model;
        glm::mat4 view;
        glm::mat4 projection;

    };

    // struct _UniformBufferObject
    // {
    //     Matrix44 model;
    //     Matrix44 view;
    //     Matrix44 projection;

    // };

    struct _Vertex
    {
        Vector2 position;
        Vector3 color;

        static VkVertexInputBindingDescription get_binding_description()
        {
            VkVertexInputBindingDescription binding_description{};
            binding_description.binding = 0;
            binding_description.stride = sizeof(_Vertex);
            binding_description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            
            return binding_description;
        }

        static array<VkVertexInputAttributeDescription, 2> get_attribute_descriptions()
        {
            array<VkVertexInputAttributeDescription, 2> attributes{};
            attributes[0].binding = 0;
            attributes[0].location = 0;
            attributes[0].format = VK_FORMAT_R32G32_SFLOAT;
            attributes[0].offset = offsetof(_Vertex, position);

            attributes[1].binding = 0;
            attributes[1].location = 1;
            attributes[1].format = VK_FORMAT_R32G32B32_SFLOAT;
            attributes[1].offset = offsetof(_Vertex, color);

            return attributes;
        }
    };

    static const vector<_Vertex> _sample_vertices {
        {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
        {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
        {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
        {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}}
    };

    static const vector<uint16_t> _sample_indices { 0, 1, 2, 2, 3, 0 };

    class DummyRender: IVulkanRecordCommand
    {
        private:
            VulkanDriver* _driver;

            float _time = 0.0f;

        private:
            VkBuffer _sample_vertex_buffer;
            VkDeviceMemory _sample_vertex_buffer_memory;
            VkBuffer _sample_indices_buffer;
            VkDeviceMemory _sample_indices_buffer_memory;
            VkDescriptorPool _sample_descriptor_pool;
            vector<VkDescriptorSet> _sample_descriptor_sets;
            VkPipelineLayout _pipeline_layout;
            VkPipeline _pipeline;
            VkDescriptorSetLayout _sample_descriptor_set_layout;
            VkPipelineLayout _sample_pipeline_layout;

            vector<VkBuffer> _sample_uniform_buffers;
            vector<VkDeviceMemory> _sample_uniform_buffers_memory;
            vector<void*> _sample_uniform_buffers_mapped;
            vector<VkCommandBuffer> _command_buffers;
            vector<VkSemaphore> _image_available_semaphores;
            vector<VkSemaphore> _render_finished_semaphores;
            vector<VkFence> _in_flight_fences;
            VkCommandPool _command_pool;
            uint32_t command_pool_index;
            // uint32_t command__index

        private:
            
            void _create_graphic_pipeline()
            {
                vector<char> vertex_code = __load_shader("render/tests/triangle.vert");
                vector<char> fragment_code = __load_shader("render/tests/triangle.frag");

                VkShaderModule vertex_shader = _driver->create_shader_module(vertex_code);
                VkShaderModule fragment_shader = _driver->create_shader_module(fragment_code);

                VkPipelineShaderStageCreateInfo vertex_stage_info{};
                vertex_stage_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
                vertex_stage_info.stage = VK_SHADER_STAGE_VERTEX_BIT;
                vertex_stage_info.module = vertex_shader;
                vertex_stage_info.pName = "main";

                VkPipelineShaderStageCreateInfo fragment_stage_info{};
                fragment_stage_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
                fragment_stage_info.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
                fragment_stage_info.module = fragment_shader;
                fragment_stage_info.pName = "main";

                VkPipelineShaderStageCreateInfo stages[] = {vertex_stage_info, fragment_stage_info};


                vector<VkDynamicState> dynamic_states{VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH, VK_DYNAMIC_STATE_SCISSOR};

                VkPipelineDynamicStateCreateInfo dynamic_state_info{};
                dynamic_state_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
                dynamic_state_info.dynamicStateCount = (uint32_t)dynamic_states.size();
                dynamic_state_info.pDynamicStates = dynamic_states.data();


                VkPipelineVertexInputStateCreateInfo vertex_input_info{};

                VkVertexInputBindingDescription binding_description = _Vertex::get_binding_description();
                array<VkVertexInputAttributeDescription, 2> attributes = _Vertex::get_attribute_descriptions();

                vertex_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
                vertex_input_info.vertexAttributeDescriptionCount = (uint32_t)attributes.size();
                vertex_input_info.vertexBindingDescriptionCount = 1;
                vertex_input_info.pVertexAttributeDescriptions = attributes.data();
                vertex_input_info.pVertexBindingDescriptions = &binding_description;

                VkPipelineInputAssemblyStateCreateInfo input_assembly_info{};
                input_assembly_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
                input_assembly_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
                input_assembly_info.primitiveRestartEnable = VK_FALSE;

                VkViewport viewport{};
                viewport.x = 0.0f;
                viewport.y = 0.0f;
                viewport.width = (float)_driver->_get_extent().width;
                viewport.height = (float)_driver->_get_extent().height;
                viewport.minDepth = 0.0f;
                viewport.maxDepth = 1.0f;

                VkRect2D scissor{};
                scissor.offset = {0, 0};
                scissor.extent = _driver->_get_extent();

                VkPipelineViewportStateCreateInfo viewport_info{};
                viewport_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
                viewport_info.viewportCount = 1;
                viewport_info.pViewports = &viewport;
                viewport_info.scissorCount = 1;
                viewport_info.pScissors = &scissor;

                VkPipelineRasterizationStateCreateInfo rasterization_info{};
                rasterization_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
                rasterization_info.depthClampEnable = VK_FALSE;
                rasterization_info.rasterizerDiscardEnable = VK_FALSE;
                rasterization_info.polygonMode = VK_POLYGON_MODE_FILL;
                rasterization_info.lineWidth = 1.0f;
                rasterization_info.cullMode = VK_CULL_MODE_BACK_BIT;
                rasterization_info.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
                rasterization_info.depthBiasEnable = VK_FALSE;
                rasterization_info.depthBiasConstantFactor = 0.0f;
                rasterization_info.depthBiasClamp = 0.0f;
                rasterization_info.depthBiasSlopeFactor = 0.0f;

                VkPipelineMultisampleStateCreateInfo multisample_info{};
                multisample_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
                multisample_info.sampleShadingEnable = VK_FALSE;
                multisample_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
                multisample_info.minSampleShading = 1.0f; // Optional
                multisample_info.pSampleMask = nullptr; // optional
                multisample_info.alphaToCoverageEnable = VK_FALSE; // optional
                multisample_info.alphaToOneEnable = VK_FALSE; // optional


                VkPipelineColorBlendAttachmentState attach_state{};
                attach_state.colorWriteMask =   VK_COLOR_COMPONENT_R_BIT |
                                                VK_COLOR_COMPONENT_G_BIT |
                                                VK_COLOR_COMPONENT_B_BIT |
                                                VK_COLOR_COMPONENT_A_BIT;
                
                attach_state.blendEnable = VK_FALSE;
                attach_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
                attach_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
                attach_state.colorBlendOp = VK_BLEND_OP_ADD;
                attach_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
                attach_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
                attach_state.alphaBlendOp = VK_BLEND_OP_ADD;

                VkPipelineColorBlendStateCreateInfo blend_state_info{};
                blend_state_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
                blend_state_info.logicOpEnable = VK_FALSE;
                blend_state_info.logicOp = VK_LOGIC_OP_COPY; // optional
                blend_state_info.attachmentCount = 1;
                blend_state_info.pAttachments = &attach_state;
                blend_state_info.blendConstants[0] = 0.0f; // optional
                blend_state_info.blendConstants[1] = 0.0f; // optional
                blend_state_info.blendConstants[2] = 0.0f; // optional
                blend_state_info.blendConstants[3] = 0.0f; // optional


                VkPipelineLayoutCreateInfo pipe_layout_info{};

                pipe_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
                pipe_layout_info.setLayoutCount = 1;
                pipe_layout_info.pSetLayouts = &_sample_descriptor_set_layout;
                pipe_layout_info.pushConstantRangeCount = 0;
                pipe_layout_info.pPushConstantRanges = nullptr;

                VkResult result = vkCreatePipelineLayout(_driver->get_device(), &pipe_layout_info, nullptr, &_pipeline_layout);
                BK_VK_ASSERT(result == VK_SUCCESS, "FAiled to create Pipeline Layout");

                // _create_render_pass();

                VkGraphicsPipelineCreateInfo pipe_info{};
                pipe_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
                pipe_info.stageCount = 2;
                pipe_info.pStages = stages;
                pipe_info.pVertexInputState = &vertex_input_info;
                pipe_info.pInputAssemblyState = &input_assembly_info;
                pipe_info.pViewportState = &viewport_info;
                pipe_info.pRasterizationState = &rasterization_info;
                pipe_info.pMultisampleState = &multisample_info;
                pipe_info.pDepthStencilState = nullptr; // optional
                pipe_info.pColorBlendState = &blend_state_info;
                pipe_info.pDynamicState = &dynamic_state_info;
                pipe_info.layout = _pipeline_layout;
                pipe_info.renderPass = _driver->get_render_pass(0);
                pipe_info.subpass = 0;
                pipe_info.basePipelineHandle = VK_NULL_HANDLE; // Optional
                pipe_info.basePipelineIndex = -1; // Optional


                result = vkCreateGraphicsPipelines(_driver->get_device(), VK_NULL_HANDLE, 1, &pipe_info, nullptr, &_pipeline);
                BK_VK_ASSERT(result == VK_SUCCESS, "Failed to create Graphic Pipeline");

                vkDestroyShaderModule(_driver->get_device(), vertex_shader, nullptr);
                vkDestroyShaderModule(_driver->get_device(), fragment_shader, nullptr);
            }

            void __update_uniform(float p_delta)
            {
                // __acc += (p_delta * 33.0f);
                // __acc += 0.0001f;
                _time += 0.001f;
                // BK_LOG(render, info, "delta: {0}", static_cast<float>(p_delta));
                // BK_LOG(render, info, "_time: {0}", _time);
                // BK_LOG(render, info, "frames: {0}", p_delta);

                // _UniformBufferObject ubo;
                // ubo.model = bmath::rotated(Matrix44(1), __acc * bmath::deg_to_rad(90.0f), Vector3(0, 0, 1));
                // ubo.view = bmath::look_at(Vector3(2.0f, 2.0f, 2.0f), Vector3::zero, Vector3(0.0f, 0.0f, 1.0f));
                // float a = bmath::deg_to_rad(45.0f);
                // BK_LOG(render, info, "view \n{0}, {1}, {2}, {3},\n {4}, {5}, {6}, {7},\n {8}, {9}, {10}, {11},\n {12}, {13}, {14}, {15}", 
                // ubo.view[0][0], ubo.view[0][1], ubo.view[0][2], ubo.view[0][3],
                // ubo.view[1][0], ubo.view[1][1], ubo.view[1][2], ubo.view[1][3],
                // ubo.view[2][0], ubo.view[2][1], ubo.view[2][3], ubo.view[2][3],
                // ubo.view[3][0], ubo.view[3][1], ubo.view[3][3], ubo.view[3][3]);
                // BK_LOG(render, info, "angle: {0}", a);
                // BK_LOG(render, info, "aspect ratio: {0}", (float)_swapchain_extent.width / (float)_swapchain_extent.height);
                // ubo.projection = bmath::perspective(bmath::deg_to_rad(45.0f), (float)_swapchain_extent.width / (float)_swapchain_extent.height, 0.1f, 10.0f);
                // BK_LOG(render, info, "projection \n{0}, {1}, {2}, {3},\n {4}, {5}, {6}, {7},\n {8}, {9}, {10}, {11},\n {12}, {13}, {14}, {15}", 
                // ubo.projection[0][0], ubo.projection[0][1], ubo.projection[0][2], ubo.projection[0][3],
                // ubo.projection[1][0], ubo.projection[1][1], ubo.projection[1][2], ubo.projection[1][3],
                // ubo.projection[2][0], ubo.projection[2][1], ubo.projection[2][3], ubo.projection[2][3],
                // ubo.projection[3][0], ubo.projection[3][1], ubo.projection[3][3], ubo.projection[3][3]);
                // ubo.projection.row1.y *= -1;

                _UniformBufferObject ubo;
                // BK_LOG(render, info, "rot: {0}", __acc * glm::radians(90.0f));
                // BK_LOG(render, info, "delta: {0}", p_delta);
                ubo.model = glm::rotate(glm::mat4(1.0f), _time * glm::radians(90.0f), glm::vec3(0.0f ,0.0f, 1.0f));
                // ubo.model = glm::mat4(1);
                ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
                // BK_LOG(render, info, "view \n{0}, {1}, {2}, {3},\n {4}, {5}, {6}, {7},\n {8}, {9}, {10}, {11},\n {12}, {13}, {14}, {15}", 
                // ubo.view[0][0], ubo.view[0][1], ubo.view[0][2], ubo.view[0][3],
                // ubo.view[1][0], ubo.view[1][1], ubo.view[1][2], ubo.view[1][3],
                // ubo.view[2][0], ubo.view[2][1], ubo.view[2][3], ubo.view[2][3],
                // ubo.view[3][0], ubo.view[3][1], ubo.view[3][3], ubo.view[3][3]);
                ubo.projection = glm::perspective(glm::radians(45.0f), (float)_driver->_get_extent().width / (float)_driver->_get_extent().height, 0.1f, 10.0f);
                // BK_LOG(render, info, "projection \n{0}, {1}, {2}, {3},\n {4}, {5}, {6}, {7},\n {8}, {9}, {10}, {11},\n {12}, {13}, {14}, {15}", 
                // ubo.projection[0][0], ubo.projection[0][1], ubo.projection[0][2], ubo.projection[0][3],
                // ubo.projection[1][0], ubo.projection[1][1], ubo.projection[1][2], ubo.projection[1][3],
                // ubo.projection[2][0], ubo.projection[2][1], ubo.projection[2][3], ubo.projection[2][3],
                // ubo.projection[3][0], ubo.projection[3][1], ubo.projection[3][3], ubo.projection[3][3]);
                ubo.projection[1][1] *= -1;
                
                memcpy(_sample_uniform_buffers_mapped[_driver->get_current_frame()], &ubo, sizeof(ubo));
                // ubo.model = Matrix44::rotate_z(Matrix44(1.0f), __acc * 0.1f);
                // ubo.view = Matrix44::look_at(Vector3(2, 2, 2), Vector3::zero, Vector3(0, 0, 1));
                // ubo.projection = Matrix44::perspective()

                // _sample_uniform_buffers
            }

            void create_descriptor_set_layout()
            {
                VkDescriptorSetLayoutBinding uboLayoutBinding{};

                uboLayoutBinding.binding = 0;
                uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                uboLayoutBinding.descriptorCount = 1;
                uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
                uboLayoutBinding.pImmutableSamplers = nullptr;

                VkDescriptorSetLayoutCreateInfo create_info{};
                create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
                create_info.bindingCount = 1;
                create_info.pBindings = &uboLayoutBinding;

                VkResult result = vkCreateDescriptorSetLayout(_driver->get_device(), &create_info, nullptr, &_sample_descriptor_set_layout);
                BK_VK_ASSERT(result == VK_SUCCESS, "vkCreateDescriptorSetLayout failed");
            }

            void create_uniform_buffers()
            {
                VkDeviceSize size = sizeof(_UniformBufferObject);

                uint32_t mf = _driver->get_max_frames_in_flight();
                _sample_uniform_buffers.resize(mf);
                _sample_uniform_buffers_memory.resize(mf);
                _sample_uniform_buffers_mapped.resize(mf);

                for (int index = 0; index < mf; ++index)
                {
                    _driver->create_buffer(size,
                        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                        _sample_uniform_buffers[index],
                        _sample_uniform_buffers_memory[index]);

                        vkMapMemory(_driver->get_device(), _sample_uniform_buffers_memory[index], 0, size, 0, &_sample_uniform_buffers_mapped[index]);
                }
            }

            void create_descriptor_pool()
            {
                VkDescriptorPoolSize pool_size{};
                pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                pool_size.descriptorCount = static_cast<uint32_t>(_driver->get_max_frames_in_flight());

                VkDescriptorPoolCreateInfo create_info{};
                create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
                create_info.poolSizeCount = 1;
                create_info.pPoolSizes = &pool_size;
                create_info.maxSets = static_cast<uint32_t>(_driver->get_max_frames_in_flight());

                VkResult r = vkCreateDescriptorPool(_driver->get_device(), &create_info, nullptr, &_sample_descriptor_pool);
                BK_VK_ASSERT(r == VK_SUCCESS, "vkCreateDescriptorPool failed");
            }


            void create_descriptor_sets()
            {
                uint32_t mf = _driver->get_max_frames_in_flight();
                vector<VkDescriptorSetLayout> layouts(mf, _sample_descriptor_set_layout);

                VkDescriptorSetAllocateInfo info{};
                info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
                info.descriptorPool = _sample_descriptor_pool;
                info.descriptorSetCount = static_cast<uint32_t>(mf);
                info.pSetLayouts = layouts.data();

                _sample_descriptor_sets.resize(mf);
                VkResult r = vkAllocateDescriptorSets(_driver->get_device(), &info, _sample_descriptor_sets.data());
                BK_VK_ASSERT(r == VK_SUCCESS, "vkAllocateDescriptorSets failed");

                for (int index = 0; index < mf; index++)
                {
                    VkDescriptorBufferInfo buffer_info{};
                    buffer_info.buffer = _sample_uniform_buffers[index];
                    buffer_info.offset = 0;
                    buffer_info.range = sizeof(_UniformBufferObject);

                    VkWriteDescriptorSet descriptor_write{};
                    descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                    descriptor_write.dstSet = _sample_descriptor_sets[index];
                    descriptor_write.dstBinding = 0;
                    descriptor_write.dstArrayElement = 0;
                    descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                    descriptor_write.descriptorCount = 1;
                    descriptor_write.pBufferInfo = &buffer_info;
                    descriptor_write.pImageInfo = nullptr;
                    descriptor_write.pTexelBufferView = nullptr;

                    vkUpdateDescriptorSets(_driver->get_device(), 1, &descriptor_write, 0, nullptr);
                }
            }

            void record_command_buffer(VkCommandBuffer p_command_buffer, int32_t p_image_index)
            {
                

                // __update_uniform(0.001f);



                // vkResetCommandPool(get_device(), _get_command_pool(), 0);
                vkCmdBindPipeline(p_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, _pipeline);

                VkBuffer vertex_buffers[] = {_sample_vertex_buffer};
                VkDeviceSize offsets[] = {0};
                vkCmdBindVertexBuffers(p_command_buffer, 0, 1, vertex_buffers, offsets);
                vkCmdBindIndexBuffer(p_command_buffer, _sample_indices_buffer, 0, VK_INDEX_TYPE_UINT16);

                VkViewport viewport{};
                viewport.x = 0.0f;
                viewport.y = 0.0f;
                viewport.width = (uint32_t)_driver->_get_extent().width;
                viewport.height = (uint32_t)_driver->_get_extent().height;
                viewport.minDepth = 0.0f;
                viewport.maxDepth = 1.0f;

                vkCmdSetViewport(p_command_buffer, 0, 1, &viewport);

                VkRect2D scissor{};
                scissor.offset = {0, 0};
                scissor.extent = _driver->_get_extent();
                vkCmdSetScissor(p_command_buffer, 0, 1, &scissor);

                vkCmdBindDescriptorSets(p_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, _pipeline_layout, 0, 1, &_sample_descriptor_sets[_driver->get_current_frame()], 0, nullptr);
                // vkCmdDraw(p_command_buffer, static_cast<uint32_t>(_sample_vertices.size()), 1, 0, 0);
                vkCmdDrawIndexed(p_command_buffer, (uint32_t)_sample_indices.size(), 1, 0, 0, 0);

                

                // _driver->add_command_buffer_to_submit(p_command_buffer);
            }

            void create_vertex_buffer()
            {
                VkDeviceSize buffer_size = sizeof(_sample_vertices[0]) * _sample_vertices.size();
                VkBuffer staging_buffer;
                VkDeviceMemory staging_buffer_memory;

                _driver->create_buffer(buffer_size,
                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                    staging_buffer,
                    staging_buffer_memory);

                void *data;
                vkMapMemory(_driver->get_device(), staging_buffer_memory, 0, buffer_size, 0, &data);
                memcpy(data, _sample_vertices.data(), (size_t)buffer_size);
                vkUnmapMemory(_driver->get_device(), staging_buffer_memory);

                _driver->create_buffer(buffer_size,
                    VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                    _sample_vertex_buffer,
                    _sample_vertex_buffer_memory);

                _driver->copy_buffer(staging_buffer, _sample_vertex_buffer, buffer_size);

                vkDestroyBuffer(_driver->get_device(), staging_buffer, nullptr);
                vkFreeMemory(_driver->get_device(), staging_buffer_memory, nullptr);
            }
        
            void create_index_buffer(const vector<uint16_t>& p_indices)
            {
                VkDeviceSize buffer_size = sizeof(uint16_t) * p_indices.size();

                VkBuffer staging_buffer;
                VkDeviceMemory staging_buffer_memory;

                _driver->create_buffer(buffer_size,
                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                    staging_buffer,
                    staging_buffer_memory);
                
                void* data;
                vkMapMemory(_driver->get_device(), staging_buffer_memory, 0, buffer_size, 0, &data);
                memcpy(data, p_indices.data(), (size_t)buffer_size);
                vkUnmapMemory(_driver->get_device(), staging_buffer_memory);

                _driver->create_buffer(buffer_size,
                    VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                    _sample_indices_buffer,
                    _sample_indices_buffer_memory);

                _driver->copy_buffer(staging_buffer, _sample_indices_buffer, buffer_size);

                vkDestroyBuffer(_driver->get_device(), staging_buffer, nullptr);
                vkFreeMemory(_driver->get_device(), staging_buffer_memory, nullptr);
            }

            void create_sync_objects()
            {
                _image_available_semaphores.resize(_driver->get_max_frames_in_flight());
                _render_finished_semaphores.resize(_driver->get_max_frames_in_flight());
                _in_flight_fences.resize(_driver->get_max_frames_in_flight());
                VkSemaphoreCreateInfo s_info{};
                s_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

                VkFenceCreateInfo f_info{};
                f_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
                f_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;


                for (int index = 0; index < _driver->get_max_frames_in_flight(); ++index)
                {
                    if (vkCreateSemaphore(_driver->get_device(), &s_info, nullptr, &_image_available_semaphores[index]) != VK_SUCCESS ||
                        vkCreateSemaphore(_driver->get_device(), &s_info, nullptr, &_render_finished_semaphores[index]) != VK_SUCCESS ||
                        vkCreateFence(_driver->get_device(), &f_info, nullptr, &_in_flight_fences[index]) != VK_SUCCESS)
                    {
                        BK_VK_ASSERT(false, "Failed to create semaphore and fence");
                    }
                }
            }

            void create_command_buffer()
            {
                // VkCommandBufferAllocateInfo alloc_info{};
                // alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                // alloc_info.commandPool = _command_pool;
                // _command_buffers.resize(_driver->get_max_frames_in_flight());
                // alloc_info.commandBufferCount = static_cast<uint32_t>(_command_buffers.size());
                // alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

                // VkResult result = vkAllocateCommandBuffers(_driver->get_device(), &alloc_info, _command_buffers.data());
                // BK_VK_ASSERT(result == VK_SUCCESS, "Failed to allocate command buffer");
            }
        

            virtual void record(int32_t p_image_index, VkCommandBuffer p_command_buffer) override
            {
                __update_uniform(0.001f);
                // VkCommandBuffer command_buffer = _driver->get_command_buffer(command_pool_index, _driver->get_current_frame());
                record_command_buffer(p_command_buffer, p_image_index);
            }

            virtual bool one_time() const override
            {
                return false;
            }

        public:
            void init()
            {
                {
                    VkCommandPoolCreateInfo info{};
                    info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
                    info.queueFamilyIndex = _driver->get_graphic_queue_family_index();
                    info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
                    command_pool_index = _driver->create_command_pool(info);
                    _command_pool = _driver->get_command_pool(command_pool_index);
                }

                {
                    VkCommandBufferAllocateInfo info{};
                    info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                    info.commandBufferCount = _driver->get_max_frames_in_flight();
                    info.commandPool = _command_pool;
                    info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
                    
                    _driver->create_command_buffers(command_pool_index, info);
                }

                create_descriptor_set_layout();
                _create_graphic_pipeline();
                create_vertex_buffer();
                create_index_buffer(_sample_indices);
                create_uniform_buffers();
                create_descriptor_pool();
                create_descriptor_sets();

                _driver->add_graphic_record(this);
            }
        public:
            DummyRender(VulkanDriver* p_driver)
            {
                _driver = p_driver;
            }
    
            ~DummyRender()
            {
                clear();
            }

            void clear()
            {
                for (int index = 0; index < _driver->get_max_frames_in_flight(); ++index)
                {
                    vkDestroyBuffer(_driver->get_device(), _sample_uniform_buffers[index], nullptr);
                    vkFreeMemory(_driver->get_device(), _sample_uniform_buffers_memory[index], nullptr);
                }

                vkDestroyDescriptorPool(_driver->get_device(), _sample_descriptor_pool, nullptr);
                vkDestroyDescriptorSetLayout(_driver->get_device(), _sample_descriptor_set_layout, nullptr);


                vkDestroyBuffer(_driver->get_device(), _sample_vertex_buffer, nullptr);
                vkFreeMemory(_driver->get_device(), _sample_vertex_buffer_memory, nullptr);

                vkDestroyBuffer(_driver->get_device(), _sample_indices_buffer, nullptr);
                vkFreeMemory(_driver->get_device(), _sample_indices_buffer_memory, nullptr);
                vkDestroyPipeline(_driver->get_device(), _pipeline, nullptr);
                vkDestroyPipelineLayout(_driver->get_device(), _pipeline_layout, nullptr);
            }
    };

    DummyRender* dummy;

    void VulkanDriver::_pick_physical_device()
    {
        uint32_t count;
        vkEnumeratePhysicalDevices(instance, &count, nullptr);
        if (count == 0)
        {
            std::abort();
            return;
        }

        _physical_devices.resize(count);
        vkEnumeratePhysicalDevices(instance, &count, _physical_devices.data());

        for (const auto& device: _physical_devices)
        {
            if (_is_device_suitable(device))
            {
                _physical_device = device;
                break;
            }
        }

        if (_physical_device  == VK_NULL_HANDLE)
        {
            OS::abort();
        }
    }

    void VulkanDriver::_load_physical_devices()
    {
        uint32_t count;
        VkResult result = vkEnumeratePhysicalDevices(instance, &count, nullptr);
        BK_VK_ASSERT(result == VK_SUCCESS, "FAiled to enumerate Physical Devices");

        _physical_devices.resize(count);

        result = vkEnumeratePhysicalDevices(instance, &count, _physical_devices.data());
        BK_VK_ASSERT(result == VK_SUCCESS, "FAiled to enumerate Physical Devices");

        _render_devices.resize(count);

        for (int index = 0; index < _physical_devices.size(); index++)
        {
            VkPhysicalDevice pd = _physical_devices[index];
            VkPhysicalDeviceProperties p;
            vkGetPhysicalDeviceProperties(pd, &p);
            
            RenderDevice& rd = _render_devices[index];
            rd._id = index;
            rd.name = p.deviceName;

            BK_LOG(render, info, "Device name: {0}", p.deviceName);
        }
    }

    void VulkanDriver::_create_instance()
    {
        VkApplicationInfo app_info{};
        VkInstanceCreateInfo create_info{};

        app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app_info.pApplicationName = "Vulkan Module";
        app_info.pEngineName = "Vulkan Render Engine";
        app_info.apiVersion = VK_VERSION_1_3;
        app_info.applicationVersion = 1;

        
        vector<const char*> extensions = _get_required_extensions();
        

        if (enableValidationLayer && _check_layers())
        {
            std::abort();
        }

        VkDebugUtilsMessengerCreateInfoEXT debug_info{};

        _initialize_debug_message_create_info(debug_info);

        if (enableValidationLayer)
        {
            create_info.enabledLayerCount = _validation_layers.size();
            create_info.ppEnabledLayerNames = _validation_layers.data();
            create_info.pNext = &debug_info;
        }
        else
        {
            create_info.enabledLayerCount = 0;
            create_info.pNext = nullptr;
        }

        create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        create_info.pApplicationInfo = &app_info;
        create_info.enabledExtensionCount = extensions.size();
        create_info.ppEnabledExtensionNames = extensions.data();
        create_info.flags = 0;
        VkResult result;
        result = vkCreateInstance(&create_info, nullptr, &instance);
        
    }

    bool VulkanDriver::_check_layers()
    {
        vector<VkLayerProperties> available_layers;
        uint32_t count;

        vkEnumerateInstanceLayerProperties(&count, nullptr);
        available_layers.resize(count);
        vkEnumerateInstanceLayerProperties(&count, available_layers.data());

        for (const auto& layer : available_layers)
        {
            bool found = false;
            for (const char* layer_name: _validation_layers)
            {
                if (strcmp(layer.layerName, layer_name) == 0)
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                return false;
            }
        }
        return true;
    }

    bool VulkanDriver::_is_device_suitable(const VkPhysicalDevice& p_device)
    {
        VkPhysicalDeviceProperties device_properties;
        VkPhysicalDeviceFeatures device_features;

        vkGetPhysicalDeviceProperties(p_device, &device_properties);
        vkGetPhysicalDeviceFeatures(p_device, &device_features);

        // int index = _find_queue_family_by_flags(_physical_device, VK_QUEUE_GRAPHICS_BIT);
        // vkGetPhysicalDeviceSurfaceSupportKHR(p_device, index, _surface, )

        return device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && device_features.geometryShader;
    }

    void VulkanDriver::_create_logical_device()
    {
        int32_t graphic_index = _find_queue_family_by_flags(_physical_device, VK_QUEUE_GRAPHICS_BIT);
        int32_t transfer_index = _find_queue_family_by_flags(_physical_device, VK_QUEUE_TRANSFER_BIT);

        float queue_priority = 1.0f;

        VkDeviceQueueCreateInfo graphic_create_info{};

        graphic_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        graphic_create_info.queueFamilyIndex = graphic_index;
        graphic_create_info.queueCount = 1;
        graphic_create_info.pQueuePriorities = &queue_priority;

        VkDeviceQueueCreateInfo transfer_create_info{};

        transfer_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        transfer_create_info.queueFamilyIndex = transfer_index;
        transfer_create_info.queueCount = 1;
        transfer_create_info.pQueuePriorities = &queue_priority;

        VkPhysicalDeviceFeatures features{};

        VkDeviceCreateInfo device_info{};

        vector<const char*> device_extensions;
        device_extensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

        device_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        vector<VkDeviceQueueCreateInfo> create_infos = {graphic_create_info, transfer_create_info};
        device_info.pQueueCreateInfos = create_infos.data();
        device_info.queueCreateInfoCount = (uint32_t)create_infos.size();
        device_info.pEnabledFeatures = &features;
        device_info.enabledExtensionCount = 1;
        device_info.ppEnabledExtensionNames = device_extensions.data();
        
        

        VkResult result = vkCreateDevice(_physical_device, &device_info, nullptr, &_device);
        if (result != VK_SUCCESS)
        {
            BK_LOG(render, info, "Failed to create device: Value output: {0}", result);
            OS::abort();
        }

        vkGetDeviceQueue(_device, graphic_index, 0, &_graphic_queue);
        vkGetDeviceQueue(_device, transfer_index, 0, &_transfer_queue);
    }

    _SwapchainSupportDetails VulkanDriver::_query_swapchain_support(VkPhysicalDevice p_device)
    {
        _SwapchainSupportDetails details;

        VkResult result;
        result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(p_device, _surface, &details.capabilities);
        BK_VK_ASSERT(result == VK_SUCCESS, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR has failed");

        uint32_t present_mode_count;
        vkGetPhysicalDeviceSurfacePresentModesKHR(p_device ,_surface, &present_mode_count, nullptr);

        if (present_mode_count != 0)
        {
            details.present_modes.resize(present_mode_count);
            vkGetPhysicalDeviceSurfacePresentModesKHR(p_device, _surface, &present_mode_count, details.present_modes.data());
        }

        uint32_t formats_count;
        vkGetPhysicalDeviceSurfaceFormatsKHR(p_device ,_surface, &formats_count, nullptr);

        if (present_mode_count != 0)
        {
            details.formats.resize(formats_count);
            vkGetPhysicalDeviceSurfaceFormatsKHR(p_device ,_surface, &formats_count, details.formats.data());
        }

        return details;
    }

    VkSurfaceFormatKHR VulkanDriver::_choose_swapchain_surface_format(const vector<VkSurfaceFormatKHR>& p_formats)
    {
        for (const auto& format : p_formats)
        {
            if (format.format == VK_FORMAT_B8G8R8A8_SRGB && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
            {
                return format;
            }
        }

        return p_formats[0];
    }

    VkPresentModeKHR VulkanDriver::_choose_swapchain_present_mode(const vector<VkPresentModeKHR>& p_presents)
    {
        for (const auto& p : p_presents)
        {
            if (p == VK_PRESENT_MODE_MAILBOX_KHR)
            {
                return p;
            }
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    VkExtent2D VulkanDriver::_choose_swapchain_extents(const VkSurfaceCapabilitiesKHR& p_capabilities)
    {
        // return p_capabilities.currentExtent;

        int width, height;
        glfwGetFramebufferSize((GLFWwindow*)_window->get_window_native(), &width, &height);

        VkExtent2D real_size {
            static_cast<uint32_t>(width),
            static_cast<uint32_t>(height)
        };

        real_size.width = bmath::clamp(real_size.width, p_capabilities.minImageExtent.width, p_capabilities.maxImageExtent.width);
        real_size.height = bmath::clamp(real_size.height, p_capabilities.minImageExtent.height, p_capabilities.maxImageExtent.height);

        return real_size;
    }

    void VulkanDriver::present()
    {
        BK_VK_ASSERT(_window, "Windows is necessary");
        _frames_data.resize(_max_frames_in_flight);
        _create_surface();
        _create_swapchain();
        _create_image_views();
        create_render_pass();
        // create_descriptor_set_layout();
        // _create_graphic_pipeline();
        _create_framebuffers();
        // _create_command_pool(_command_pool, VK_QUEUE_GRAPHICS_BIT);
        // _create_command_pool(_editor_command_pool, VK_QUEUE_GRAPHICS_BIT);
        _create_command_pool(_transfer_command_pool, VK_QUEUE_TRANSFER_BIT);
        // _create_vertex_buffer();
        // _create_index_buffer(_sample_indices);
        // create_uniform_buffers();
        // _create_descriptor_pool();
        // _create_descriptor_sets();
        // _create_command_buffer();
        // _create_sync_objects();

        dummy = new DummyRender(this);
        dummy->init();

    }

    void VulkanDriver::_clean_swapchain()
    {
        for (auto& framebuffer: _swapchain_frame_buffers)
        {
            vkDestroyFramebuffer(_device, framebuffer, nullptr);
        }
        for (size_t index = 0; index < _image_views.size(); ++index)
        {
            vkDestroyImageView(_device, _image_views[index], nullptr);
        }

        vkDestroySwapchainKHR(_device, _swapchain, nullptr);
    }

    void VulkanDriver::_recreate_swapchain()
    {
        vkDeviceWaitIdle(_device);

        _create_swapchain();
        _create_image_views();
        _create_framebuffers();
    }

    void VulkanDriver::_create_swapchain()
    {
        _SwapchainSupportDetails details = _query_swapchain_support(_physical_device);
        VkSurfaceFormatKHR format = _choose_swapchain_surface_format(details.formats);
        VkPresentModeKHR present_mode = _choose_swapchain_present_mode(details.present_modes);

        uint32_t image_count = details.capabilities.minImageCount + 1;

        // if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
        //     imageCount = swapChainSupport.capabilities.maxImageCount;
        // }


        VkSwapchainCreateInfoKHR swap_create_info{};
        swap_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        swap_create_info.surface = _surface;
        swap_create_info.minImageCount = image_count;
        swap_create_info.imageFormat = format.format;
        swap_create_info.imageColorSpace = format.colorSpace;
        swap_create_info.imageExtent = _choose_swapchain_extents(details.capabilities);
        swap_create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        swap_create_info.preTransform = details.capabilities.currentTransform;
        swap_create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        swap_create_info.presentMode = present_mode;
        swap_create_info.queueFamilyIndexCount = 0;
        swap_create_info.pQueueFamilyIndices = nullptr;
        swap_create_info.clipped = VK_TRUE;
        swap_create_info.oldSwapchain = _swapchain;
        swap_create_info.imageArrayLayers = 1; // More are for stereoscopic 3D application
        swap_create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

        _swapchain_format = format.format;
        _swapchain_extent = swap_create_info.imageExtent;

        VkResult result = vkCreateSwapchainKHR(_device, &swap_create_info, nullptr, &_swapchain);
        BK_VK_ASSERT(result == VK_SUCCESS, "Failed to create swapchain");


        vkGetSwapchainImagesKHR(_device, _swapchain, &image_count, nullptr);
        _swapchain_images.resize(image_count);
        vkGetSwapchainImagesKHR(_device, _swapchain, &image_count, _swapchain_images.data());
    }

    void VulkanDriver::_create_image_views()
    {
        _image_views.resize(_swapchain_images.size());

        for (size_t index = 0; index < _image_views.size(); ++index)
        {
            VkImageViewCreateInfo create_info{};

            create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            create_info.image = _swapchain_images[index];
            create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
            create_info.format = _swapchain_format;
            create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            create_info.subresourceRange.baseMipLevel = 0;
            create_info.subresourceRange.levelCount = 1;
            create_info.subresourceRange.baseArrayLayer = 0;
            create_info.subresourceRange.layerCount = 1;

            VkResult result = vkCreateImageView(_device, &create_info, nullptr, &_image_views[index]);
            BK_VK_ASSERT(result == VK_SUCCESS, "Faiuled to create ImageView");
        }
    }

    VkShaderModule VulkanDriver::create_shader_module(const vector<char>& p_code)
    {
        VkShaderModuleCreateInfo info{};

        info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        info.codeSize = p_code.size();
        info.pCode = reinterpret_cast<const uint32_t*>(p_code.data());

        VkShaderModule shader_module;

        VkResult result = vkCreateShaderModule(get_device(), &info, nullptr, &shader_module);
        BK_VK_ASSERT(result == VK_SUCCESS, "Failed to create shader module");

        return shader_module;
    }

    

    uint32_t VulkanDriver::create_render_pass()
    {
        VkRenderPass rp;
        _create_render_pass(rp);
        _render_passes.push_back(rp);
        return _render_passes.size() - 1;
    }

    bool VulkanDriver::_create_render_pass(VkRenderPass& p_render_pass)
    {
        VkAttachmentDescription color_attachment{};
        color_attachment.format = _swapchain_format;
        color_attachment.samples = VK_SAMPLE_COUNT_1_BIT;
        color_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        color_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        color_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        color_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        color_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        color_attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        VkAttachmentReference color_attach_ref{};
        color_attach_ref.attachment = 0;
        color_attach_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        

        VkSubpassDescription subpass{};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &color_attach_ref;

        VkSubpassDependency dependency{};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;

        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;

        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;


        VkRenderPassCreateInfo render_pass_info{};
        render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        render_pass_info.attachmentCount = 1;
        render_pass_info.pAttachments = &color_attachment;
        render_pass_info.subpassCount = 1;
        render_pass_info.pSubpasses = &subpass;
        render_pass_info.dependencyCount = 1;
        render_pass_info.pDependencies = &dependency;

        VkResult result = vkCreateRenderPass(_device, &render_pass_info, nullptr, &p_render_pass);
        BK_VK_ASSERT(result == VK_SUCCESS, "Failed to create render pass");
        return true;
    }

    void VulkanDriver::_create_framebuffers()
    {
        _swapchain_frame_buffers.resize(_image_views.size());

        for (int index = 0; index < _image_views.size(); ++index)
        {
            VkImageView attachments[] = {
                _image_views[index]
            };

            VkFramebufferCreateInfo frame_info{};
            frame_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            frame_info.renderPass = get_render_pass(0);
            frame_info.attachmentCount = 1;
            frame_info.pAttachments = attachments;
            frame_info.width = _swapchain_extent.width;
            frame_info.height = _swapchain_extent.height;
            frame_info.layers = 1;

            VkResult result = vkCreateFramebuffer(_device, &frame_info, nullptr, &_swapchain_frame_buffers[index]);
            BK_VK_ASSERT(result == VK_SUCCESS, "Cannot create framebuffer.");
        }
    }

    void VulkanDriver::_create_command_pool(VkCommandPool& p_command_pool, uint32_t p_queue_family)
    {
        int index = _find_queue_family_by_flags(_physical_device, p_queue_family);

        VkCommandPoolCreateInfo pool_info{};
        pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        pool_info.queueFamilyIndex = index;

        VkResult result = vkCreateCommandPool(_device, &pool_info, nullptr, &p_command_pool);
        BK_VK_ASSERT(result == VK_SUCCESS, "Failed to create command pool");
    }

    uint32_t VulkanDriver::create_command_pool(VkCommandPoolCreateInfo& p_info)
    {
        _CommandGroup& cg = _command_groups.emplace_back();
        VkResult result = vkCreateCommandPool(get_device(), &p_info, nullptr, &cg._command_pool);
        BK_VK_ASSERT(result == VK_SUCCESS, "vkCreateCommandPool from create_command_pool has failed");
        return _command_groups.size() - 1;
    }

    VkCommandPool VulkanDriver::get_command_pool(uint32_t p_index)
    {
        return _command_groups[p_index]._command_pool;
    }

    VkCommandBuffer VulkanDriver::get_command_buffer(uint32_t p_pool_index, uint32_t p_buffer_index)
    {
        return _command_groups[p_pool_index]._command_buffers[p_buffer_index];
    }

    uint32_t VulkanDriver::_find_memory_type(uint32_t p_type_filter, VkMemoryPropertyFlags p_properties_flags)
    {
        VkPhysicalDeviceMemoryProperties memory_properties;
        vkGetPhysicalDeviceMemoryProperties(_physical_device, &memory_properties);

        for (uint32_t index = 0; index < memory_properties.memoryTypeCount; index++)
        {
            if ((p_type_filter & (1 << index)) && (memory_properties.memoryTypes[index].propertyFlags & p_properties_flags) == p_properties_flags)
            {
                return index;
            }
        }

        BK_VK_ASSERT(false, "Failed to find_memory_type");
        return (uint32_t)-1;
    }

    RenderResult VulkanDriver::create_buffer(    VkDeviceSize p_size, VkBufferUsageFlags p_usage,
                                VkMemoryPropertyFlags p_properties,
                                VkBuffer& p_buffer, VkDeviceMemory& p_buffer_memory)
    {
        VkBufferCreateInfo buffer_info{};
        buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        buffer_info.size = p_size;
        buffer_info.usage = p_usage;
        buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        VkResult result = vkCreateBuffer(_device, &buffer_info, nullptr, &p_buffer);
        if (result != VK_SUCCESS)
        {
            BK_LOG(render, error, "Error creating buffer {0}", result);
            return RenderResult::create_buffer_failed;
        }

        VkMemoryRequirements memory_requirements;
        vkGetBufferMemoryRequirements(_device, p_buffer, &memory_requirements);

        VkMemoryAllocateInfo alloc_info{};

        alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        alloc_info.allocationSize = memory_requirements.size;
        alloc_info.memoryTypeIndex = _find_memory_type(memory_requirements.memoryTypeBits, p_properties);

        result = vkAllocateMemory(_device, &alloc_info, nullptr, &p_buffer_memory);
        // BK_VK_ASSERT(result == VK_SUCCESS, "Failed to vkAllocateMemory");
        if (result != VK_SUCCESS)
        {
            BK_LOG(render, error, "vkAllocateMemory Error Code {0}", result);
            BK_VK_ASSERT(false, "vkAllocateMemory");
            return RenderResult::allocate_memory_failed;
        }

        result = vkBindBufferMemory(_device, p_buffer, p_buffer_memory, 0);
        // BK_VK_ASSERT(result == VK_SUCCESS, "Failed to vkBindBufferMemory");
        if (result != VK_SUCCESS)
        {
            BK_LOG(render, error, "vkBindBufferMemory Error Code {0}", result);
            BK_VK_ASSERT(false, "vkAllocateMemory");
            return RenderResult::bind_buffer_memory;
        }

        return RenderResult::success;
    }

    void VulkanDriver::copy_buffer(VkBuffer p_from, VkBuffer p_to, VkDeviceSize p_size)
    {
        VkCommandBufferAllocateInfo alloc_info{};
        alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        alloc_info.commandPool = _transfer_command_pool;
        alloc_info.commandBufferCount = 1;

        VkCommandBuffer command_buffer;
        VkResult result = vkAllocateCommandBuffers(_device, &alloc_info, &command_buffer);
        BK_VK_ASSERT(result == VK_SUCCESS, "Error copy_buffer vkAllocateCommandBuffers");

        VkCommandBufferBeginInfo begin_info{};
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(command_buffer, &begin_info);

        VkBufferCopy copy_region{};
        copy_region.srcOffset = 0;
        copy_region.dstOffset = 0;
        copy_region.size = p_size;

        vkCmdCopyBuffer(command_buffer, p_from, p_to, 1, &copy_region);

        vkEndCommandBuffer(command_buffer);

        VkSubmitInfo submit_info{};
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &command_buffer;
        vkQueueSubmit(_transfer_queue, 1, &submit_info, VK_NULL_HANDLE);
        vkQueueWaitIdle(_transfer_queue);

        vkFreeCommandBuffers(_device, _transfer_command_pool, 1, &command_buffer);
    }

    void VulkanDriver::create_command_buffers(uint32_t p_command_pool_index, VkCommandBufferAllocateInfo& p_info)
    {
        vector<VkCommandBuffer>& buffers = _command_groups[p_command_pool_index]._command_buffers;
        buffers.resize(p_info.commandBufferCount);
        VkResult result = vkAllocateCommandBuffers(get_device(), &p_info, buffers.data());
        BK_VK_ASSERT(result == VK_SUCCESS, "Failed to allocate command buffer");
    }

    void VulkanDriver::_graphics_records(int32_t p_image_index)
    {
        VkCommandBuffer command_buffer = get_command_buffer(0, get_current_frame());
        vkResetCommandBuffer(command_buffer, 0);
        VkCommandBufferBeginInfo begin_info{};
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin_info.flags = 0; // optional
        begin_info.pInheritanceInfo = nullptr;

        VkResult result = vkBeginCommandBuffer(command_buffer, &begin_info);
        BK_VK_ASSERT(result == VK_SUCCESS, "Failed to Begin command buffer");

        VkRenderPassBeginInfo render_pass_info{};
        render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        render_pass_info.renderPass = get_render_pass(0);
        render_pass_info.framebuffer = get_framebuffer(p_image_index);
        render_pass_info.renderArea.offset = {0, 0};
        render_pass_info.renderArea.extent = _get_extent();

        VkClearValue clear_color = {{{0.0f, 0.0f, 0.0f, 1.0f}}};

        render_pass_info.pClearValues = &clear_color;
        render_pass_info.clearValueCount = 1;

        vkCmdBeginRenderPass(command_buffer, &render_pass_info, VK_SUBPASS_CONTENTS_INLINE);

        

        for (IVulkanRecordCommand* r: _graphic_records)
        {
            r->record(p_image_index, command_buffer);
        }

        vkCmdEndRenderPass(command_buffer);

        result = vkEndCommandBuffer(command_buffer);
        BK_VK_ASSERT(result == VK_SUCCESS, "Failed to End command buffer");

        add_command_buffer_to_submit(command_buffer);
    }

    void VulkanDriver::add_graphic_record(IVulkanRecordCommand* p_record)
    {
        _graphic_records.push_back(p_record);
    }

    

    int32_t VulkanDriver::_find_queue_family_by_flags(VkPhysicalDevice p_device, uint32_t p_flags)
    {
        int32_t i = -1;

        uint32_t count;
        vkGetPhysicalDeviceQueueFamilyProperties(p_device, &count, nullptr);
        vector<VkQueueFamilyProperties> families;
        families.resize(count);

        vkGetPhysicalDeviceQueueFamilyProperties(p_device, &count, families.data());

        for(uint32_t index = 0; index < count; ++index)
        {
            if (families[index].queueFlags & p_flags)
            {
                i = index;
            }
        }

        return i;
    }

    void VulkanDriver::_create_surface()
    {
        // VkResult result = glfwCreateWindowSurface(instance, (GLFWwindow*)_window->get_window_native(), nullptr, &_surface);
        // BK_VK_ASSERT(result == VK_SUCCESS, "Error creating Vulkan surface");

        VkWin32SurfaceCreateInfoKHR khr_create_info{};
        khr_create_info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
        GLFWwindow* w = (GLFWwindow*)_window->get_window_native();
        khr_create_info.hwnd = glfwGetWin32Window(w);
        khr_create_info.hinstance = GetModuleHandle(nullptr);

        VkResult result = vkCreateWin32SurfaceKHR(instance, &khr_create_info, nullptr, &_surface);
        BK_VK_ASSERT(result == VK_SUCCESS, "Error creating Vulkan surface");
    }

    vector<const char*> VulkanDriver::_get_required_extensions() const
    {
        uint32_t count;
        const char** extensions;
        extensions = glfwGetRequiredInstanceExtensions(&count);

        vector<const char*> e(extensions, extensions + count);

        if (enableValidationLayer)
        {
            e.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        return e;
    }

    void VulkanDriver::_setup_debug_callback()
    {
        if (!enableValidationLayer)
        {
            return;
        }

        VkDebugUtilsMessengerCreateInfoEXT create_info{};

        _initialize_debug_message_create_info(create_info);

        create_info.pUserData = this;

        VkResult result = _create_debug_utils_messenger_EXT(instance, &create_info, nullptr, &debugMessenger);
        if (result != VK_SUCCESS)
        {
            std::abort();
        }
        
    }

    
    uint32_t VulkanDriver::create_fences(uint32_t p_amount) // BROKEN
    {
        VkFenceCreateInfo info{};
        info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        _fences.resize(p_amount);

        for (int index = 0; index < p_amount; index++)
        {
            VkResult r = vkCreateFence(get_device(), &info, nullptr, &_fences[index]);
        }

        return 0;
    }

    uint32_t VulkanDriver::create_semaphores(uint32_t p_amount)
    {
        VkSemaphoreCreateInfo info{};
        info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        vector<VkSemaphore> ss;
        ss.resize(p_amount);
        for (int index = 0; index < p_amount; index++)
        {
            VkResult r = vkCreateSemaphore(get_device(), &info, nullptr, &ss[index]);
            BK_VK_ASSERT(r == VK_SUCCESS, "vkCreateSemaphore failed");
        }

        _semaphores.push_back(ss);

        return _semaphores.size() - 1;
    }
    
    

    void VulkanDriver::draw(float p_delta)
    {
        
        // if (_command_buffers_to_submit.size() == 0)
        // {
        //     return;
        // }

        VkResult result;
        result = vkWaitForFences(get_device(), 1, &get_fence(get_current_frame()), VK_TRUE, UINT64_MAX);
        BK_VK_ASSERT(result == VK_SUCCESS, "Failed vkWaitForFences");


        uint32_t image_index;
        result = vkAcquireNextImageKHR(
                    get_device(),
                    _get_swapchain(),
                    UINT64_MAX,
                    get_frame_semaphore(0, get_current_frame()),
                    VK_NULL_HANDLE, &image_index);

        if (result == VK_ERROR_OUT_OF_DATE_KHR)
        {
            _recreate_swapchain();
            return;
        }
        else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
        {
            BK_LOG(render, fatal, "vkAcquireNextImageKHR Error code: {0} ", result);
            BK_VK_ASSERT(false, "Failed to submit queue");
        }
        
        result = vkResetFences(get_device(), 1, &get_fence(get_current_frame()));
        BK_VK_ASSERT(result == VK_SUCCESS, "Failed vkResetFences");



        _graphics_records(image_index);
        
        // _one_time_records.clear();

        VkSubmitInfo submit_info{};
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore wait_semaphores[] = {get_frame_semaphore(0, get_current_frame())};
        VkPipelineStageFlags wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
        submit_info.waitSemaphoreCount = 1;
        submit_info.pWaitSemaphores = wait_semaphores;
        submit_info.pWaitDstStageMask = wait_stages;
        submit_info.commandBufferCount = _command_buffers_to_submit.size();
        submit_info.pCommandBuffers = _command_buffers_to_submit.data();
        // submit_info.commandBufferCount = 0;
        // submit_info.pCommandBuffers = nullptr;
        
        VkResult success;
        // // VkSemaphore signal_semaphores[] = {_render_finished_semaphores[_driver->get_current_frame()]};
        VkSemaphore signal_semaphores[] = {get_frame_semaphore(1, get_current_frame())};
        submit_info.signalSemaphoreCount = 1;
        submit_info.pSignalSemaphores = signal_semaphores;

        success = vkQueueSubmit(_graphic_queue, 1, &submit_info, get_fence(get_current_frame()));
        BK_VK_ASSERT(success == VK_SUCCESS, "Failed to submit queue");



        VkPresentInfoKHR present_info{};
        present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

        present_info.waitSemaphoreCount = 1;
        present_info.pWaitSemaphores = signal_semaphores;

        VkSwapchainKHR swapchains[] = {_swapchain};
        present_info.swapchainCount = 1;
        present_info.pSwapchains = swapchains;
        present_info.pImageIndices = &image_index;
        present_info.pResults = nullptr;



        success = vkQueuePresentKHR(_graphic_queue, &present_info);

        _command_buffers_to_submit.clear();
        // BK_VK_ASSERT(success == VK_SUCCESS, "Failed to present queue KHR");
        if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
        {
            _recreate_swapchain();
        }
        else if (result != VK_SUCCESS)
        {
            BK_LOG(render, fatal, "vkQueuePresentKHR Error code: {0} ", result);
            BK_VK_ASSERT(false, "Failed to submit queue");
        }

        // BK_LOG(render, info, "drawing");

        _current_frame = (_current_frame + 1) % get_max_frames_in_flight();
    }


    VulkanDriver& VulkanDriver::get()
    {
        static VulkanDriver* instance;
        if (!instance)
        {
            instance = new VulkanDriver;
            instance->instance = nullptr;
        }

        return *instance;
    }

    void VulkanDriver::init()
    {
        _current_frame = 0;
        _validation_layers.push_back("VK_LAYER_KHRONOS_validation");
        _create_instance();
        _setup_debug_callback();
        _load_physical_devices();
        // _pick_physical_device();
        use_device(_render_devices[0]);
        _create_logical_device();
        create_fences(get_max_frames_in_flight());
        create_semaphores(get_max_frames_in_flight());
        create_semaphores(get_max_frames_in_flight());
    }


    void VulkanDriver::stop()
    {
        clean();
    }

    void VulkanDriver::clean()
    {
        vkDeviceWaitIdle(_device);

        _clean_swapchain();

        

        if (enableValidationLayer)
        {
            _destroy_debug_utils_messenger_EXT(instance, debugMessenger, nullptr);
        }

        delete dummy;

        for(vector<VkSemaphore>& ss: _semaphores)
        {
            for (VkSemaphore s: ss)
            {
                vkDestroySemaphore(get_device(), s, nullptr);
            }
        }

        
        for (VkFence f: _fences)
        {
            vkDestroyFence(get_device(), f, nullptr);
        }

        for (const _CommandGroup& cg: _command_groups)
        {
            vkDestroyCommandPool(get_device(), cg._command_pool, nullptr);
        }

        // vkDestroyCommandPool(_device, _command_pool, nullptr);
        vkDestroyCommandPool(_device, _transfer_command_pool, nullptr);


        
        for (VkRenderPass p: _render_passes)
        {
            vkDestroyRenderPass(_device, p, nullptr);
        }

        vkDestroySurfaceKHR(instance, _surface, nullptr);
        vkDestroyDevice(get_device(), nullptr);
        vkDestroyInstance(instance, nullptr);
    }
}