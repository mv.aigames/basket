#ifndef BASKET_MATERIAL_HPP
#define BASKET_MATERIAL_HPP

#include <core/pch.hpp>
#include <core/memory.hpp>
#include <core/color.hpp>
#include <resources/texture.hpp>
#include <math/math.hpp>
#include <render/shader/a_shader.hpp>

namespace basket
{
    class Material
    {
        public:
            void activate() noexcept;
            void set_shader(shared_pointer<AShader>& p_shader) noexcept;
            void set_param(const std::string& p_id, int p_value);
            void set_param(const std::string& p_id, float p_value);
            void set_param(const std::string& p_id, const Color& p_value);
            void set_param(const std::string& p_id, const Matrix44& p_value);
            void set_param(const std::string& p_id, const Vector3& p_value);
            void set_param(const std::string& p_name, const Texture& p_value);

            std::vector<AShader::VariableInfo> get_params() const;
            void update_params();
            void draw();
        
        private:
            void load_params();

        private:
            std::unordered_map<std::string, AShader::shader_integer> integer_params;
            std::unordered_map<std::string, AShader::shader_floating> floating_params;
            std::unordered_map<std::string, AShader::shader_vector3> vector3_params;
            std::unordered_map<std::string, AShader::shader_matrix4> matrix44_params;
            std::unordered_map<std::string, AShader::shader_vector4> vector4_params;
            std::unordered_map<std::string, AShader::shader_texture> texture_params;

            weak_pointer<AShader> shader;
    };
}

#endif