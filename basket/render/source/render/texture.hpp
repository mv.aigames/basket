#ifndef BASKET_TEXTURE_HPP
#define BASKET_TEXTURE_HPP

#include <core/pch.hpp>
#include <core/memory.hpp>
#include <core/color.hpp>
#include <core/results.hpp>
#include <io/file.hpp>
#include <render/render_api.hpp>

namespace basket
{
    class AImage
    {
        friend class Texture;
        public:
            using byte_type = uint8_t;
            using container_type = std::vector<byte_type>;

        public:
            virtual Result read(const std::vector<byte_type>& content) = 0;
            container_type get_data() const noexcept { return data; }
            void set_data(const container_type& p_data) noexcept { data = p_data; }
            size_t get_width() const noexcept { return width; }
            size_t get_height() const noexcept { return height; }

            Color get_color(size_t p_x, size_t p_y) const noexcept;
            PixelFormat get_pixel_format() const noexcept { return pixel_format; }
            void set_pixel_format(PixelFormat p_pixel_format) noexcept { pixel_format = p_pixel_format; }
        protected:
            void set_width(size_t p_width) noexcept { width = p_width; }
            void set_height(size_t p_height) noexcept { height = p_height; }
        private:
            // Data is without header. Only body.
            PixelFormat pixel_format;
            container_type data;
            size_t width;
            size_t height;
    };

    class EmptyImage: public AImage
    {
        public:
            EmptyImage() { set_width(0); set_height(0); set_data(container_type());}
            virtual Result read(const std::vector<byte_type>& content) override { return Result::success; }
    };

    class PNGReader: public AImage
    {
        struct IHDR
        {
            uint32_t length;
            uint32_t width;
            uint32_t height;
            uint8_t bit_depth;
            uint8_t color_type;
            uint8_t compression_method = 0;
            uint8_t filter_method = 0;
            uint8_t interlance_method = 0;
            uint32_t crc;
        } ihdr;

        

        public:
            virtual Result read(const std::vector<byte_type>& p_content) override;
        private:
            std::vector<uint8_t> decompress_idat(const uint8_t* p_data, size_t p_size) const;
    };

    class BMPReader: public AImage
    {
        public:
            virtual Result read(const std::vector<byte_type>& p_content) override;

        private:
            // 32 bits follow the order alpha, red, green, blue.
            container_type read_data_32bits(const container_type& p_data) const;
            struct Header
            {
                static constexpr uint16_t signature = 0x424D;
                uint32_t file_size;
                uint32_t reserved;
                uint32_t offset;
            };

            Header header;

            struct InfoHeader
            {
                uint32_t info_header_size;
                int32_t width;
                int32_t height;
                uint16_t planes{1};
                uint16_t bits_per_pixel;
                uint32_t compression;
                uint32_t image_size;
                uint32_t x_pixels_per_meter;
                uint32_t y_pixels_per_meter;
                uint32_t color_used;
                uint32_t important_colors;
            };

            InfoHeader info_header;
    };

    class Texture
    {
        public:
            Texture();
            Texture(const std::string& filepath);
            Result open(const std::string& filepath);
            size_t get_width() const { return reader->get_width(); }
            size_t get_height() const { return reader->get_height(); }
            const std::string& get_type() const;
            AImage::container_type get_data() const noexcept { return reader->get_data(); }
            PixelFormat get_pixel_format() const noexcept { return reader->get_pixel_format(); }
        
        private:
            Result load_bmp_file(const std::vector<unsigned char>& p_file_content);
            Result load_png_file(const std::vector<unsigned char>& p_file_content);
            uint32_t get_magic_number(const AImage::container_type& p_file_content) const;
            shared_pointer<AImage> reader;
            // _internal::TextureProperties properties;
            std::unordered_map<File::Format, shared_pointer<AImage>> readers;
    };
}

#endif