#ifndef BASKET_VERTEX_ARRAY_HPP
#define BASKET_VERTEX_ARRAY_HPP

namespace basket
{
    class VertexArray
    {
        public:
            VertexArray();
            void bind();
            void unbind();
        private:
            unsigned int id;
    };
}

#endif
