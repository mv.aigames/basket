#ifndef BASKET_OPENGL_PROGRAMN_HPP
#define BASKET_OPENGL_PROGRAMN_HPP

#include <core/core.hpp>
#include <math/math.hpp>

namespace basket
{
    class ShaderProgram
    {
        public:
            ShaderProgram();
            ~ShaderProgram();
            void attach_vertex_shader(const class VertexShader& p_vertex);
            void attach_fragment_shader(const class FragmentShader& p_fragment);
            void detach_vertex_shader(const class VertexShader& p_vertex);
            void detach_fragment_shader(const class FragmentShader& p_fragment);
            void link();
            size_t get_id() const { return id; }
            void use() const;

            void set_matrix(std::string_view p_name, const Matrix44& p_matrix);
            void set_vector(std::string_view p_name, const Vector3& p_vector);
            void set_vector(std::string_view p_name, const Color& p_vector);

        private:
            std::vector<const class VertexShader*> vertexes;
            std::vector<const class FragmentShader*> fragments;
            size_t id;
    };
}

#endif