#include "vertex_attribute.hpp"

#include <drivers/drivers.hpp>

using namespace basket;

void VertexAttribute::set_attribute_pointer(uint8_t p_index, uint64_t p_size, uint64_t p_chunk, void* p_offset)
{
    glVertexAttribPointer(p_index, (GLsizei)p_size, GL_FLOAT, GL_FALSE, (GLint)p_chunk, p_offset);
}

void VertexAttribute::enable_vertex_attribute(uint8_t p_index)
{
    glEnableVertexAttribArray(p_index);
}