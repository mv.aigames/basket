#ifndef BASKET_VERTEX_ATTRIBUTE_HPP
#define BASKET_VERTEX_ATTRIBUTE_HPP

#include <core/core.hpp>

namespace basket
{
    class VertexAttribute
    {
        public:
            void set_attribute_pointer(uint8_t p_index, uint64_t p_size, uint64_t p_chunk, void* p_offset);
            void enable_vertex_attribute(uint8_t p_index);
    };
}

#endif