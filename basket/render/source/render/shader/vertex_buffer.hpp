#ifndef BASKET_VERTEX_BUFFER_HPP
#define BASKET_VERTEX_BUFFER_HPP

#include <core/core.hpp>
#include <math/vector3.hpp>

namespace basket
{
    class VertexBuffer
    {
        public:
            using vertex_type = float;
        
        public:
            VertexBuffer();
            void resize(std::vector<vertex_type>& p_vertices);
            size_t get_size() const;
            const vertex_type* get_data();
            size_t get_data_size() const;
            void bind() const;
            void unbind();
        private:
            std::vector<vertex_type> vertices;
            size_t id;
    };
}

#endif