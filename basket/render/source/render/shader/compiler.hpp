#ifndef BASKET_SHADER_COMPILER_HPP
#define BASKET_SHADER_COMPILER_HPP

#include <core/pch.hpp>

namespace basket::shader
{
    class Compiler
    {
        public:
            bool load(const char* filename);
            void compile();
            bool success();
        
        private:
            bool compilation_succeed;
            std::string raw_code;
            std::string error_message;
    };
}

#endif