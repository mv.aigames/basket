#include "vertex_buffer.hpp"

#include <drivers/drivers.hpp>

using namespace basket;

VertexBuffer::VertexBuffer()
{
    glGenBuffers(1, (GLuint*)&id);
}

const VertexBuffer::vertex_type* VertexBuffer::get_data()
{
    return vertices.data();
}

size_t VertexBuffer::get_size() const
{
    return vertices.size();
}

size_t VertexBuffer::get_data_size() const
{
    return sizeof(vertex_type) * vertices.size();
}

void VertexBuffer::bind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, (GLuint)id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_type) * vertices.size(), vertices.data(), GL_STATIC_DRAW);
}

void VertexBuffer::unbind()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VertexBuffer::resize(std::vector<vertex_type>& p_vertices)
{
    vertices = std::move(p_vertices);
}
