#include "glsl_parser.hpp"

#include <core/logging.hpp>

using namespace basket;

static std::set<const char*> keys{"vertex", "fragment"};

GLSLParser::GLSLParser(const std::string& p_source_code): source_code(p_source_code)
{
}

bool GLSLParser::parse()
{
    vertex_code = get_chunk("vertex");
    fragment_code = get_chunk("fragment");
    return true;
}

std::string GLSLParser::get_vertex_code()
{
    return vertex_code;
}

std::string GLSLParser::get_fragment_code()
{
    return fragment_code;
}

std::string GLSLParser::get_chunk(const std::string& p_token, size_t p_offset)
{
    size_t pos = p_offset;
    while((pos = source_code.find(token_type, pos)) != std::string::npos)
    {
        size_t token_type_pos = pos;
        size_t space_pos = source_code.find_first_of(' ', token_type_pos);
        size_t token_begin_pos = source_code.find_first_not_of(" \n", space_pos);
        size_t token_end_pos = source_code.find_first_of("\n", token_begin_pos);
        // BK_LOG(cat, info, (
        //                     "chunk: ", source_code.substr(token_begin_pos, token_end_pos - token_begin_pos),
        //                     ", Contains token: ", source_code.substr(token_begin_pos, token_end_pos - token_begin_pos) == p_token
        //                     );
        pos = token_end_pos;
        if (source_code.substr(token_begin_pos, token_end_pos - token_begin_pos) == p_token)
        {
            // We have found the token we want. Now advance until the next token or end of file.
            pos = source_code.find(token_type, token_end_pos);
            if (pos == std::string::npos)
            {
                pos = source_code.size() + 1;
            }
            std::string_view code_chunk(source_code.data() + token_end_pos, pos - token_end_pos);
            // BK_LOG(cat, info, ("Code: ", code_chunk);
            return source_code.substr(token_end_pos, pos - token_end_pos);
        }
    }
    return "";
}