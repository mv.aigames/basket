#ifndef BASKET_GLSL_PARSER_HPP
#define BASKET_GLSL_PARSER_HPP

#include <core/pch.hpp>

namespace basket
{
    class GLSLParser
    {
        public:
            GLSLParser(const std::string& p_source_code);
            bool parse();
            std::string get_vertex_code();
            std::string get_fragment_code();
        private:
            std::string get_chunk(const std::string& p_token, size_t p_offset = 0);
            std::string source_code;
            std::string vertex_code = "";
            std::string fragment_code = "";

            static constexpr std::string_view token_type = "#shader_type";
    };
}

#endif