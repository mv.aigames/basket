#include "shader_manager.hpp"
#include <platform/opengl/square_primitive_shader.hpp>
#include <platform/opengl/sprite_shader.hpp>

using namespace basket;

ShaderManager::ShaderManager()
{   
}

Result ShaderManager::add_shader(const std::string& p_id, const AShader* p_shader) noexcept
{
    if (shaders.find(p_id) != shaders.end())
    {
        return Result::shader_already_exists;
    }

    shaders[p_id] = shared_pointer<AShader>(const_cast<AShader*>(p_shader));
    return Result::success;
}

shared_pointer<AShader> ShaderManager::get_shader(const std::string& p_id)
{
    auto it = shaders.find(p_id);
    if (it != shaders.end())
    {
        return it->second;   
    }
    return nullptr;
}

void ShaderManager::load_builtin_shaders()
{
    switch(mode)
    {
        case Mode::opengl:
            load_opengl_builtin_shaders();
        break;
        case Mode::vulkan:
            // BK_ASSERT(false, "There is no vulkan support");
        break;
    }
}

Result ShaderManager::add_shader(const std::string& p_id, const std::string& p_shader) noexcept
{
    return Result::success;
}

Result ShaderManager::load_opengl_builtin_shaders()
{
    shaders.clear();
    shaders["square_primitive"] = std::make_shared<OpenglSquarePrimitiveShader>();
    shaders["sprite"] = std::make_shared<OpenglSpriteShader>();
    return Result::success;
}

void ShaderManager::set_mode(Mode p_mode)
{
    if (p_mode != mode)
    {
        // Update all references
    }

    mode = p_mode;
    load_builtin_shaders();
}