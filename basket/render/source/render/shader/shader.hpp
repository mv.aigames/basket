#ifndef BASKET_A_SHADER_HPP
#define BASKET_A_SHADER_HPP

#include <core/math/matrix44.hpp>
#include <core/math/vector4.hpp>
#include <core/results.hpp>
#include <core/string.hpp>
#include <core/filesystem.hpp>
#include <core/vector.hpp>
#include <core/macros.hpp>
// #include <core/math/>

namespace basket
{
    class BASKET_API Shader
    {
        public:
            using shader_integer = int;
            using shader_floating = float; // real_t doesn't apply here.
            using shader_vector4 = VectorXYZW_Template<shader_floating>;
            using shader_matrix4 = _Matrix44_Template<shader_floating>;
            using shader_color = VectorXYZW_Template<shader_floating>;
            using shader_vector3 = VectorXYZ_Template<shader_floating>;
            // using shader_texture = Texture;

            enum class VariableType
            {
                integer,
                floating,
                matrix,
                vector4,
                vector3,
                matrix44,
                sampler2d,
                unknown
            };

            struct VariableInfo
            {
                std::string name;
                VariableType type;
            };

        public:
            static Result compile(const filesystem::path& p_filename);
            virtual vector<VariableInfo> get_params() const = 0;
            virtual void set_integer(const string& p_name, const uint64_t p_value) = 0;
            virtual void set_float(const string& p_name, const float p_value) = 0;
            virtual void set_matrix(const string& p_name, const Matrix44& p_matrix) = 0;
            virtual void set_vector(const string& p_name, const Vector3& p_vector) = 0;
            virtual void set_vector(const string& p_name, const Vector4& p_vector) = 0;

    };
}

#endif