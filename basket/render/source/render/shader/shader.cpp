#include "shader.hpp"

#include <core/os.hpp>
#include <core/logging.hpp>

BK_DECLARE_LOG(render);

namespace basket
{
    Result Shader::compile(const filesystem::path& p_filename)
    {
        string extension = p_filename.get_extension();
        filesystem::path out(p_filename.remove_extension());
        string out_path = p_filename.get_path_native() + ".spv";
        if (OS::glslc(p_filename.get_path_native(), out_path) != Result::success)
        {
            BK_LOG(render, error, "Failed to compile shader");
            return Result::failed;
        }        

        return Result::success;
    }
}
