#include "shader_program.hpp"

#include <drivers/drivers.hpp>
// #include "fragment_shader.hpp"
// #include "vertex_shader.hpp"

using namespace basket;

ShaderProgram::ShaderProgram()
{
    id = glCreateProgram();
}

ShaderProgram::~ShaderProgram()
{
    for(const auto& e : vertexes)
    {
        glDetachShader((GLuint)id, (GLuint)e->get_id());
    }

    for(const auto& f : fragments)
    {
        glDetachShader((GLuint)id, (GLuint)f->get_id());
    }
}

void ShaderProgram::link()
{
    glLinkProgram((GLuint)id);
    GLint success;
    glGetProgramiv((GLuint)id, GL_LINK_STATUS, &success);
    if(!success) {
        char info_log[512];
        glGetProgramInfoLog((GLuint)id, 512, NULL, info_log);
        BK_LOG(render, info, "Progran Shader error: {0}", info_log);
    }
}

void ShaderProgram::attach_vertex_shader(const VertexShader& p_vertex)
{
    vertexes.push_back(&p_vertex);
    glAttachShader((GLuint)id, (GLuint)p_vertex.get_id());
}

void ShaderProgram::attach_fragment_shader(const FragmentShader& p_fragment)
{
    fragments.push_back(&p_fragment);
    glAttachShader((GLuint)id, (GLuint)p_fragment.get_id());
}

void ShaderProgram::detach_vertex_shader(const VertexShader& p_vertex)
{
    for(decltype(vertexes)::iterator it = vertexes.begin(); it != vertexes.end(); ++it)
    {
        if (*it == &p_vertex)
        {
            glDetachShader((GLuint)id, (GLuint)(*it)->get_id());
            vertexes.erase(it);
            break;
        }
    }
}

void ShaderProgram::detach_fragment_shader(const FragmentShader& p_vertex)
{
    for(decltype(fragments)::iterator it = fragments.begin(); it != fragments.end(); ++it)
    {
        if (*it == &p_vertex)
        {
            glDetachShader((GLuint)id, (GLuint)(*it)->get_id());
            fragments.erase(it);
            break;
        }
    }
}

void ShaderProgram::use() const
{
    glUseProgram((GLuint)id);
}


void ShaderProgram::set_matrix(std::string_view p_name, const Matrix44& p_matrix)
{
    unsigned int location = glGetUniformLocation((GLuint)id, p_name.data());
    glUniformMatrix4fv(location, 1, GL_TRUE, &p_matrix.row0.x);
}

void ShaderProgram::set_vector(std::string_view p_name, const Vector3& p_vector)
{
    unsigned int location = glGetUniformLocation((GLuint)id, p_name.data());
    glUniform3f(location, p_vector.x, p_vector.y, p_vector.z);
}

void ShaderProgram::set_vector(std::string_view p_name, const Color& p_color)
{
    unsigned int location = glGetUniformLocation((GLuint)id, p_name.data());
    // glUniform(location, p_vector.r, p_vector.g, p_vector.b);
    glUniform4f(location, p_color.r, p_color.g, p_color.b, p_color.a);
}