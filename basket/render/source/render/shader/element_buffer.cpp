#include "element_buffer.hpp"

using namespace basket;

ElementBuffer::ElementBuffer()
{
    glGenBuffers(1, (GLuint*)&id);
}

void ElementBuffer::bind()
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (GLuint)id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(element_index_type) * get_size(), get_data(), GL_STATIC_DRAW);
}

const ElementBuffer::element_index_type* ElementBuffer::get_data()
{
    return indices.data();
}

size_t ElementBuffer::get_size() const
{
    return indices.size();
}

void ElementBuffer::resize(std::vector<element_index_type>& p_indices)
{
    indices = std::move(p_indices);
}