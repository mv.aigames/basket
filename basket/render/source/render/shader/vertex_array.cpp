#include "vertex_array.hpp"
#include <drivers/drivers.hpp>

using namespace basket;

VertexArray::VertexArray()
{
    glGenVertexArrays(1, &id);
}

void VertexArray::bind()
{
    glBindVertexArray(id);
}

void VertexArray::unbind()
{
    glBindVertexArray(0);
}