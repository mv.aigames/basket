#ifndef BASKET_ELEMENT_BUFFER_HPP
#define BASKET_ELEMENT_BUFFER_HPP

#include <core/core.hpp>
#include <drivers/drivers.hpp>

namespace basket
{
    class ElementBuffer
    {
        public:
            using element_index_type = GLuint;

        public:
            ElementBuffer();
            const element_index_type* get_data();
            size_t get_size() const;
            void resize(std::vector<element_index_type>& p_indices);
            void bind();
        private:
            size_t id;
            std::vector<element_index_type> indices;
    };
}

#endif