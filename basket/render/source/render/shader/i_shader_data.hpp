#ifndef BASKET_SHADER_DATA_HPP
#define BASKET_SHADER_DATA_HPP

#include <core/pch.hpp>

namespace basket
{
    class IShaderData
    {
        public:
            virtual std::vector<float> get_vertex_data() const;
    };
}

#endif