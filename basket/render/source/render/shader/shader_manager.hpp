#ifndef BASKET_SHADER_MANAGER_HPP
#define BASKET_SHADER_MANAGER_HPP

#include <core/pch.hpp>
#include <core/memory.hpp>
#include <render/shader/a_shader.hpp>


namespace basket
{
    class ShaderManager
    {
        public:
            enum class Mode
            {
                vulkan,
                opengl
            };
        public:
            static ShaderManager& get()
            {
                static ShaderManager instance;
                return instance;
            }

            Result add_shader(const std::string& p_id, const AShader* p_shader) noexcept;
            Result add_shader(const std::string& p_id, const std::string& p_filepath) noexcept;
            shared_pointer<AShader> get_shader(const std::string& p_id);
            void set_mode(Mode p_mode);

        private:
            void load_builtin_shaders();
            Result load_opengl_builtin_shaders();

        private:
            Mode mode;
            ShaderManager();
            std::unordered_map<std::string, shared_pointer<AShader>> shaders;
    };
}

#endif