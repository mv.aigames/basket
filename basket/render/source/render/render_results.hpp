#ifndef BASKET_RENDER_RESULTS_HPP
#define BASKET_RENDER_RESULTS_HPP

namespace basket
{
    enum RenderResult
    {
        success = 0,
        create_buffer_failed,
        allocate_memory_failed,
        bind_buffer_memory
    };
}

#endif