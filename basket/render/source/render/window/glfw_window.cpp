#include "glfw_window.hpp"
#include <core/logging.hpp>
#include <core/glfw.hpp>
// #include <engine/inputs/input.hpp>
// #include <engine/events/system.hpp>
// #include <engine/events/components.hpp>

#include <core/logging.hpp>

BK_DECLARE_LOG(render);

namespace basket
{
    GLFWWindow::GLFWWindow(const char* p_title, int64_t p_width, int64_t p_height)
    {
        

        glfwDefaultWindowHints();

        // glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
        // glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
        // glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        // Cannot be a api. Vulkan is jealous about sharing.
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        // glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
        // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL 

        window = glfwCreateWindow((int)p_width, (int)p_height, p_title, nullptr, nullptr);
        if (!window)
        {
            BK_LOG(render, error, "Failed to create window");
            return;
        }


        glfwSetErrorCallback([](int p_error_code, const char* description) {
            BK_LOG(render, info, description);
            
        });

        glfwSetWindowUserPointer(window, this);
        

        // glfwSetKeyCallback(window, [](GLFWwindow* p_window, int p_key, int p_scancode, int p_action, int p_mods) {
        //     switch(p_action)
        //     {
        //         case GLFW_PRESS:
        //         {   
        //             KeyboardEvent e{true, input::get_input_code(p_key)};
        //             EventSystem::get().trigger_event<KeyboardEvent>(e);
        //         }
        //             break;
        //         case GLFW_RELEASE:
        //         {
        //             KeyboardEvent e{false, input::get_input_code(p_key)};
        //             EventSystem::get().trigger_event<KeyboardEvent>(e);
        //         }
        //             break;
        //         case GLFW_REPEAT:
                    
        //             break;
        //         default:
        //             BK_LOG(cat, info, ("Unknown GLFW event registered: ", p_action);
        //             break;
        //     }
        // });

        // glfwSetFramebufferSizeCallback(window, [](GLFWwindow* p_window, int p_width, int p_height) {
        //     // glViewport(0, 0, p_width, p_height);
        //     });
    }

    void GLFWWindow::show()
    {
        
    }

    void GLFWWindow::hide()
    {
        
    }

    void GLFWWindow::close()
    {
        
    }

    void* GLFWWindow::get_window_native() const
    {
        return window;
    }

    bool GLFWWindow::should_close()
    {
        return glfwWindowShouldClose(window);
    }

    void GLFWWindow::pull_events()
    {
        glfwPollEvents();
    }

    void GLFWWindow::size_buffer_callback(GLFWwindow* window, int width, int height)
    {
        // glViewport(0, 0, width, height);
    }

    GLFWWindow::~GLFWWindow()
    {
        glfwDestroyWindow(window);
        glfwTerminate();
    }   
}
