#ifndef BASKET_GLFW_WINDOW_HPP
#define BASKET_GLFW_WINDOW_HPP


#include <core/pch.hpp>
#include <core/macros.hpp>
#include "window.hpp"



struct GLFWwindow;

namespace basket
{
    class BASKET_API GLFWWindow: public AWindow
    {
        public:
            GLFWWindow(const char* p_title, int64_t p_width, int64_t p_height);
            ~GLFWWindow();

            virtual void show() override;
            virtual void hide() override;
            virtual void close() override;
            virtual bool should_close() override;
            virtual void pull_events() override;
            virtual void* get_window_native() const override;
            void size_buffer_callback(GLFWwindow* window, int width, int height);

        private:
            GLFWwindow* window;
            // void key_event_callback(GLFWwindow* p_window, int key, int scancode, int mods);
    };

    // struct CommomGLFWData
    // {
    //     GLFWwindow* window;
    // };
}

#endif