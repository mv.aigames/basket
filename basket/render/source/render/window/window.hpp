#ifndef BASKET_WINDOW_HPP
#define BASKET_WINDOW_HPP

#include  <core/string.hpp>

namespace basket
{
    struct WindowGenerationData
    {
        string title;
        uint32_t width;
        uint32_t height;
        uint32_t flags;
    };
    
    class AWindow
    {
        public:
        enum class flags: uint32_t
        {
            resizeable,
            fullscreen,
            borderless
        };

        public:
            virtual void show() = 0;
            virtual void hide() = 0;
            virtual void close() = 0;
            virtual bool should_close() = 0;
            virtual void pull_events() = 0;
            virtual void* get_window_native() const = 0;
    };
}


#endif