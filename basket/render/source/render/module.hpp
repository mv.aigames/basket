#ifndef BASKET_RENDER_MODULE_HPP
#define BASKET_RENDER_MODULE_HPP

#include <core/module_loader/module.hpp>
#include <core/macros.hpp>
// #include <render/render.hpp>
#include <core/logging.hpp>
BK_DECLARE_LOG(render);

namespace basket
{
    class Render;

    namespace render
    {

        class BASKET_API Module: public AModule
        {
            public:
                Module();
            public:
                virtual Result pre_init() override;
                virtual Result init() override;
                virtual Result post_init() override;
                virtual Result pre_destroy() override;
                virtual Result destroy() override;
                virtual Result post_destroy() override;
            
            public:
                virtual void update(const real_t p_delta) override;
            
            public:
                class Render* get_render();
            private:
                class Render* _render;
        };
    }
}

#endif