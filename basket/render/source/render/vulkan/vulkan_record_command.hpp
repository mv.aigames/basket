#ifndef BASKET_VULKAN_RECORD_COMMAND_HPP
#define BASKET_VULKAN_RECORD_COMMAND_HPP

#include <core/macros.hpp>
#include <vulkan/vulkan.h>

namespace basket
{
    class BASKET_API IVulkanRecordCommand
    {
        public:
            virtual bool one_time() const = 0;
            virtual void record(int32_t p_image_index, VkCommandBuffer p_command_buffer) = 0;
    };
}

#endif