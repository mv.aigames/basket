#shader_type fragment
#version 330 core

out vec4 out_color;

in vec4 vertex_color;

void main()
{
    out_color = vertex_color;
}

#shader_type vertex
#version 330 core

layout(location = 0) in vec3 vertex_position;

uniform mat4 u_model;
uniform mat4 u_projection;
uniform mat4 u_view;
uniform vec4 u_color;

out vec4 vertex_color;

void main()
{
    gl_Position = u_projection * u_view * u_model * vec4(vertex_position, 1.0);
    vertex_color = u_color;
}