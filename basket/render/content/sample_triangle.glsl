#shader_type fragment
#version 330 core

out vec4 out_color;

in vec4 vertex_color;

void main()
{
    out_color = vec4(1);
}

#shader_type vertex
#version 330 core

layout(location = 0) in vec3 vertex_position;

uniform vec4 u_color;

out vec4 vertex_color;

void main()
{
    gl_Position = vec4(vertex_position, 1.0);
    vertex_color = u_color;
}