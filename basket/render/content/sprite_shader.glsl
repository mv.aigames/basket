#shader_type vertex
#version 330
layout(location = 0) in vec3 vertrex_coord;
layout(location = 1) in vec3 vertex_color;
layout(location = 2) in vec2 texture_coord;

uniform mat4 u_model;
uniform mat4 u_projection;
uniform mat4 u_view;

out vec3 fragment_color;
out vec2 fragment_texture_coord;

void main ()
{
    gl_Position = u_projection * u_view * u_model * vec4(vertrex_coord, 1.0);
    fragment_color = vertex_color;
    fragment_texture_coord = texture_coord;
}

#shader_type fragment
#version 330

out vec4 out_color;

in vec3 fragment_color;
in vec2 fragment_texture_coord;

uniform sampler2D u_texture;

uniform vec4 u_color = vec4(1);

void main()
{
    vec4 c = texture(u_texture, fragment_texture_coord);
    out_color = vec4(c.r * u_color.r, c.g * u_color.g, c.b * u_color.b, c.a * u_color.a);
    // out_color = mix(texture(u_texture, fragment_texture_coord), vec4(fragment_color, 1.0), 0.0);
}