#ifndef PHYSICS_COMPONENTS_HPP
#define PHYSICS_COMPONENTS_HPP

namespace basket
{
    struct RigidbodyComponent
    {

    };

    struct SphereColliderComponent
    {
        float radius = 0.5f;
    };

    struct BoxColliderComponent
    {
        Vector3 size;
    };
}

#endif
