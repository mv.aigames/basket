#include "system.hpp"

#include <ecs/notebook.hpp>
#include <core/logging.hpp>
#include <ecs/world.hpp>
#include <3d/components.hpp>


namespace basket
{
    PhysicsSystem& PhysicsSystem::get()
    {
        static PhysicsSystem instance;
        return instance;
    }

    void PhysicsSystem::added_component_event(std::shared_ptr<BoxColliderComponent>& p_component)
    {
        BK_LOG(cat, info, ("Added BoxcolliderComponent");
    }

    void PhysicsSystem::added_component_event(std::shared_ptr<RigidbodyComponent>& p_component)
    {
        BK_LOG(cat, info, ("Added RigidbodyComponent");
    }

    void PhysicsSystem::init()
    {

    }

    void PhysicsSystem::set_gravity(const Vector3& p_value) 
    {
        gravity = p_value;
    }

    Vector3 PhysicsSystem::get_gravity() const 
    {
        return gravity;
    }

    void PhysicsSystem::update()
    {
        
        ecs::Notebook& notebook = World::get().get_notebook();
        ecs::View<RigidbodyComponent, TransformComponent> view = notebook.get_view<RigidbodyComponent, TransformComponent>();
        view.each([&](RigidbodyComponent& rb, TransformComponent& tc) {
            // BK_LOG(cat, info, ("Transform position: ", tc.position);
            tc.position -= (gravity * Time::get().get_delta());
            // BK_LOG(cat, info, ("Inside view call");
        });
        // view.each([](std::shared_ptr<RigidbodyComponent> p_rigidbody, std::shared_ptr<BoxColliderComponent> p_collider) {

        // });
    }

    // enough const? I don't think so.
    const shared_pointer<const HitResult> PhysicsSystem::test_collision(const Vector3& p_bc_position, const BoxColliderComponent& p_bc, const Vector3& p_sc_position, const SphereColliderComponent& p_sc) const 
    {

        return nullptr;
    }
}