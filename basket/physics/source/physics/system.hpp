#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include <core/core.hpp>
#include <ecs/notebook.hpp>
#include <ecs/entity.hpp>
#include <ecs/system.hpp>
#include <math/math.hpp>

#include "components.hpp"

namespace basket
{
    struct HitResult
    {

    };


    class PhysicsSystem
    {
        friend class Main;

        public:
            static PhysicsSystem& get();
            void init();
            void update();
            void shutdown();
            void set_gravity(const Vector3& p_value) ;
            Vector3 get_gravity() const ;
            const shared_pointer<const HitResult> test_collision(const Vector3& p_bc_position, const BoxColliderComponent& p_bc, const Vector3& p_sc_position, const SphereColliderComponent& p_sc) const ;
            

        private:
            PhysicsSystem() = default;
            PhysicsSystem(const PhysicsSystem& p_other) = default;
            PhysicsSystem(PhysicsSystem&& p_other) = default;
            PhysicsSystem& operator= (const PhysicsSystem& p_otehr) = default;
            PhysicsSystem& operator= (PhysicsSystem&& p_other) = default;
            void added_component_event(std::shared_ptr<BoxColliderComponent>& p_box_collider);
            void added_component_event(std::shared_ptr<RigidbodyComponent>& p_box_collider);

        private:
            Vector3 gravity{0.0f, -9.8f, 0.0f};
    };
}

#endif
