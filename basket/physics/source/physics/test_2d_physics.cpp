#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <basket.hpp>

using namespace basket;

TEST_CASE("2D Physics", "[Shader]") {

    _2d::PhysicsSystem& system = _2d::PhysicsSystem::get();
    _2d::PointColliderComponent pc1;
    _2d::PointColliderComponent pc2;
    _2d::TransformComponent tc1;
    _2d::TransformComponent tc2;
    REQUIRE(system.is_overlapping(tc1, pc1, tc2, pc2));

    _2d::CircleColliderComponent cc1;
    _2d::CircleColliderComponent cc2;

    tc1.position = Vector2::zero;
    tc2.position = Vector2(0, 1);
    cc1.radius = 6.0f;
    cc2.radius = 6.0f;
    REQUIRE(system.is_overlapping(tc1, pc1, tc2, cc1));

    tc1.position.y = 10.0f;
    REQUIRE(system.is_overlapping(tc1, cc1, tc2, cc2));

    _2d::SquareColliderComponent sc1;
    _2d::SquareColliderComponent sc2;
    tc1.position = Vector2(5, 5);
    tc2.position = Vector2(4, 4);
    sc1.size = Vector2(3);
    REQUIRE(system.is_overlapping(tc1, pc1, tc2, sc1));
    tc1.position = Vector2(5, 5);
    tc2.position = Vector2(0, 0);
    sc1.size = Vector2(1);
    cc1.radius = 3;
    REQUIRE(!system.is_overlapping(tc1, cc1, tc2, sc1));
    tc1.position = Vector2(0, 0);
    sc1.size = Vector2(4);
    cc1.radius = 4;
    REQUIRE(system.is_overlapping(tc1, cc1, tc2, sc1));

    tc1.position = Vector2(5, 5);
    tc2.position = Vector2(4, 4);
    sc1.size = Vector2(2, 2);
    sc2.size = Vector2(3);
    REQUIRE(system.is_overlapping(tc1, sc1, tc2, sc2));

    tc1.position.y = 100.0f;
    REQUIRE(!system.is_overlapping(tc1, sc1, tc2, sc2));
}