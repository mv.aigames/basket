#include "bengine.hpp"

#include <render/window/glfw_window.hpp>
#include <render/render.hpp>
#include <core/logging.hpp>
#include <core/glfw.hpp>
#include <render/window/window.hpp>
#include <core/glfw.hpp>
#include <core/os.hpp>
#include <engine/time.hpp>

#include <core/profiler.hpp>

namespace basket
{

    Engine::Engine()
    {
        
    }

    Engine& Engine::get()
    {
        static Engine instance;
        return instance;
    }

    int Engine::main_impl(int argc, char** argv, Application* p_application)
    {
        // if (!glfwInit())
        // {
        //     BK_LOG(cat, info, ("GLFW couldn't be initialized properly");
        //     OS::abort();
        // }

        set_application(p_application);
        init_modules();
        run();

        return 0;
    }

    void Engine::init_modules()
    {
        _modules = app->get_modules();

        for(auto& m : _modules)
        {
            m->pre_init();
        }

        app->startup();

        for(auto& m : _modules)
        {
            m->init();
        }
        
        app->post_startup();

        for(auto& m : _modules)
        {
            m->post_init();
        }

        for (auto& m: _modules)
        {
            switch(m->get_group())
            {
                case AUpdate::Group::pre_physics:
                    _pre_physics_updates.push_back(m);
                    break;
                case AUpdate::Group::physics:
                    _physics_updates.push_back(m);
                    break;
                case AUpdate::Group::post_physics:
                    _post_physics_updates.push_back(m);
                    break;
                case AUpdate::Group::game:
                    _game_updates.push_back(m);
                    break;
                case AUpdate::Group::render:
                    BK_LOG(engine, info, "Adding udpate to render group.");
                    _render_updates.push_back(m);
                    break;
                case AUpdate::Group::none:
                    _none_updates.push_back(m);
                    break;
            }
        }
        
    }

    

    void Engine::run_impl()
    {

        while(!app->want_to_shutdown())
        {
            BK_PROFILE(GameLoop);
            Time::update();
            real_t delta = Time::get_delta();

            for (auto& u: _pre_physics_updates)
            {
                u->update(delta);
            }

            for (auto& u: _physics_updates)
            {
                u->update(delta);
            }

            for (auto& u: _post_physics_updates)
            {
                u->update(delta);
            }

            app->update(delta);
            
            for (auto& u: _game_updates)
            {
                u->update(delta);
            }

            // for (size_t index = 0; _render_updates.size(); ++index)
            // {
            //     _render_updates[index]->update(delta);
            // }
            for (auto& u: _render_updates)
            {
                u->update(delta);
            }

            // while (delta < Time::delta<real_t, (real_t)60>)
            // {
            //     Time::update();
            //     delta += Time::get_delta();
            // }

        }

        app->shutdown();
        clean();
    }

    void Engine::clean() 
    {
        for (int index = _modules.size() - 1; index >= 0; --index)
        {
            _modules[index]->pre_destroy();
        }

        for (int index = _modules.size() - 1; index >= 0; --index)
        {
            _modules[index]->destroy();
        }

        for (int index = _modules.size() - 1; index >= 0; --index)
        {
            _modules[index]->post_destroy();
        }
        // main_render->shutdown();
    }
}
