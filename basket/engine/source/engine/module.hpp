#ifndef BASKET_ENGINE_MODULE_HPP
#define BASKET_ENGINE_MODULE_HPP

#include <core/module_loader/module.hpp>
#include <core/macros.hpp>
#include <core/logging.hpp>


BK_DECLARE_LOG(engine);
namespace basket::engine
{
    class BASKET_API Module: public AModule
    {
        public:
            Module(): AModule(AUpdate::Group::game) {}
        public:
            virtual Result pre_init() override;
            virtual Result init() override;
            virtual Result post_init() override;
            virtual Result pre_destroy() override;
            virtual Result destroy() override;
            virtual Result post_destroy() override;
    };
}

#endif