#ifndef BASKET_EVENT_SYSTEM_HPP
#define BASKET_EVENT_SYSTEM_HPP

// #include <ecs/system.hpp>
#include <core/pch.hpp>
#include <core/typedefs.hpp>
#include <core/results.hpp>

namespace basket
{
    class EventSystem
    {
        public:
            virtual Result init();
            virtual void update();
            virtual void shutdown();
            static EventSystem& get();

            // template<typename T>
            // void register_event(std::function<void(T&)> p_function)
            // {
            //     pool<T>.emplace_back(p_function);
            // }

            // template<typename T>
            // void trigger_event(T& p_event)
            // {
            //     for(std::function<void(T&)>& func : pool<T>)
            //     {
            //         func(p_event);
            //     }
            // }

        private:
            EventSystem() = default;
            // template<typename T>
            // static std::vector< std::function<void(T&)> > pool;
    };

    // template<typename T>
    // std::vector<std::function<void(T&)>> EventSystem::pool;
}

#endif
