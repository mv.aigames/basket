#include "system.hpp"

namespace basket
{
    EventSystem& EventSystem::get()
    {
        static EventSystem instance;
        return instance;
    }

    Result EventSystem::init()
    {
        return Result::success;
    }

    void EventSystem::update()
    {
        
    }

    void EventSystem::shutdown()
    {
        
    }
}