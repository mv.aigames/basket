#ifndef BASKET_INPUT_COMPONENTS_HPP
#define BASKET_INPUT_COMPONENTS_HPP


#include <core/math/vector2.hpp>

namespace basket
{
    enum class EventType: unsigned char
    {
        mouse_down, mouse_up, mouse_motion,
        window_move, window_close, window_minimize,
        key_down, key_up
    };

    enum class EventCategory: uint8_t
    {
        window = 1, 
        input = 1 << 1, 
        render = 1 << 2, 
        mouse = 1 << 3, 
        keyboard = 1 << 4
    };



    #define ADD_EVENT_PROPERTIES(EVENT_CLASS, EVENT_TYPE, EVENT_CATEGORY) \
        const EventType type = EVENT_TYPE; \
        const EventCategory category = EVENT_CATEGORY; \
        const char* name = #EVENT_CLASS

    struct KeyboardEvent
    {
        const bool pressed;
        const input::KeyCode code;
        ADD_EVENT_PROPERTIES(KeyboardUpEvent, EventType::key_up, EventCategory::input);

    };

    struct MouseButtonEvent
    {
        const bool pressed;
        const Vector2i position;        
        const uint8_t button;
        ADD_EVENT_PROPERTIES(MouseButtonEvent, EventType::key_up, EventCategory::input);
    };

    struct MouseMotionEvent
    {
        const Vector2i position;
        const Vector2 motion;
        ADD_EVENT_PROPERTIES(MouseMotionEvent, EventType::mouse_motion, EventCategory::input);
    };

    
}

#endif
