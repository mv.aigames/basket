#ifndef BASKET_TIME_HPP
#define BASKET_TIME_HPP

#include <core/macros.hpp>
#include <core/typedefs.hpp>

namespace basket
{
    class BASKET_API Time
    {
        

        public:
            static Time& get()
            {
                static Time instance;
                return instance;
            }


            BK_FORCEINLINE static real_t get_delta() { return get().get_delta_impl(); }
            // float get_total_time();
            real_t get_delta_impl();
            // float get_physics_delta();
            BK_FORCEINLINE static void update() { get().update_impl(); }
            // float get_herz();

        public:
            template<typename U, U value>
            static constexpr U delta = static_cast<U>(1) / static_cast<U>(value);
        
        private:
            Time();
            void update_impl();
            real_t _delta;

    };
}

#endif