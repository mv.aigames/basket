#include "input.hpp"
#include <core/pch.hpp>
#include <core/glfw.hpp>

namespace basket::input
{
    static std::unordered_map<int, KeyCode> codes = {
      {GLFW_KEY_0, KeyCode::zero},  
      {GLFW_KEY_1, KeyCode::one},  
      {GLFW_KEY_2, KeyCode::two},  
      {GLFW_KEY_3, KeyCode::three},  
      {GLFW_KEY_4, KeyCode::four},  
      {GLFW_KEY_5, KeyCode::five},  
      {GLFW_KEY_6, KeyCode::six},  
      {GLFW_KEY_7, KeyCode::seven},  
      {GLFW_KEY_8, KeyCode::eight},  
      {GLFW_KEY_9, KeyCode::nine},

      {GLFW_KEY_UP, KeyCode::arrow_up},
      {GLFW_KEY_DOWN, KeyCode::arrow_down},
      {GLFW_KEY_LEFT, KeyCode::arrow_left},
      {GLFW_KEY_RIGHT, KeyCode::arrow_right},

      {GLFW_KEY_A, KeyCode::a},
      {GLFW_KEY_B, KeyCode::b},
      {GLFW_KEY_C, KeyCode::c},
      {GLFW_KEY_D, KeyCode::d},
      {GLFW_KEY_E, KeyCode::e},
      {GLFW_KEY_F, KeyCode::f},
      {GLFW_KEY_G, KeyCode::g},
      {GLFW_KEY_H, KeyCode::h},
      {GLFW_KEY_I, KeyCode::i},
      {GLFW_KEY_J, KeyCode::j},
      {GLFW_KEY_K, KeyCode::k},
      {GLFW_KEY_L, KeyCode::l},
      {GLFW_KEY_M, KeyCode::m},
      {GLFW_KEY_N, KeyCode::n},
      {GLFW_KEY_O, KeyCode::o},
      {GLFW_KEY_P, KeyCode::p},
      {GLFW_KEY_Q, KeyCode::q},
      {GLFW_KEY_R, KeyCode::r},
      {GLFW_KEY_S, KeyCode::s},
      {GLFW_KEY_T, KeyCode::t},
      {GLFW_KEY_U, KeyCode::u},
      {GLFW_KEY_V, KeyCode::v},
      {GLFW_KEY_W, KeyCode::w},
      {GLFW_KEY_X, KeyCode::x},
      {GLFW_KEY_Y, KeyCode::y},
      {GLFW_KEY_Z, KeyCode::z},
    };

    BASKET_API KeyCode get_input_code(int p_platform_code)
    {
        std::unordered_map<int, KeyCode>::iterator it = codes.find(p_platform_code);
        if (it != codes.end())
        {
            return it->second;
        }
        
        return KeyCode::unknown;
    }
}