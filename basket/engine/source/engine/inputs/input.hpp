#ifndef BASKET_INPUT_HPP
#define BASKET_INPUT_HPP

#include <core/macros.hpp>

namespace basket::input
{
    enum class KeyCode
    {
        a = 0, b, c, d, e, f, g, h, i, j, 
        k, l, m, n, o, p, q, r, s, t, u, 
        v, w, x, y, z,
        zero, one, two, three, four, five, six, seven, eight, nine,
        arrow_up, arrow_down, arrow_left, arrow_right,
        unknown
    };

    BASKET_API KeyCode get_input_code(int p_platform_code);
}

#endif