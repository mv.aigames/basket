#ifndef BASKET_BENGINE_HPP
#define BASKET_BENGINE_HPP

#include <core/macros.hpp>
#include <core/memory.hpp>

#include <engine/application.hpp>

namespace basket
{
    class BASKET_API Engine
    {
        private:
            Engine();
            vector<AModule*> _modules;

            vector<AUpdate*> _pre_physics_updates;
            vector<AUpdate*> _physics_updates;
            vector<AUpdate*> _post_physics_updates;
            vector<AUpdate*> _game_updates;
            vector<AUpdate*> _render_updates;
            vector<AUpdate*> _none_updates;

            // object_pointer<class AWindow> main_window;
            // object_pointer<class Render> main_render;

        private:
            Application* app;

        public:
            static Engine& get();
            BK_FORCEINLINE static void run() { get().run_impl(); }
            BK_FORCEINLINE static int main(int argc, char** argv, Application* p_application) { return get().main_impl(argc, argv, p_application); }
            // BK_FORCEINLINE static void generate_window(const struct WindowGenerationData& p_data) { get().generate_window_impl(p_data); }
            // BK_FORCEINLINE static class IRenderDriver* get_render_driver() { return get().get_render_driver_impl(); }
            // BK_FORCEINLINE static class AWindow* get_window() { return get().get_window_impl(); }
        
        private:
            void set_application_impl(Application* p_application) { app = p_application; }
            void run_impl();
            int main_impl(int argc, char** argv, Application* p_application);
            // void generate_window_impl(const struct WindowGenerationData& p_data);
            void init_modules();
            // class IRenderDriver* get_render_driver_impl();
            // class AWindow* get_window_impl();
            void clean();

        public:
            BK_FORCEINLINE static void set_application(Application* p_application) { get().set_application_impl(p_application); }
    };
}

#endif