#ifndef BASKET_APPLICATION_HPP
#define BASKET_APPLICATION_HPP

#include <core/typedefs.hpp>
#include <core/macros.hpp>
#include <core/vector.hpp>
#include <core/module_loader/module.hpp>

namespace basket
{
    class BASKET_API Application
    // : public AUpdate
    {
        // public:
        //     Application(): AUpdate(AUpdate::Group::game) {}
        
        public:
            /**
             * @brief Load all dependencies.
             * 
             * @return * void 
             */
            virtual void startup()  {}

            /**
             * @brief Load all dependencies.
             * 
             * @return * void 
             */
            virtual void post_startup()  {}

            /**
             * @brief Update application
             * 
             */
            virtual void update(const real_t delta)  = 0;

            /**
             * @brief tells to the engine whether that application wants to shutdown or not.
             * 
             * @return true 
             * @return false 
             */
            virtual bool want_to_shutdown() const  = 0;
            /**
             * @brief clean everything
             * 
             */
            virtual void shutdown()  = 0;

            virtual vector<AModule*> get_modules() = 0;
    };

    
}

#endif