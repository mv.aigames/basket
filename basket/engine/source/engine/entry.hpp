#ifndef BASKET_ENTRY_HPP
#define BASKET_ENTRY_HPP


#include <engine/application.hpp>
#include <core/functional.hpp>
#include <core/logging.hpp>
#include "bengine.hpp"

#include <iostream>

namespace basket
{
    Application* get_application();
}


int main (int argc, char** argv)
{
    ::basket::Application* app = ::basket::get_application();
    ::basket::Engine::main(argc, argv, app);
    return 0;
}

#endif