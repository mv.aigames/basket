#include "module.hpp"

#include <core/logging.hpp>

#include <core/os.hpp>



namespace basket::engine
{

    Result Module::pre_init() 
    {
        BK_LOG(engine, info, "Engine pre_init");
        return Result::success;
    }
    Result Module::init() 
    {
        BK_LOG(engine, info, "Engine init");
        return Result::success;
    }
    Result Module::post_init() 
    {
        BK_LOG(engine, info, "Engine post_init");
        return Result::success;
    }
    Result Module::pre_destroy() 
    {
        BK_LOG(engine, info, "Engine pre_destroy");
        return Result::success;
    }
    Result Module::destroy() 
    {
        BK_LOG(engine, info, "Engine destroy");
        return Result::success;
    }
    Result Module::post_destroy() 
    {
        BK_LOG(engine, info, "Engine post_destroy");
        return Result::success;
    }
}
