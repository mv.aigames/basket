#include "timer_system.hpp"

#include <core/logging.hpp>

using namespace basket;

TimerSystem& TimerSystem::get()
{
    static TimerSystem instance;
    return instance;
}

Result TimerSystem::init()
{
    ecs::Notebook& notebook = World::get().get_notebook();
    std::function<void(const ecs::Entity&, TimerComponent&)> f = std::bind(&TimerSystem::added_timer_component, this, std::placeholders::_1, std::placeholders::_2);
    notebook.bind_component_added_event_object<TimerComponent>(f);

    return Result::success;
}

void TimerSystem::update()
{
    for(std::vector<TimerComponent*>::iterator it = running_components.begin(); it != running_components.end(); ++it)
    {
        TimerComponent* t = (*it);
        t->remaining_time -= Time::get().get_delta();
        if (t->remaining_time <= 0.0f)
        {
            // Emit signal
            for(timer_finished_function& f : functions[t])
            {
                f(*t);
            }

            t->remaining_time = t->time;
            if (!t->repeat)
            {
                running_components.erase(it);
                paused_components.push_back(t);
            }
        }
    }
}

void TimerSystem::shutdown()
{

}

void TimerSystem::add_timer_finished_function(const TimerComponent& p_component, timer_finished_function& p_function)
{
    functions[&p_component].push_back(p_function);
}

void TimerSystem::remove_timer_finished_function(const TimerComponent& p_component, timer_finished_function& p_function)
{
    // std::vector<timer_finished_function>& fs = functions[&p_component];
    // for(std::vector<timer_finished_function>::iterator it = fs.begin(); it != fs.end(); ++it)
    // {
    //     if (*it == p_function)
    //     {
    //         fs.erase(&p_function);
    //         break;
    //     }
    // }
}

void TimerSystem::play(TimerComponent& p_component)
{
    for (std::vector<TimerComponent*>::iterator it = paused_components.begin(); it != paused_components.end(); ++it)
    {
        if (*it == &p_component)
        {
            paused_components.erase(it);
            break;
        }
    }

    running_components.push_back(&p_component);
}

void TimerSystem::pause(TimerComponent& p_component)
{
    for (std::vector<TimerComponent*>::iterator it = running_components.begin(); it != running_components.end(); ++it)
    {
        if (*it == &p_component)
        {
            running_components.erase(it);
            break;
        }
    }

    paused_components.push_back(&p_component);
}

void TimerSystem::stop(TimerComponent& p_component)
{
    for (std::vector<TimerComponent*>::iterator it = running_components.begin(); it != running_components.end(); ++it)
    {
        if (*it == &p_component)
        {
            running_components.erase(it);
            break;
        }
    }

    p_component.remaining_time = p_component.time;
    paused_components.push_back(&p_component);
}


void TimerSystem::added_timer_component(const ecs::Entity& p_entity, TimerComponent& p_component)
{
    p_component.remaining_time = p_component.time;

    if (p_component.auto_play)
    {
        running_components.push_back(&p_component);
    }
    else
    {
        paused_components.push_back(&p_component);
    }
}