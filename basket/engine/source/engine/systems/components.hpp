#ifndef BASKET_UTILS_COMPONENTS_HPP
#define BASKET_UTILS_COMPONENTS_HPP

namespace basket
{
    struct TimerComponent
    {
        float time = 1.0f;
        bool auto_play = false;
        bool repeat = true;
        float remaining_time = 1.0f;
        bool paused = false;
    };
}

#endif