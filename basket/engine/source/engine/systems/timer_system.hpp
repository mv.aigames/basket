#ifndef BASKET_TIMER_SYSTEM_HPP
#define BASKET_TIMER_SYSTEM_HPP

#include <ecs/ecs.hpp>
#include "components.hpp"

namespace basket
{
    class TimerSystem: public ISystem
    {
        public:
            using timer_finished_function = std::function<void(TimerComponent&)>;


        public:
            static TimerSystem& get();
            virtual Result init() override;
            virtual void update() override;
            virtual void shutdown() override;
        
        public:
            void play(TimerComponent& p_component);
            void pause(TimerComponent& p_component);
            void stop(TimerComponent& p_component);
            void add_timer_finished_function(const TimerComponent& p_component, timer_finished_function& p_function);
            void remove_timer_finished_function(const TimerComponent& p_component, timer_finished_function& p_function);
            void added_timer_component(const ecs::Entity& p_entity, TimerComponent& p_component);
        
        private:
            std::vector<TimerComponent*> paused_components;
            std::vector<TimerComponent*> running_components;
            std::unordered_map<const TimerComponent*, std::vector<timer_finished_function>> functions;
    };
}

#endif