#include <engine/time.hpp>
#include <windows.h>


namespace basket
{
    static LARGE_INTEGER _new_counter;
    static LARGE_INTEGER _old_counter;
    static LARGE_INTEGER _frequency;
    

    Time::Time()
    {
        QueryPerformanceCounter(&_old_counter);
    }

    void Time::update_impl()
    {
        _old_counter = _new_counter;
        QueryPerformanceCounter(&_new_counter);
        QueryPerformanceFrequency(&_frequency);

        _delta = static_cast<real_t>(_new_counter.QuadPart - _old_counter.QuadPart) / static_cast<real_t>(_frequency.QuadPart);
        
    }

    real_t Time::get_delta_impl()
    {
        return _delta;
    }
}