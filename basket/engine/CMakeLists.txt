project(basket_engine)



set(BASKET_ENGINE_SOURCES
    source/engine/bengine.cpp
    source/engine/events/system.cpp
    source/engine/inputs/glfw_input.cpp
    source/engine/module.cpp
    source/engine/platform/time_windows.cpp
)

set(BASKET_ENGINE_HEADERS
    source/engine/entry.hpp
)

set(BK_LIBS
    glfw
    ${Vulkan_LIBRARIES}
    ${BASKET_core}
    ${BASKET_render}
    Tracy::TracyClient
)

set(BK_DIRS
    ${CMAKE_SOURCE_DIR}/thirdparty/glfw/include
    ${Vulkan_INCLUDE_DIRS}
    ${BASKET_core_PATH}/source
    ${BASKET_render_PATH}/source
)


build_module(engine "${BASKET_ENGINE_SOURCES}" "${BK_LIBS}" "${BK_DIRS}")
