#ifndef BASKET_MESH_RESULT_HPP
#define BASKET_MESH_RESULT_HPP

namespace basket
{
    enum class MeshResult
    {
        success = 0,
        parse_fail,
    };
}

#endif