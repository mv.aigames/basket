#ifndef BASKET_OBJ_PARSER_HPP
#define BASKET_OBJ_PARSER_HPP

#include <mesh/mesh_parser.hpp>
#include <core/macros.hpp>

namespace basket
{
    class BASKET_API ObjParser: public AMeshParser
    {
        public:
            virtual MeshResult parse(const string& p_filepath) override;
    };
}

#endif