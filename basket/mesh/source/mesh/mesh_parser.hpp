#ifndef BVASKET_MESH_PARSER_HPP
#define BVASKET_MESH_PARSER_HPP

#include "mesh_results.hpp"

#include <core/string.hpp>

namespace basket
{
    class AMeshParser
    {
        public:
            virtual MeshResult parse(const string& p_filepath) = 0;
    };
}

#endif