#include "editor_imgui_vulkan.hpp"

#include <core/logging.hpp>
#include <render/window/glfw_window.hpp>
#include <render/render.hpp>

#include <imgui.h>
#include <backends/imgui_impl_glfw.h>
#include <backends/imgui_impl_vulkan.h>

// TODO: Replace
#define VK_ARRAYSIZE(_ARR)          ((int)(sizeof(_ARR) / sizeof(*(_ARR)))) 


static void check_vk_result(VkResult err)
{
    if (err == 0)
        return;
    fprintf(stderr, "[vulkan] Error: VkResult = %d\n", err);
    if (err < 0)
        abort();
}


namespace basket
{

    void EditorImguiVulkan::record(int32_t p_image_index, VkCommandBuffer p_command_buffer)
    {
        VkResult err;
        VkCommandBuffer command_buffer = p_command_buffer;

        // {
        //     // err = vkResetCommandPool(g_Device, fd->CommandPool, 0);
        //     // check_vk_result(err);
        //     VkCommandBufferBeginInfo info = {};
        //     info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        //     info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        //     err = vkBeginCommandBuffer(command_buffer, &info);
        //     check_vk_result(err);
        // }
        // {
        //     VkRenderPassBeginInfo info = {};
        //     info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        //     info.renderPass = _driver->get_render_pass(0);
        //     info.framebuffer = _driver->get_framebuffer(p_image_index);
        //     info.renderArea.extent.width = _driver->_get_extent().width;
        //     info.renderArea.extent.height = _driver->_get_extent().height;

        //     VkClearValue cv;
        //     cv.color.float32[0] = clear_color.x;
        //     cv.color.float32[1] = clear_color.y;
        //     cv.color.float32[2] = clear_color.z;
        //     cv.color.float32[3] = 0.0f;
        //     info.clearValueCount = 1;
        //     info.pClearValues = &cv;
        //     vkCmdBeginRenderPass(command_buffer, &info, VK_SUBPASS_CONTENTS_INLINE);
        // }

        // Record dear imgui primitives into command buffer
        ImDrawData* main_draw_data = ImGui::GetDrawData();
        ImGui_ImplVulkan_RenderDrawData(main_draw_data, command_buffer);

        // Submit command buffer
        // vkCmdEndRenderPass(command_buffer);
        // vkEndCommandBuffer(command_buffer);
        // _driver->add_command_buffer_to_submit(command_buffer);
    }

    void EditorImguiVulkan::set_render(Render* p_render)
    {
        _render = p_render;
        _driver = dynamic_cast<VulkanDriver*>(_render->get_render_driver());
    }

    void EditorImguiVulkan::setup()
    {
        {
            VkCommandPoolCreateInfo info{};
            info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            info.queueFamilyIndex = _driver->get_graphic_queue_family_index();
            info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
            _command_pool_index = _driver->create_command_pool(info);
        }

        {
            VkCommandBufferAllocateInfo info{};
            info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            info.commandBufferCount = _driver->get_max_frames_in_flight();
            info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            info.commandPool = _driver->get_command_pool(_command_pool_index);
            _driver->create_command_buffers(_command_pool_index, info);
        }

        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
        io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;         // Enable Docking
        io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable; 

        ImGui::StyleColorsDark();
        //ImGui::StyleColorsLight();

        // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
        ImGuiStyle& style = ImGui::GetStyle();
        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            style.WindowRounding = 0.0f;
            style.Colors[ImGuiCol_WindowBg].w = 1.0f;
        }

        VkDescriptorPoolSize pool_sizes[] =
        {
            { VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
        };
        VkDescriptorPoolCreateInfo pool_info = {};
        pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
        pool_info.maxSets = 1000 * VK_ARRAYSIZE(pool_sizes);
        pool_info.poolSizeCount = (uint32_t)VK_ARRAYSIZE(pool_sizes);
        pool_info.pPoolSizes = pool_sizes;
        VkResult result = vkCreateDescriptorPool(_driver->get_device(), &pool_info, nullptr, &_descriptor_pool);
        BK_VK_ASSERT(result ==VK_SUCCESS, "FAiled to create editor descriptor pool");


        BK_LOG(editor, info, "seting up vulkan imgui!");
        ImGui_ImplGlfw_InitForVulkan((GLFWwindow*)_render->get_window()->get_window_native(), true);
        ImGui_ImplVulkan_InitInfo info{};
        info.Instance = _driver->get_instance();
        info.PhysicalDevice = _driver->get_physical_device();
        info.Device = _driver->get_device();
        info.QueueFamily = _driver->get_graphic_queue_family_index();
        info.Queue = _driver->get_queue();
        info.PipelineCache = VK_NULL_HANDLE;
        info.DescriptorPool = _descriptor_pool;
        info.Subpass = 0;
        info.MinImageCount = 2;
        info.ImageCount = 2;
        info.MSAASamples = VK_SAMPLE_COUNT_1_BIT;
        info.CheckVkResultFn = nullptr;

        ImGui_ImplVulkan_Init(&info, _driver->get_render_pass(0));

        // // Upload Fonts
        {
            // Use any command queue

            // VkCommandPool command_pool = wd->Frames[wd->FrameIndex].CommandPool;
            VkCommandPool command_pool = _driver->get_command_pool(_command_pool_index);
            // VkCommandBuffer command_buffer = wd->Frames[wd->FrameIndex].CommandBuffer;
            VkCommandBuffer command_buffer = _driver->get_command_buffer(_command_pool_index, 0);

            VkResult r;

            r = vkResetCommandPool(_driver->get_device(), command_pool, 0);
            check_vk_result(r);
            VkCommandBufferBeginInfo begin_info = {};
            begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            begin_info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
            r = vkBeginCommandBuffer(command_buffer, &begin_info);
            check_vk_result(r);

            ImGui_ImplVulkan_CreateFontsTexture(command_buffer);

            VkSubmitInfo end_info = {};
            end_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            end_info.commandBufferCount = 1;
            end_info.pCommandBuffers = &command_buffer;
            r = vkEndCommandBuffer(command_buffer);
            check_vk_result(r);
            r = vkQueueSubmit(_driver->get_queue(), 1, &end_info, VK_NULL_HANDLE);
            check_vk_result(r);

            r = vkDeviceWaitIdle(_driver->get_device());
            check_vk_result(r);
            ImGui_ImplVulkan_DestroyFontUploadObjects();
        }

        // bool show_demo_window = true;
        // bool show_another_window = false;
        // ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

        // _draw_data = new DrawImguiDataRecordCommand(_driver);
        _driver->add_graphic_record(this);
    }

    void EditorImguiVulkan::draw()
    {
        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        ImGuiIO& io = ImGui::GetIO(); (void)io;

        if (show_demo_window)
            ImGui::ShowDemoWindow(&show_demo_window);
        
        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to create a named window.
        {
            static float f = 0.0f;
            static int counter = 0;

            ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

            ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
            ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
            ImGui::Checkbox("Another Window", &show_another_window);

            ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
            ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

            if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
                counter++;
            ImGui::SameLine();
            ImGui::Text("counter = %d", counter);

            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);
            ImGui::End();
        }

        // 3. Show another simple window.
        if (show_another_window)
        {
            ImGui::Begin("Another Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
            ImGui::Text("Hello from another window!");
            if (ImGui::Button("Close Me"))
                show_another_window = false;
            ImGui::End();
        }

        // Rendering
        ImGui::Render();
        // _main_draw_data = ImGui::GetDrawData();
        // const bool main_is_minimized = (_main_draw_data->DisplaySize.x <= 0.0f || _main_draw_data->DisplaySize.y <= 0.0f);
        // wd->ClearValue.color.float32[0] = clear_color.x * clear_color.w;
        // wd->ClearValue.color.float32[1] = clear_color.y * clear_color.w;
        // wd->ClearValue.color.float32[2] = clear_color.z * clear_color.w;
        // wd->ClearValue.color.float32[3] = clear_color.w;
        // if (!main_is_minimized)
        //     FrameRender(wd, main_draw_data);

        // Update and Render additional Platform Windows
        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
        }
    }

   

    void EditorImguiVulkan::clean()
    {
        BK_LOG(editor, info, "cleaning vulkan imgui!");
        ImGui_ImplVulkan_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
        vkDestroyDescriptorPool(_driver->get_device(), _descriptor_pool, nullptr);
        _driver = nullptr;
        _render = nullptr;
    }

    EditorImguiVulkan::~EditorImguiVulkan()
    {
        clean();
    }
}