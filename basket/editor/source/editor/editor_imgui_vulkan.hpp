#ifndef BASKET_EDITOR_IMGUI_VULKAN_HPP
#define BASKET_EDITOR_IMGUI_VULKAN_HPP

#include <editor/editor_draw.hpp>

#include <render/drivers/vulkan_driver.hpp>
#include <render/vulkan/vulkan_record_command.hpp>
#include <imgui.h>
#include <backends/imgui_impl_glfw.h>
#include <backends/imgui_impl_vulkan.h>

namespace basket
{  

    class BASKET_API EditorImguiVulkan: public EditorDraw, public IVulkanRecordCommand
    {
        
        private:
            VulkanDriver* _driver;
            class Render* _render;
            uint32_t _command_pool_index;
            VkDescriptorPool _descriptor_pool;
            // DrawImguiDataRecordCommand* _draw_data;

            bool show_demo_window = true;
            bool show_another_window = false;
            ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

        public:
            void set_render(Render* p_driver);
            void setup();
            void clean();
        
        public:
            virtual void record(int32_t p_image_index, VkCommandBuffer p_command_buffer) override;
            virtual bool one_time() const override { return false; }

        private:
            // void _init_imgui();
            // void FrameRender(ImGui_ImplVulkanH_Window* wd, ImDrawData* draw_data);

        public:
            virtual void draw() override;
        
        public:
            ~EditorImguiVulkan();
        

    };

    
}

#endif