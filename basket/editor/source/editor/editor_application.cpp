#include "editor_application.hpp"

#include <core/filesystem.hpp>
#include <core/module.hpp>
#include <core/os.hpp>

#include <render/window/window.hpp>
#include <render/shader/shader.hpp>
#include <render/render.hpp>
#include <render/module.hpp>
#include <render/drivers/vulkan_driver.hpp>

#include <engine/entry.hpp>
#include <engine/module.hpp>

#include "editor_imgui_vulkan.hpp"
#include <core/dstream.hpp>



namespace basket
{

    void EditorApplication::startup() 
    {
        _render->generate_window({"Basket Editor", 1400, 900, 0});
        editor_window = _render->get_window();
        // OS::execute("echo 42");
        _vulkan_driver = dynamic_cast<VulkanDriver*>(_render->get_render_driver());
        // init_imgui();
        {
            dstream d("dummy.txt", ios_base::in);

            if (!d.good())
            {
                BK_LOG(core, warning, "The file cannot be opened");
            }
            else
            {
                string s = "sample text";
                d << s;
                d.close();
            }
        }

        {
            dstream d("dummy.txt", ios_base::in | ios_base::out);

            if (!d.good())
            {
                BK_LOG(core, warning, "The file cannot be opened");
                return;
            }

            string s = "sample text";
            d << s;

            s = "Other text";
            d << s;
            d.close();
        }

        {
            dstream d("dummy.txt", ios_base::in | ios_base::out);

            if (!d.good())
            {
                BK_LOG(core, warning, "The file cannot be opened");
                return;
            }

            string s;
            s.resize(5);

            d.read(s.data(), s.size());

            BK_LOG(core, info, "from read {0}", s.c_str());
        }


        
    }

    void EditorApplication::post_startup() 
    {
        // _imgui_vulkan->set_render(_render);
        // _imgui_vulkan->setup();
    }


    void EditorApplication::init_imgui()
    {
        
    }

    void EditorApplication::update(const real_t p_delta) 
    {
        editor_window->pull_events();
        // _imgui_vulkan->draw();
        // _update_imgui(p_delta);
    }

    void EditorApplication::_update_imgui(const real_t p_delta)
    {
        
    }

    bool EditorApplication::want_to_shutdown() const
    {
        return editor_window->should_close();
    }

    void EditorApplication::shutdown() 
    {
        // ImGui_ImplVulkan_Shutdown();
        // ImGui_ImplGlfw_Shutdown();
        // _imgui_vulkan->clean();
        // delete _imgui_vulkan;
    }

    Application* get_application()
    {
        return new EditorApplication;
    }


    vector<AModule*> EditorApplication::get_modules()
    {
        vector<AModule*> modules;
        modules.push_back(new core::Module());
        modules.push_back(_render_module);
        modules.push_back(new engine::Module());
        return modules;
    }

    EditorApplication::EditorApplication()
    {
        _render_module = new render::Module();
        _render = _render_module->get_render();
        // _imgui_vulkan = new EditorImguiVulkan();
        
    }
}
