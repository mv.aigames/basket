#ifndef BASKET_EDITOR_APPLICATION_HPP
#define BASKET_EDITOR_APPLICATION_HPP

#include <engine/application.hpp>

#include <core/macros.hpp>
#include <core/memory.hpp>
#include <core/math/vector4.hpp>


namespace basket::render
{
    class Module;
}

namespace basket
{
    class AWindow;
    class BASKET_API EditorApplication: public Application
    {
        private:

        AWindow* editor_window;
        class Render* _render;
        class VulkanDriver* _vulkan_driver;
        class ::basket::render::Module* _render_module;
        class EditorImguiVulkan* _imgui_vulkan;

        public:
            EditorApplication();

        public:
            virtual void startup()  override;
            virtual void post_startup()  override;
            virtual void update(const real_t p_delta)  override;
            virtual bool want_to_shutdown() const  override;
            virtual void shutdown()  override;
            virtual vector<AModule*> get_modules() override;
        
        private:
            bool _show_demo_window = false;
            bool _show_another_window = false;
            Vector4 _clear_color;
            void init_imgui();
            void _update_imgui(const real_t p_delta);
    };
}

#endif
