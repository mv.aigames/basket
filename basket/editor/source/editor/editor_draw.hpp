#ifndef BASKET_EDITOR_DRAW_HPP
#define BASKET_EDITOR_DRAW_HPP

#include <core/macros.hpp>

namespace basket
{
    class BASKET_API EditorDraw
    {
        public:
            virtual void draw() { }
    };
}

#endif
